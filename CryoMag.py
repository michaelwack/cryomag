#!/usr/bin/python

"""CryoMag is a program to carry out measurements with a 2G SQUID and spinner magnetometers.<br>
It's mainly designed for paleomagnetic purposes."""


# CryoMag - an application to operate the 2G cryogenic magnetometer and spinner magnetometers
# (c) Michael Wack 2008, 2009, 2010, 2019, 2020, 2022, 2023

import wx
import CryoMagMainFrame

class CryoMagApp(wx.App):
    """Main application class"""
    def OnInit(self):
        """Init application and create main frame"""
        self.main = CryoMagMainFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        
        self.Bind(wx.EVT_CHAR, self.OnCryoMagChar)
        return True
    
    def OnCryoMagChar(self, event):
        """handle keypresses, especially function keys for function selection"""
        keycode = event.GetKeyCode()
        
        # send accelerator keys to main frame
        if (keycode >= wx.WXK_F1) and (keycode <= wx.WXK_F10):
            self.main.HandleAccKey(keycode)
            
        event.Skip()


def main():
    application = CryoMagApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
