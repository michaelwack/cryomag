<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
 version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns="http://www.w3.org/1999/xhtml">

<xsl:output method="html"/>

<xsl:template match="/">
<html>
  <head><title>CryoMag datafile</title>
    <style type="text/css">
      table { border-collapse:collapse; border:1px solid black; empty-cells:show;}
      td.specimen { border:1px solid #000; padding:10px; }
      td { border:1px solid #000; padding:5px; }
    </style>
  </head>
  <body>
   <xsl:apply-templates/>
  <p style="font-size:x-small">
  This is a <a href="http://geophysik.uni-muenchen.de/~wack/cryomag">CryoMag</a> XML data file.
  </p>
  </body>
</html>

</xsl:template>

<xsl:template match="specimen">
  <h1>
    specimen:
    <xsl:value-of select="@name"/>
  </h1>
  <table>
  <tr>
    <td class="specimen">coreaz</td>
    <td class="specimen"><xsl:value-of select="@coreaz"/></td>
    <td class="specimen">coredip</td>
    <td class="specimen"><xsl:value-of select="@coredip"/></td>
  </tr>
  <tr>
    <td class="specimen">bedaz</td>
    <td class="specimen"><xsl:value-of select="@bedaz"/></td>
    <td class="specimen">beddip</td>
    <td class="specimen"><xsl:value-of select="@beddip"/></td>
  </tr>
  <tr>
    <td class="specimen">volume</td>
    <td class="specimen"><xsl:value-of select="@vol"/>&#160;ccm</td>
    <td class="specimen">weight</td>
    <td class="specimen"><xsl:value-of select="@weight"/>&#160;g</td>
  </tr>
  </table>
  
  <h2>measurement data</h2>

  <xsl:apply-templates/>

  
 
</xsl:template>

<xsl:template match="step">
  <h3>
     step: <xsl:value-of select="@step"/> (<xsl:value-of select="@type"/>) [<xsl:value-of select="@comment"/>]
  </h3>
     <table>
       <thead>
          <tr><th></th><th>X&#160;[Am<sup>2</sup>]</th><th>Y&#160;[Am<sup>2</sup>]</th><th>Z&#160;[Am<sup>2</sup>]</th><th>M&#160;[Am<sup>2</sup>]</th><th>sM</th><th>a95</th><th>D</th><th>I</th><th>time</th></tr>
       </thead>
       <tbody>
          <xsl:apply-templates select="holder"/>
          <xsl:apply-templates select="measurements"/>
          <xsl:apply-templates select="results"/>
       </tbody>
     </table>
</xsl:template>

<xsl:template match="holder">
   <tr>
     <td>holder</td>
     <td><xsl:value-of select="@X"/></td>
     <td><xsl:value-of select="@Y"/></td>
     <td><xsl:value-of select="@Z"/></td>
     <td><xsl:value-of select="@M"/></td>
     <td><xsl:value-of select="@sM"/></td>
     <td><xsl:value-of select="@a95"/></td>
     <td><xsl:value-of select="@D"/></td>
     <td><xsl:value-of select="@I"/></td>
     <td><xsl:value-of select="@time"/></td>
   </tr>
</xsl:template>

<xsl:template match="results">
   <tr>
     <td>results</td>
     <td><xsl:value-of select="@X"/></td>
     <td><xsl:value-of select="@Y"/></td>
     <td><xsl:value-of select="@Z"/></td>
     <td><xsl:value-of select="@M"/></td>
     <td><xsl:value-of select="@sM"/></td>
     <td><xsl:value-of select="@a95"/></td>
     <td><xsl:value-of select="@D"/></td>
     <td><xsl:value-of select="@I"/></td>
     <td><xsl:value-of select="@time"/></td>
   </tr>
</xsl:template>

<xsl:template match="measurements">
         <xsl:apply-templates select="magmoment"/>
</xsl:template>

<xsl:template match="magmoment">
  <tr>
     <td><xsl:value-of select="@type"/></td>
     <td><xsl:value-of select="@X"/></td>
     <td><xsl:value-of select="@Y"/></td>
     <td><xsl:value-of select="@Z"/></td>
     <td><xsl:value-of select="@M"/></td>
     <td><xsl:value-of select="@sM"/></td>
     <td><xsl:value-of select="@a95"/></td>
     <td><xsl:value-of select="@D"/></td>
     <td><xsl:value-of select="@I"/></td>
     <td><xsl:value-of select="@time"/></td>
   </tr>
</xsl:template>

</xsl:stylesheet>
