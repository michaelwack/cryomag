# Boa:FramePanel:CryoMagSettingsPanel
'''Settings panel allows to change the measurement positions and mode of baselines.'''
# written by Michael Wack 2008, 2009, 2010

import wx
import wx.gizmos
import wx.grid

import CryoMagSettings


class CryoMagSettingsPanel(wx.Panel):
    def _init_coll_boxSizer1_Items(self, parent):

        if CryoMagSettings.measmode == 'C3':  # cryo mode
            parent.Add(self.HolderBox, 0, border=0, flag=wx.EXPAND)
            parent.Add(self.positionsbox, 0, border=0, flag=wx.GROW)
            parent.Add(self.baselinebox, 0, border=0, flag=wx.GROW)
            parent.Add(self.squidsbox, 0, border=0, flag=wx.EXPAND)

    def _init_sizers(self):

        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):

        wx.Panel.__init__(self, id=wx.ID_ANY,
                          name=u'CryoMagSettingsPanel', parent=prnt, pos=wx.Point(411, 245),
                          size=wx.Size(474, 493), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(474, 493))
        self.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL, False, u'Sans'))

        # controls for C2 and C3 mode

        self.PositionModeComboBox = wx.ComboBox(choices=[],
                                                id=wx.ID_ANY,
                                                name=u'PositionModeComboBox', parent=self, pos=wx.Point(16, 72),
                                                size=wx.Size(352, 22), style=wx.CB_READONLY, value=u'')
        self.PositionModeComboBox.Bind(wx.EVT_COMBOBOX,
                                       self.OnPositionModeComboBoxCombobox,
                                       id=wx.ID_ANY)

        self.PositionGrid = wx.grid.Grid(id=wx.ID_ANY,
                                         name=u'PositionGrid', parent=self, pos=wx.Point(16, 120),
                                         size=wx.Size(360, 120), style=0)
        self.PositionGrid.EnableEditing(False)
        self.PositionGrid.SetDefaultCellBackgroundColour(wx.Colour(212, 208,
                                                                   202))
        self.PositionGrid.SetColLabelSize(16)
        self.PositionGrid.SetDefaultColSize(50)
        self.PositionGrid.SetDefaultRowSize(16)

        self.staticText1 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'', name='staticText1',
                                         parent=self, pos=wx.Point(16, 102), size=wx.Size(200, 18),
                                         style=0)

        if CryoMagSettings.measmode == 'C2':  # cryo mode (C2)
            self.staticText1.SetLabel(u'Components')

        if CryoMagSettings.measmode == 'C3':  # cryo mode (C3)
            self.staticText1.SetLabel(u'Euler angles (x-convention)')

            self.positionsbox = wx.StaticBox(id=wx.ID_ANY,
                                             label=u'Specimen Positions', name=u'positionsbox', parent=self,
                                             pos=wx.Point(0, 48), size=wx.Size(474, 200), style=0)

            self.HolderBox = wx.StaticBox(id=wx.ID_ANY,
                                          label=u'Holder Positions', name=u'HolderBox', parent=self,
                                          pos=wx.Point(0, 0), size=wx.Size(474, 48), style=0)

            self.fourposradioButton = wx.RadioButton(id=wx.ID_ANY,
                                                     label=u'4 orth. positions', name=u'fourposradioButton',
                                                     parent=self, pos=wx.Point(16, 16), size=wx.Size(152, 24),
                                                     style=wx.RB_GROUP)
            self.fourposradioButton.SetValue(True)

            self.oneposradioButton = wx.RadioButton(id=wx.ID_ANY,
                                                    label=u'1 position (4 times)', name=u'oneposradioButton',
                                                    parent=self, pos=wx.Point(192, 16), size=wx.Size(136, 24),
                                                    style=0)

            self.baselinebox = wx.StaticBox(id=wx.ID_ANY,
                                            label=u'Specimen Baselines', name=u'baselinebox', parent=self,
                                            pos=wx.Point(0, 248), size=wx.Size(474, 104), style=0)

            self.beforeandafterradioButton = wx.RadioButton(id=wx.ID_ANY,
                                                            label=u'before and after all positions',
                                                            name=u'beforeandafterradioButton', parent=self,
                                                            pos=wx.Point(24,
                                                                         269), size=wx.Size(216, 24), style=wx.RB_GROUP)
            self.beforeandafterradioButton.SetValue(True)

            self.beforeeachradioButton = wx.RadioButton(id=wx.ID_ANY,
                                                        label=u'before each position', name=u'beforeeachradioButton',
                                                        parent=self, pos=wx.Point(248, 268), size=wx.Size(192, 24),
                                                        style=0)

            self.staticText2 = wx.StaticText(id=wx.ID_ANY,
                                             label=u'warning threshold:', name='staticText2', parent=self,
                                             pos=wx.Point(27, 312), size=wx.Size(117, 16), style=0)

            self.thresSpinCtrl1 = wx.SpinCtrl(id=wx.ID_ANY,
                                              initial=5, max=9, min=1, name=u'thresSpinCtrl1', parent=self,
                                              pos=wx.Point(153, 314), size=wx.Size(40, 23),
                                              style=wx.SP_ARROW_KEYS)

            self.thresSpinCtrl2 = wx.SpinCtrl(id=wx.ID_ANY,
                                              initial=-11, max=-1, min=-12, name=u'thresSpinCtrl2', parent=self,
                                              pos=wx.Point(236, 303), size=wx.Size(44, 23),
                                              style=wx.SP_ARROW_KEYS)

            self.staticText3 = wx.StaticText(id=wx.ID_ANY,
                                             label=u'* 10', name='staticText3', parent=self, pos=wx.Point(197,
                                                                                                          317),
                                             size=wx.Size(36, 19), style=0)
            self.staticText3.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.BOLD,
                                             False, u'Sans'))

            self.staticText4 = wx.StaticText(id=wx.ID_ANY,
                                             label=u'Am\xb2', name='staticText4', parent=self,
                                             pos=wx.Point(286, 317), size=wx.Size(35, 19), style=0)
            self.staticText4.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.BOLD,
                                             False, u'Sans'))

            self.squidsbox = wx.StaticBox(id=wx.ID_ANY,
                                          label=u'SQUIDs', name=u'squidsbox', parent=self, pos=wx.Point(0,
                                                                                                        352),
                                          size=wx.Size(474, 40), style=0)

            self.noreadspinCtrl = wx.SpinCtrl(id=wx.ID_ANY,
                                              initial=3, max=99, min=3, name=u'noreadspinCtrl', parent=self,
                                              pos=wx.Point(121, 364), size=wx.Size(40, 22),
                                              style=wx.SP_ARROW_KEYS)

            self.staticText5 = wx.StaticText(id=wx.ID_ANY,
                                             label=u'no. of readings', name='staticText5', parent=self,
                                             pos=wx.Point(24, 368), size=wx.Size(96, 14), style=0)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)

        self.PositionGrid.CreateGrid(12, 3)
        self.PositionGrid.DisableDragRowSize()
        self.PositionGrid.DisableDragColSize()

        self.LoadPositions()

    def LoadPositions(self):
        '''Load the positions displayed to the user.'''

        if CryoMagSettings.measmode == 'C3':  # cryo mode
            self.positionmodes = CryoMagSettings.C3positionmodes
        if CryoMagSettings.measmode == 'C2':  # spinner mode
            self.positionmodes = CryoMagSettings.C2positionmodes

        # add position modes to Choice Box
        for pm in self.positionmodes:
            self.PositionModeComboBox.Append(pm[0])

            # select initial choice
        self.PositionModeComboBox.SetSelection(1)
        self.SelectPositions()

    def OnPositionModeComboBoxCombobox(self, event):
        '''If the user selects a position in the combobox we have to update the grid'''
        self.SelectPositions()

    def SelectPositions(self):
        '''Display positions in the grid depending on the selection of the combobox'''
        # clear grid
        self.PositionGrid.ClearGrid()

        # show measurement positions in grid, depending on the selection of the combobox
        p = self.GetSelectedMeasurementPositions()

        for row in range(len(p)):
            for col in range(len(p[row])):
                self.PositionGrid.SetCellValue(row, col, str(p[row][col]))

    def GetSelectedMeasurementPositions(self):
        '''returns a list of positions depending on the selected entry in the combobox'''
        return self.positionmodes[self.PositionModeComboBox.GetSelection()][1]

    def GetSelectedMeasurementPositionsHolder(self):
        '''returns the orientation of the holder for each measurement position depending on the selected entry in the combobox'''
        return self.positionmodes[self.PositionModeComboBox.GetSelection()][2]

    def GetHolderMeasurementPositions(self):
        '''returns a list of positions for the holder depending on the user selection'''
        if self.oneposradioButton.GetValue() == True:
            return [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
        else:
            return [[0, 0, 0], [90, 0, 0], [180, 0, 0], [270, 0, 0]]
