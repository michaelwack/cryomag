"""CryoMagMainFrame provides the main window of the application."""
# written by Michael Wack 2008, 2009, 2010, 2021, 2022

import glob  # file patterns
import os.path  # file and directory functions

#import wx

import CryoMagSettings
from CryoMagAux import *
from CryoMagDataPanel import CryoMagDataPanel
from CryoMagMeasurementPanel import CryoMagMeasurementPanel
from CryoMagSettingsPanel import CryoMagSettingsPanel
from CryoMagSpecimens import CryoMagSpecimens
from CryoMagSpecimensPanel import CryoMagSpecimensPanel
from CryoMagTimedMeasurementPanel import CryoMagTimedMeasurementPanel
from MagPlots import *

def create(parent):
    return CryoMagMainFrame(parent)


class CryoMagMainFrame(wx.Frame):

    def _init_ctrls(self, prnt):
        
        wx.Frame.__init__(self, id=wx.ID_ANY,
                          name=u'CryoMagMainFrame', parent=prnt, pos=wx.Point(97, 200),
                          size=wx.Size(800, 699), style=wx.DEFAULT_FRAME_STYLE,
                          title=u'CryoMag')
        self.DataTimer = wx.Timer(id=wx.ID_ANY, owner=self)
        self.SetClientSize(wx.Size(788, 672))
        self.SetFont(wx.Font(8, wx.DEFAULT, wx.NORMAL, wx.NORMAL, False,
                             u'Arial'))

        self.MainSplitterWindow = wx.SplitterWindow(parent=self, name=u'MainSplitterWindow', size=wx.Size(500, 300),
                                                    style=wx.SP_LIVE_UPDATE | wx.SP_3D)


        ############
        ## LEFT PANEL

        self.LeftPanel = wx.Panel(id=wx.ID_ANY, name=u'LeftPanel', size=wx.Size(100, 100),
                                  parent=self.MainSplitterWindow)


        ## Plot Header Panel
        self.PlotHeaderPanel = wx.Panel(id=wx.ID_ANY,
                                        name=u'PlotHeaderPanel', parent=self.LeftPanel, pos=wx.Point(0, 0),
                                        size=wx.Size(441, 27), style=wx.TAB_TRAVERSAL)


        self.NoPointsShowSpinCtrl = wx.SpinCtrl(id=wx.ID_ANY,
                                                initial=99, max=999, min=2, name=u'NoPointsShowSpinCtrl',
                                                parent=self.PlotHeaderPanel, pos=wx.Point(260, 2),
                                                size=wx.Size(52, 22), style=wx.SP_ARROW_KEYS)
        self.NoPointsShowSpinCtrl.SetToolTip(u'Maximum number of points shown in plots')
        self.NoPointsShowSpinCtrl.Bind(wx.EVT_TEXT,
                                       self.OnNoPointsShowSpinCtrlText,
                                       id=wx.ID_ANY)

        self.CoreRadioButton = wx.RadioButton(id=wx.ID_ANY,
                                              label=u'core', name=u'CoreRadioButton',
                                              parent=self.PlotHeaderPanel, pos=wx.Point(10, 5), size=wx.Size(56,
                                                                                                             19),
                                              style=wx.RB_GROUP)
        self.CoreRadioButton.SetValue(True)
        self.CoreRadioButton.Bind(wx.EVT_RADIOBUTTON,
                                  self.OnCoreRadioButtonRadiobutton,
                                  id=wx.ID_ANY)

        self.GeographicRadioButton = wx.RadioButton(id=wx.ID_ANY,
                                                    label=u'geographic', name=u'GeographicRadioButton',
                                                    parent=self.PlotHeaderPanel, pos=wx.Point(72, 5), size=wx.Size(96,
                                                                                                                   19),
                                                    style=0)
        self.GeographicRadioButton.SetValue(False)
        self.GeographicRadioButton.Bind(wx.EVT_RADIOBUTTON,
                                        self.OnGeographicRadioButtonRadiobutton,
                                        id=wx.ID_ANY)

        self.TiltCorrectedRadioButton = wx.RadioButton(id=wx.ID_ANY,
                                                       label=u'tilt corrected', name=u'TiltCorrectedRadioButton',
                                                       parent=self.PlotHeaderPanel, pos=wx.Point(168, 3),
                                                       size=wx.Size(96, 24))
        self.TiltCorrectedRadioButton.SetValue(False)
        self.TiltCorrectedRadioButton.Bind(wx.EVT_RADIOBUTTON,
                                           self.OnTiltCorrectedRadioButtonRadiobutton,
                                           id=wx.ID_ANY)

        self.staticSpecimenText = wx.StaticText(id=wx.ID_ANY,
                                                label=u'S: ---', name='staticSpecimenText',
                                                parent=self.PlotHeaderPanel, pos=wx.Point(320, 8),
                                                size=wx.Size(112, 14))



        ## ZijderveldPlotPanel
        self.ZijderveldPlotPanel = ZijderveldPlot(parent=self.LeftPanel)

        ## StereoPlotPanel
        self.StereoPlotPanel = StereoPlot(parent=self.LeftPanel)

        self.LeftSizer = wx.BoxSizer(orient=wx.VERTICAL)
        self.LeftSizer.Add(self.PlotHeaderPanel, 0, border=0, flag=wx.GROW)
        self.LeftSizer.Add(self.ZijderveldPlotPanel, 1, border=0, flag=wx.GROW)

        self.LeftSizer.Add(wx.StaticLine(id=wx.ID_ANY,
                                         name='staticLine2', parent=self.LeftPanel, pos=wx.Point(0, 348),
                                         size=wx.Size(441, 3), style=wx.LI_HORIZONTAL), 0, border=0, flag=wx.GROW)

        self.LeftSizer.Add(self.StereoPlotPanel, 1, border=0, flag=wx.GROW)

        self.LeftPanel.SetSizer(self.LeftSizer)


        ############
        ## RIGHT PANEL

        self.RightPanel = wx.Panel(id=wx.ID_ANY, name=u'RightPanel', size=wx.Size(100, 100),
                                   parent=self.MainSplitterWindow)

        self.RightSizer = wx.BoxSizer(orient=wx.VERTICAL)

        #self.RightSizer.Add(wx.Button(self.RightPanel, label="Dada"), 0, border=0, flag=wx.GROW)

        ## Tool Panel
        self.ToolPanel = wx.Panel(id=wx.ID_ANY,
                                  name=u'ToolPanel',
                                  parent=self.RightPanel,
                                  pos=wx.Point(444, 0),
                                  size=wx.Size(630, 40), style=wx.TAB_TRAVERSAL)
        self.ToolPanel.SetMinSize(wx.Size(-1, 40))

        self.DirButton = wx.Button(id=wx.ID_ANY,
                                   label=u'Change Directory', name=u'DirButton', parent=self.ToolPanel,
                                   pos=wx.Point(20, 8), size=wx.Size(112, 24))
        self.DirButton.Bind(wx.EVT_BUTTON, self.OnDirButtonButton,
                            id=wx.ID_ANY)

        self.ExportButton = wx.Button(id=wx.ID_ANY,
                                      label=u'Export', name=u'ExportButton', parent=self.ToolPanel,
                                      pos=wx.Point(148, 8), size=wx.Size(70, 24))
        self.ExportButton.Bind(wx.EVT_BUTTON, self.OnExportButtonButton,
                               id=wx.ID_ANY)

        self.RightSizer.Add(self.ToolPanel, 0, border=0, flag=wx.GROW)


        ## Function Panel
        self.FunctionPanel = wx.Panel(id=wx.ID_ANY,
                                      name=u'FunctionPanel', parent=self.RightPanel, pos=wx.Point(444, 40),
                                      size=wx.Size(630, 206), style=wx.TAB_TRAVERSAL)

        # self.FunctionPanel = panel displayed at startup
        self.TitleStaticText = wx.StaticText(id=wx.ID_ANY,
                                             label=u'Welcome to CryoMag!', name=u'TitleStaticText',
                                             parent=self.FunctionPanel, pos=wx.Point(50, 60), size=wx.Size(250,30))
        self.TitleStaticText.SetFont(wx.Font(16, wx.SWISS, wx.NORMAL, wx.NORMAL,
                                             False, u'Arial'))

        self.staticText6 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'written by Michael Wack 2008-2023 (wack@geophysik.uni-muenchen.de)\n\
        For error reports or feature requests please use the ticketing system at',
                                         name='staticText6', parent=self.FunctionPanel, pos=wx.Point(50, 100),
                                         size=wx.Size(500, 39))

        # self.hyperlink1 = HyperLinkCtrl( parent = self.FunctionPanel, id=wx.ID_ANY, label='http://svn.geophysik.uni-muenchen.de/trac/cryomag', URL = 'http://svn.geophysik.uni-muenchen.de/trac/cryomag', pos=wx.Point( 50, 130))

        self.staticText7 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'If you use this software, please cite:\n\
        		M. Wack, A new software for the measurement of magnetic moments\n\
        		using SQUID and spinner magnetometers, Computers & Geosciences,\n\
        		Volume 36, Issue 9, September 2010, Pages 1178-1184, ISSN 0098-3004',
                                         name='staticText7', parent=self.FunctionPanel, pos=wx.Point(50,
                                                                                                     150),
                                         size=wx.Size(500, 56))
        self.staticText7.SetFont(wx.Font(9, wx.DEFAULT, wx.NORMAL, wx.NORMAL,
                                         False, u'Arial'))

        # self.hyperlink1 = HyperLinkCtrl( parent = self.FunctionPanel, id=wx.ID_ANY, label='doi:10.1016/j.cageo.2010.05.002', URL = 'http://dx.doi.org/10.1016/j.cageo.2010.05.002', pos=wx.Point( 50, 205))

        self.staticText8 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'Many thanks to Stuart Gilder for the basic concept, a lot of good ideas\nand the testing of this program.',
                                         name='staticText8', parent=self.FunctionPanel, pos=wx.Point(50, 230),
                                         size=wx.Size(500, 56))

        self.RightSizer.Add(self.FunctionPanel, 0, border=0, flag=wx.GROW)
        
        ## Button Panel
        self.ButtonPanel = wx.Panel(id=wx.ID_ANY,
                                    name=u'ButtonPanel', parent=self.RightPanel, pos=wx.Point(444, 246),
                                    size=wx.Size(630, 24), style=wx.TAB_TRAVERSAL)

        self.ButtonSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.HolderButton = wx.Button(id=wx.ID_ANY,
                                      label=u'F5 Holder', name=u'HolderButton', parent=self.ButtonPanel,
                                      pos=wx.Point(0, 0), size=wx.Size(105, 24))
        self.HolderButton.SetMinSize(wx.Size(70, 24))
        self.HolderButton.Bind(wx.EVT_BUTTON, self.OnHolderButtonButton,
                               id=wx.ID_ANY)

        self.ButtonSizer.Add(self.HolderButton, 1, border=0, flag=0)

        self.MeasurementButton = wx.Button(id=wx.ID_ANY,
                                           label=u'F6 Measurement', name=u'MeasurementButton',
                                           parent=self.ButtonPanel, pos=wx.Point(126, 0), size=wx.Size(105,
                                                                                                       24))
        self.MeasurementButton.Bind(wx.EVT_BUTTON,
                                    self.OnMeasurementButtonButton,
                                    id=wx.ID_ANY)

        self.ButtonSizer.Add(self.MeasurementButton, 1, border=0, flag=0)

        self.TimedMeasurementButton = wx.Button(id=wx.ID_ANY,
                                                label=u'F7 Timed Meas.', name=u'TimedMeasurementButton',
                                                parent=self.ButtonPanel, pos=wx.Point(126, 0), size=wx.Size(105, 24),
                                                style=0)
        self.TimedMeasurementButton.Bind(wx.EVT_BUTTON,
                                         self.OnTimedMeasurementButtonButton,
                                         id=wx.ID_ANY)

        self.ButtonSizer.Add(self.TimedMeasurementButton, 1, border=0, flag=0)

        self.SpecimenButton = wx.Button(id=wx.ID_ANY,
                                        label=u'F8 Specimens', name=u'SpecimenButton',
                                        parent=self.ButtonPanel, pos=wx.Point(252, 0), size=wx.Size(105,
                                                                                                    24))

        self.SpecimenButton.Bind(wx.EVT_BUTTON, self.OnSpecimenButtonButton,
                                 id=wx.ID_ANY)

        self.ButtonSizer.Add(self.SpecimenButton, 1, border=0, flag=0)

        self.DataButton = wx.Button(id=wx.ID_ANY,
                                    label=u'F9 Data', name=u'DataButton', parent=self.ButtonPanel,
                                    pos=wx.Point(378, 0), size=wx.Size(105, 24))
        self.DataButton.Bind(wx.EVT_BUTTON, self.OnDataButtonButton,
                             id=wx.ID_ANY)

        self.ButtonSizer.Add(self.DataButton, 1, border=0, flag=0)

        self.SettingsButton = wx.Button(id=wx.ID_ANY,
                                        label=u'F10 Settings', name=u'SettingsButton',
                                        parent=self.ButtonPanel, pos=wx.Point(504, 0), size=wx.Size(105,
                                                                                                    24))
        self.SettingsButton.Bind(wx.EVT_BUTTON, self.OnSettingsButtonButton,
                                 id=wx.ID_ANY)

        self.ButtonSizer.Add(self.SettingsButton, 1, border=0, flag=0)

        self.ButtonPanel.SetSizer(self.ButtonSizer)

        self.RightSizer.Add(self.ButtonPanel, 0, border=0, flag=wx.GROW)

        ## DecayPlotPanel
        self.DecayPlotPanel = XYPlot(parent=self.RightPanel)
        self.DecayPlotPanel.legend.append({'dpstyle': None, 'txt': 'Decay Plot'})
        self.DecayPlotPanel.legend.append({'dpstyle': None, 'txt': ''})

        self.RightSizer.Add(self.DecayPlotPanel, 1, border=0, flag=wx.GROW)


        ##
        #self.RightSizer.SetSizeHints(self.RightPanel)
        self.RightPanel.SetSizerAndFit(self.RightSizer)

        self.MainSplitterWindow.SetMinimumPaneSize(400)
        self.MainSplitterWindow.SplitVertically(self.LeftPanel, self.RightPanel, -630)
        #self.MainSplitterWindow.Initialize(self.RightPanel)

        #self.Layout()



    def __init__(self, parent):

        self._init_ctrls(parent)

        # set title of main window
        self.SetTitle('CryoMag %s [%s - %s]' % (
            CryoMagSettings.cryomagversion, CryoMagSettings.instrname, CryoMagSettings.measmode))


        self.TitleStaticText.SetLabel('Welcome to CryoMag %s' % CryoMagSettings.cryomagversion)

        self.Maximize()

        self.DataTimer.Start(100)

        # bind functions to plot panels
        self.DecayPlotPanel.Bind(EVT_PLOT_POINTSELECT, self.OnDecayPlotPanelPointSelect)
        self.StereoPlotPanel.Bind(EVT_PLOT_POINTSELECT, self.OnStereoPlotPanelPointSelect)
        self.ZijderveldPlotPanel.Bind(EVT_PLOT_POINTSELECT, self.OnZijderveldPlotPanelPointSelect)

        self.DecayPlotPanel.Bind(EVT_PLOT_POINTDELETE, self.OnPlotPanelPointDelete)
        self.StereoPlotPanel.Bind(EVT_PLOT_POINTDELETE, self.OnPlotPanelPointDelete)
        self.ZijderveldPlotPanel.Bind(EVT_PLOT_POINTDELETE, self.OnPlotPanelPointDelete)

        # self.FunctionPanel.SetFocus()

        self.FunctionPanels = {}
        self.FunctionPanels['default'] = self.FunctionPanel
        self.FunctionPanels['measurement'] = CryoMagMeasurementPanel(self.RightPanel)
        self.FunctionPanels['timedmeasurement'] = CryoMagTimedMeasurementPanel(self.RightPanel)
        self.FunctionPanels['specimens'] = CryoMagSpecimensPanel(self.RightPanel)
        self.FunctionPanels['data'] = CryoMagDataPanel(self.RightPanel)
        self.FunctionPanels['settings'] = CryoMagSettingsPanel(self.RightPanel)

        # set minimum size for all function panels
        for key, panel in self.FunctionPanels.items():
            panel.SetMinSize(wx.Size(100, 400))

        # add panels to sizer
        for panelname, panel in self.FunctionPanels.items():
            if panelname != 'default':  # default is already added by Boa
                self.RightSizer.Insert(1, panel, proportion=0, flag=wx.GROW)

        # show default panel - hide all others
        self.LoadFunctionPanel('default')

        # initialize list of specimens
        self.specimens = CryoMagSpecimens()

        # init ShownSpecimen
        self.ShownSpecimen = ''

        # change to an initial data directory
        # self.ChangeWorkingDirectory('.')

        # no valid holder
        self.validholder = False


    def HandleAccKey(self, keycode):
        if keycode == wx.WXK_F5:
            self.OnHolderMeasurement()
        elif keycode == wx.WXK_F6:
            self.OnSpecimenMeasurement()
        elif keycode == wx.WXK_F7:
            self.OnTimedSpecimenMeasurement()
        elif keycode == wx.WXK_F8:
            self.OnSpecimens()
        elif keycode == wx.WXK_F9:
            self.OnData()
        elif keycode == wx.WXK_F10:
            self.OnSettings()

    def GetCurrentHolderResults(self):
        """Returns the last measured holder results or if not, zero dummy data"""
        if self.validholder == True:
            return self.specimens.GetStepData('holder')[-1]['results']
        else:
            # return dummy data
            return {'X': 0.0, 'Y': 0.0, 'Z': 0.0, 'M': 0.0, 'D': 0.0, 'I': 0.0, 'a95': 0.0, 'sM': 0.0, 'time': '',
                    'no_readings': 0}

    # add measured data to self.measurement data
    def StoreData(self, specimenname, data):
        # add specimen if it is not already in our list
        if not self.specimens.SpecimenExists(specimenname):
            self.specimens.AddSpecimen(specimenname)

        # add the data as a new step
        self.specimens.AddStepData(specimenname, data)

        # update selectable specimens in data panel
        # self.FunctionPanels[ 'data'].UpdateSpecimens()
        # performance tuning 20.1.09
        # mark data panel for update when it's shown the next time
        self.FunctionPanels['data'].needupdate = True

        self.specimens.WriteSpecimenDataToFile(specimenname)

        # update plots with new data
        self.ShowSpecimenData(specimenname, True)

    def ChangeWorkingDirectory(self, dirname=0):
        """Change the working directory and load all contained data"""
        if dirname == 0:
            dlg = wx.DirDialog(self, "Choose a directory:",
                               defaultPath=os.getcwd(), style=wx.DD_DEFAULT_STYLE)

            # If the user selects OK, then we process the dialog's data.
            # This is done by getting the path data from the dialog - BEFORE
            # we destroy it. 
            if dlg.ShowModal() == wx.ID_OK:
                dirname = dlg.GetPath()

            # Only destroy a dialog after you're done with it.
            dlg.Destroy()

        if dirname != 0:
            # change current directory
            os.chdir(dirname)
            self.LoadSpecimenDataFromDirectory(show_progress=True)

    def LoadSpecimenDataFromDirectory(self, show_progress=False):
        """Load all specimen data files from current working directory"""
        # remove old measurement data from memory
        self.specimens.Clear()

        # specimen counter
        speccount = 0
        # interate over all .cmag.xml files and load their data
        files = glob.glob('./*.cmag.xml')

        # create progress dialog if needed
        if show_progress:
            dlg = wx.ProgressDialog("Loading data files....",
                                    "",
                                    maximum=len(files) + 1,
                                    parent=wx.GetApp().GetTopWindow(),
                                    style=wx.PD_APP_MODAL
                                          | wx.PD_ELAPSED_TIME
                                          | wx.PD_ESTIMATED_TIME
                                          | wx.PD_REMAINING_TIME
                                          | wx.PD_AUTO_HIDE
                                    )
            # set the focus to the dialog to prevent key events in other windows
            # dlg.SetFocus()

        count = 0

        for f in files:
            speccount += self.specimens.LoadSpecimenDataFromFile(f)

            if show_progress:
                # update progress dialog
                # dlg.Pulse( f)
                dlg.Update(count)
                count += 1

        # load holder data from home directory
        holderfilename = os.path.join(os.path.expanduser("~"), "holder.cmag.xml")

        if os.path.isfile(holderfilename):
            # if there was no holder data, set valid_holder to false
            if self.specimens.LoadSpecimenDataFromFile(holderfilename) == 0:
                self.validholder = False
        else:
            # file not found, no valid holder data
            self.validholder = False

        if show_progress:
            # update progress dialog
            dlg.Update(count, "Updating specimen data ...")

        # update data panel
        # self.FunctionPanels[ 'data'].UpdateSpecimens()
        # 23.1.09 no longer needed, just mark it for update
        self.FunctionPanels['data'].needupdate = True

        # update specimen panel
        self.FunctionPanels['specimens'].UpdateSpecimens()

        # We are done, destroy the progress dialog
        if show_progress:
            dlg.Destroy()

        # Show Report
        wx.MessageBox("%d specimens from %s loaded." % (speccount, os.getcwd()))

    def ShowSpecimenData(self, specimenname, bSelLast, coord=-1, maxnoofpoints=-1):
        """
        update plots with measurement data
        coord specifies coordinate system to use
        if bSelLast is set the last point in plot is selected
        coord specifies the coordinate system to use automatic( -1), core(0), geo(1), bedding(2)"""

        # check if specimen exists or return
        if not self.specimens.SpecimenExists(specimenname):
            return

            # find out what coordinate system we need
        if coord == -1:
            if self.CoreRadioButton.GetValue() == True:
                coord = 0
            elif self.GeographicRadioButton.GetValue() == True:
                coord = 1
            elif self.TiltCorrectedRadioButton.GetValue() == True:
                coord = 2

        data = self.specimens.GetStepData(specimenname)

        # update specimen name and add type of first step
        label = specimenname
        try:
            label += ',' + data[0]['type']
        except:
            pass

        self.staticSpecimenText.SetLabel(label)
        self.ShownSpecimen = specimenname

        # 17-6-2008
        # find out how many points to display
        if maxnoofpoints == -1:
            # take setting out of SpinCtrl on top of plots
            maxnoofpoints = int(self.NoPointsShowSpinCtrl.GetValue())

        # core coordinates
        # get data points, at maximum `maxnoofpoint` to the end
        xyzdata = [(r['results']['X'], r['results']['Y'], r['results']['Z'], r['step']) for r in data[-maxnoofpoints:]]
        # incdecdata =  [(r['results']['I'], r['results']['D'], r['step']) for r in data]

        if coord > 0:  # convert to geographic coordinates
            coreaz = self.specimens.GetProp(specimenname)['coreaz']
            coredip = self.specimens.GetProp(specimenname)['coredip']
            xyzdata = [core2geo(xyz, coreaz, coredip) for xyz in xyzdata]

            self.ZijderveldPlotPanel.SetXYZLabels(('N', 'E', '-UP'))
        else:
            self.ZijderveldPlotPanel.SetXYZLabels(('X', 'Y', 'Z'))

        if coord == 2:  # convert further to tilt corrected coordinates
            beddip = self.specimens.GetProp(specimenname)['beddip']
            bedaz = self.specimens.GetProp(specimenname)['bedaz']
            # BUGFIX: exchanged bedaz and beddip 30-09-2008
            xyzdata = [geo2bed(xyz, bedaz, beddip) for xyz in xyzdata]

            # decay data is always the same
        decaydata = [[float(xyz[3]), math.sqrt(xyz[0] ** 2 + xyz[1] ** 2 + xyz[2] ** 2)] for xyz in xyzdata]

        # normalize decay data
        maxdd = .0
        for dd in decaydata:
            if dd[1] > maxdd:
                maxdd = dd[1]
        decaydata = [[dd[0], dd[1] / maxdd] for dd in decaydata]

        self.ZijderveldPlotPanel.SetXYZData(xyzdata)
        self.StereoPlotPanel.SetXYZData([xyzdata])
        # write maximum in decay plots legend
        self.DecayPlotPanel.legend[1]['txt'] = "1 = %.2E Am^2" % maxdd
        self.DecayPlotPanel.SetData([decaydata])

        if bSelLast:
            self.StereoPlotPanel.SelectDataPoint(0, len(xyzdata) - 1)
            self.DecayPlotPanel.SelectDataPoint(0, len(decaydata) - 1)
            self.ZijderveldPlotPanel.SelectDataPoint(0, len(xyzdata) - 1)

    def OnDecayPlotPanelPointSelect(self, event):
        (pnr, dnr) = event.GetPointIndex()
        # select corresponding points in other plots
        self.StereoPlotPanel.SelectDataPoint(0, dnr)
        self.ZijderveldPlotPanel.SelectDataPoint(0, dnr)

        # TODO: select entry in DataPanel

    def OnStereoPlotPanelPointSelect(self, event):
        (pnr, dnr) = event.GetPointIndex()
        # select corresponding points in other plots
        self.DecayPlotPanel.SelectDataPoint(0, dnr)
        self.ZijderveldPlotPanel.SelectDataPoint(0, dnr)

        # TODO: select entry in DataPanel

    def OnZijderveldPlotPanelPointSelect(self, event):
        (pnr, dnr) = event.GetPointIndex()
        # select corresponding points in other plots
        self.StereoPlotPanel.SelectDataPoint(0, dnr)
        self.DecayPlotPanel.SelectDataPoint(0, dnr)

        # TODO: select entry in DataPanel

    # plot wants to delete a point -> we have to care about
    def OnPlotPanelPointDelete(self, event):
        pass
        # delete on plot panels deactivated 27.5.08

        # (pnr, dnr) = event.GetPointIndex()
        # try:
        #    # delete corresponding data from memory and rewrite file
        #    self.DeleteStep( self.ShownSpecimen, dnr)
        #    
        # except AttributeError:
        #    # no shown specimen - do nothing
        #    pass

    # deletes measurements and results of the dnr'th step
    def DeleteStep(self, specimen, dnr):
        dlg = wx.MessageDialog(self,
                               "Do you really want to delete step %s from specimen %s? The data willl be deleted from the data file and can't be restored."
                               % (str(self.specimens.GetStepData(specimen)[dnr]['step']), specimen),
                               'Delete', wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
        try:
            if dlg.ShowModal() == wx.ID_YES:
                # if we are deleting the last data point of the holder
                # we have to invalidate the holder to avoid spurious measurements
                if specimen == 'holder' and dnr == len(self.specimens.GetStepData(specimen)) - 1:
                    self.validholder = False

                # delte selected step data
                self.specimens.DelStepData(specimen, dnr)

                # update selectable specimens in data panel
                self.FunctionPanels['data'].UpdateSpecimens()

                # save altered data to file
                self.specimens.WriteSpecimenDataToFile(specimen)

                # update Plots
                self.ShowSpecimenData(self.ShownSpecimen, False, 0)


        finally:
            dlg.Destroy()

    ###########################
    # handle button functions #
    ###########################

    def LoadFunctionPanel(self, panel):
        # Hide all function panels
        for p in self.FunctionPanels.values():
            p.Hide()

        # show the selected one
        self.FunctionPanels[panel].Show()

        self.RightSizer.Layout()

        self.FunctionPanels[panel].SetFocus()

    def OnHolderButtonButton(self, event):
        """Load measurement panel and start holder measurement"""
        self.OnHolderMeasurement()

    def OnHolderMeasurement(self):
        """Load measurement panel and start holder measurement"""
        if CryoMagSettings.measmode == 'C2':
            wx.MessageBox('Holder measurement not available with spinner magnetometers', 'Function disabled')
        else:
            self.LoadFunctionPanel('measurement')
            self.FunctionPanels['measurement'].StartHolderMeasurement()

    def OnMeasurementButtonButton(self, event):
        """Load measurement panel and start specimen measurement"""
        self.OnSpecimenMeasurement()

    def OnSpecimenMeasurement(self):
        """Load measurement panel and start specimen measurement"""
        self.LoadFunctionPanel('measurement')
        self.FunctionPanels['measurement'].StartSpecimenMeasurement()

    def OnTimedMeasurementButtonButton(self, event):
        """Load measurement panel and start specimen measurement"""
        self.OnTimedSpecimenMeasurement()

    def OnTimedSpecimenMeasurement(self):
        """Load measurement panel and start timed specimen measurement"""
        if CryoMagSettings.measmode == 'C2':
            wx.MessageBox('Timed measurement not available with spinner magnetometers', 'Function disabled')
        else:
            self.LoadFunctionPanel('timedmeasurement')
            self.FunctionPanels['timedmeasurement'].InitSpecimenTimedMeasurement()

    def OnSpecimenButtonButton(self, event):
        """Load specimen panel"""
        self.OnSpecimens()

    def OnSpecimens(self):
        """Load specimen panel"""
        self.LoadFunctionPanel('specimens')

    def OnDataButtonButton(self, event):
        """Load data panel"""
        self.OnData()

    def OnData(self):
        """Load data panel"""
        self.LoadFunctionPanel('data')

    def OnSettingsButtonButton(self, event):
        """Load settings panel"""
        self.OnSettings()

    def OnSettings(self):
        """Load settings panel"""
        self.LoadFunctionPanel('settings')

    def OnDirButtonButton(self, event):
        """Open a new working directory"""
        self.ChangeWorkingDirectory()

    def OnExportButtonButton(self, event):
        """Export data to other file formats
        User can select format in a single choice dialog"""

        dlg = wx.SingleChoiceDialog(
            self, 'Choose export format ....', 'CryoMag Export',
            ['Palmag Munich files (*.dat)', 'Palmag Munich ASC file (*.asc)', 'PaleoMac files (*.pmd)',
             'ASCII result file (*.txt)', 'ASCII measurement file (*.txt)',
             'Specimen list (*.txt)', 'Standard MagIC Text File (*.txt)', 'CIT format (PaleoMag) (*.sam)',
             'IAPD format (*.dat)'],
            wx.CHOICEDLG_STYLE
        )

        try:

            if dlg.ShowModal() == wx.ID_OK:
                sel = dlg.GetSelection()

                if sel == 0:
                    # Munich Palmag format
                    self.ExportDataToPalmag()
                elif sel == 1:
                    # Specimen List
                    self.ExportDataToPalmagASC()
                elif sel == 2:
                    # Specimen List
                    self.ExportDataToPaleoMac()
                elif sel == 3:
                    # Single big file with results of each step
                    self.ExportDataToSingleFile()
                elif sel == 4:
                    # single big file with all measurement data
                    self.ExportDetailedDataToSingleFile()
                elif sel == 5:
                    # Specimen List
                    self.ExportSpecimenList()
                elif sel == 6:
                    # Standard MagIC Text File
                    self.ExportMagICText()
                elif sel == 7:
                    # PaleoMag / CIT SAM file
                    self.ExportDataToPaleoMag()
                elif sel == 8:
                    # Super IAPD .dat file
                    self.ExportDataToIAPD()
                else:
                    wx.MessageBox('Invalid selection.')
        finally:
            dlg.Destroy()

    def ExportDataToSingleFile(self):
        """Export results of all specimens to a file"""
        dlg = wx.FileDialog(self, "Export specimen results", wildcard="*.txt",
                            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportToFile(dlg.GetPath())
                wx.MessageBox('%d specimens exported to %s.' % (sc, dlg.GetPath()))

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportDetailedDataToSingleFile(self):
        """Export results of all specimens to a file"""
        dlg = wx.FileDialog(self, "Export detailed measurements", wildcard="*.txt",
                            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportMeasurementsToFile(dlg.GetPath())
                wx.MessageBox('%d specimens exported to %s.' % (sc, dlg.GetPath()))

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportDataToPalmag(self):
        """Export results of all specimens to an user selectable directory."""
        dlg = wx.DirDialog(self, "Choose a directory:",
                           defaultPath=os.getcwd(), style=wx.DD_DEFAULT_STYLE)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportToPalmag(dlg.GetPath())

                wx.MessageBox('%d specimens exported to Palmag format.' % sc)

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportDataToPalmagASC(self):
        """Export results of all specimens to a file"""
        dlg = wx.FileDialog(self, "Export Palmag ASC", wildcard="*.asc",
                            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportToPalmagASC(dlg.GetPath())
                wx.MessageBox('%d specimens exported to %s.' % (sc, dlg.GetPath()))

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportSpecimenList(self):
        """Export results of all specimens to a file"""
        dlg = wx.FileDialog(self, "Export specimen list", wildcard="*.txt",
                            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportPropToFile(dlg.GetPath())
                wx.MessageBox('%d specimens exported to %s.' % (sc, dlg.GetPath()))

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportDataToPaleoMac(self):
        """Export results of all specimens to an user selectable directory."""
        dlg = wx.DirDialog(self, "Choose a directory:",
                           defaultPath=os.getcwd(), style=wx.DD_DEFAULT_STYLE)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportToPaleoMac(dlg.GetPath())

                wx.MessageBox('%d specimens exported to PaleoMac format.' % sc)

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportMagICText(self):
        """Export results of all specimens to a MAGIC Text File"""
        dlg = wx.FileDialog(self, "Export to standard MagIC Text File", wildcard="*.txt",
                            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # ask user how to map step types to treatment (ac, dc, temp)
                # get all used step types as unique set in upper case
                typeset = set([])
                for spec in self.specimens.GetSpecimenList():
                    if spec != 'holder':
                        for step in self.specimens.GetStepData(spec):
                            typeset.add(step['type'].upper())

                typelist = list(typeset)

                # wx.MessageBox( str( typeset), '')
                treatmenttypes = {}

                for tt in ('treatment_temp', 'treatment_ac_field', 'treatment_dc_field'):
                    ttdlg = wx.MultiChoiceDialog(self, 'Select step types to be exported as %s.' % tt,
                                                 'Export to Standard MagIC Text File', typelist)
                    try:
                        if ttdlg.ShowModal() == wx.ID_OK:
                            treatmenttypes[tt] = []
                            sel = ttdlg.GetSelections()
                            for ttc in sel:
                                # append selected types to the current treatment_type
                                # e.g. treatmenttypes['treatment_ac'] = ['AF', 'AC']
                                treatmenttypes[tt].append(typelist[ttc])
                            # delete items that were selected to not show them in the next dialog
                            for ttc in sel[::-1]:
                                del typelist[ttc]
                    finally:
                        ttdlg.Destroy()

                # wx.MessageBox( str( treatmenttypes), '')

                # write data to file
                sc = self.specimens.ExportToMagICText(dlg.GetPath(), treatmenttypes)

                wx.MessageBox('%d specimens exported to %s.' % (sc, dlg.GetPath()))

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportDataToPaleoMag(self):
        """Export results of all specimens to an user selectable directory."""
        dlg = wx.FileDialog(self, "Export to CIT format (PaleoMag)", wildcard="*.sam",
                            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportToPaleoMag(dlg.GetPath())

                wx.MessageBox('%d specimens exported to PaleoMag sample data format.' % sc)

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

    def ExportDataToIAPD(self):
        """Export results of all specimens to an user selectable directory."""
        dlg = wx.DirDialog(self, "Choose a directory:",
                           defaultPath=os.getcwd(), style=wx.DD_DEFAULT_STYLE)

        try:
            # If the user selects OK, then we process the dialog's data.
            if dlg.ShowModal() == wx.ID_OK:
                # write data to file
                sc = self.specimens.ExportToIAPD(dlg.GetPath())

                wx.MessageBox('%d specimens exported to IAPD format.' % sc)

        finally:
            # Only destroy a dialog after we're done with it.
            dlg.Destroy()

            ######################

    # change projections #
    ######################

    def OnCoreRadioButtonRadiobutton(self, event):
        # feed plots with core coordinates
        self.ShowSpecimenData(self.ShownSpecimen, True)

    def OnGeographicRadioButtonRadiobutton(self, event):
        # feed plots with geographic coordinates
        self.ShowSpecimenData(self.ShownSpecimen, True)

    def OnTiltCorrectedRadioButtonRadiobutton(self, event):
        # feed plot with tilt corrected coordinates
        self.ShowSpecimenData(self.ShownSpecimen, True)

    #############################
    # change no of points shown #
    #############################

    def OnNoPointsShowSpinCtrlText(self, event):
        self.ShowSpecimenData(self.ShownSpecimen, True)
