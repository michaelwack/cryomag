# CryoMagSpecimens
"""Manage specimen data, load and store to files, handle data export"""
# written by Michael Wack 2008, 2009, 2020, 2021

# TODO: check inverse drill option - broken

from xml.etree import ElementTree
from CryoMagAux import *
from copy import copy
import os.path
import time
import CryoMagSettings

class CryoMagSpecimens:
    """CryoMagSpecimens manages a list of specimens and associated data"""

    def __init__(self):
        self.Clear()

    def Clear(self):
        """Clear all data, add a default holder"""
        # initialize specimen data
        try:
            del self.data
        except AttributeError:
            pass

        self.data = {}
        self.AddSpecimen('holder')

    def AddSpecimen(self, specimenname,
                    prop={'coreaz': 0, 'coredip': 0, 'beddip': 0, 'bedaz': 0, 'weight': 1, 'vol': 11, 'lat': None,
                          'lon': None}, stepdata=[]):
        """Adds a Specimen to the internal list.<br>
        prop is a dictionary of properties of the specimen.<br>
        stepdata is a list of dictionaries with data of each step.
        """

        self.data[specimenname] = {}
        self.data[specimenname]['prop'] = copy(prop)
        self.data[specimenname]['stepdata'] = copy(stepdata)

    def DelSpecimen(self, specimenname):
        """Del a specimen and all its data from the internal list"""
        if self.SpecimenExists(specimenname):
            del self.data[specimenname]

    def RenameSpecimen(self, oldspecimenname, newspecimenname):
        """Deletes a specimen from memory, the data file persists
        and can be deleted using DelSpecimenDataFile()."""

        # return error if specimen to rename doesn't exist
        if not self.SpecimenExists(oldspecimenname):
            return 1
        # return error if new specimenname already exists
        if self.SpecimenExists(newspecimenname, casesens=False):
            return 2
        # add a new specimen with the data of the old one
        self.AddSpecimen(newspecimenname, self.GetProp(oldspecimenname), self.GetStepData(oldspecimenname))

        # delete the old one
        self.DelSpecimen(oldspecimenname)

        return 0

    def GetSpecimenList(self):
        """Return lowercase sorted list of specimens"""
        return sorted([str(k) for k in self.data.keys()], key=str.lower)

    def AddStepData(self, specimenname, stepdata):
        """Add Data for one step. stepdata is a dictionary."""
        if self.SpecimenExists(specimenname):
            self.GetStepData(specimenname).append(copy(stepdata))

    def GetStepData(self, specimenname):
        """returns list of data of all steps"""
        return self.data[specimenname]['stepdata']

    def DelStepData(self, specimenname, stepno):
        """delete one step with number stepno from the list of steps"""
        del self.data[specimenname]['stepdata'][stepno]

    def GetProp(self, specimenname):
        """Get the properties of a specimen.<br>
        Should be used as a Lvalue to change the properties."""
        return self.data[specimenname]['prop']

    def SpecimenExists(self, specimenname, casesens=True):
        """Checks if a specimen exists"""
        if casesens:
            # care about case
            return specimenname in self.GetSpecimenList()
        else:
            # do not care about case
            return specimenname.lower() in [s.lower() for s in self.GetSpecimenList()]

    def ExportToFile(self, filename):
        """Export results and properties of all specimens to a file"""

        outfile = open(filename, 'wb')
        # write header
        outfile.write(('# Procedure data exported by CryoMag %s\x0D\x0A' % CryoMagSettings.cryomagversion).encode())
        outfile.write(('\t'.join(('name', 'coreaz', 'coredip', 'bedaz', 'beddip', 'vol', 'weight', 'step', 'type',
                                 'comment', 'time', 'X [Am^2]', 'Y [Am^2]', 'Z [Am^2]', 'M', 'sM', 'a95', 'Dc', 'Ic',
                                 'Dg', 'Ig', 'Ds', 'Is')) + '\x0D\x0A').encode())

        sc = 0
        # write specimen steps
        for spec in self.GetSpecimenList():
            if spec != 'holder':
                sc += 1
                p = self.GetProp(spec)
                for step in self.GetStepData(spec):
                    res = step['results']
                    Dc = res['D']
                    Ic = res['I']

                    xyz = (res['X'], res['Y'], res['Z'])

                    xyzgeo = core2geo(xyz, p['coreaz'], p['coredip'])
                    (Dg, Ig, M) = XYZ2DIL(xyzgeo)

                    xyzbed = geo2bed(xyzgeo, p['bedaz'], p['beddip'])
                    (Ds, Is, M) = XYZ2DIL(xyzbed)

                    outfile.write(('\t'.join((spec, "%.1f" % p['coreaz'], "%.1f" % p['coredip'],
                                             "%.1f" % p['bedaz'], "%.1f" % p['beddip'], "%.1f" % p['vol'],
                                             "%.1f" % p['weight'],
                                             str(step['step']), str(step['type']), str(step['comment']),
                                             str(res['time']),
                                             "%.4E" % res['X'], "%.4E" % res['Y'], "%.4E" % res['Z'], "%.4E" % res['M'],
                                             "%.4E" % res['sM'], "%.3f" % res['a95'],
                                             "%.1f" % Dc, "%.1f" % Ic, "%.1f" % Dg, "%.1f" % Ig, "%.1f" % Ds,
                                             "%.1f" % Is)) + '\x0D\x0A').encode())

        # close file
        outfile.close()

        return sc

    def ExportMeasurementsToFile(self, filename):
        """Export all procedure data to a file"""
        # optionally assume inversed drill direction = z marked the wrong way
        outfile = open(filename, 'wb')
        # write header
        outfile.write(('# Procedure data exported by CryoMag %s\x0D\x0A' % CryoMagSettings.cryomagversion).encode())
        outfile.write(('\t'.join(('name', 'coreaz', 'coredip', 'bedaz', 'beddip', 'vol', 'weight', 'step', 'type',
                                 'comment', 'time', 'mode', 'X [Am^2]', 'Y [Am^2]', 'Z [Am^2]', 'M', 'sM', 'a95', 'Dc',
                                 'Ic', 'Dg', 'Ig', 'Ds', 'Is')) + '\x0D\x0A').encode())

        sc = 0
        # write specimen steps
        for spec in self.GetSpecimenList():
            if spec != 'holder':
                sc += 1
                p = self.GetProp(spec)
                for step in self.GetStepData(spec):
                    # write all measurements, results for each step

                    lines = []
                    # holder results
                    if 'holderresults' in step:
                        lines += [step['holderresults']]
                    lines[-1]['mode'] = 'holder results'
                    # measurements
                    for meas in step['measurements']:
                        lines += [meas['values']]
                        lines[-1]['mode'] = meas['name']
                    # results
                    lines += [step['results']]
                    lines[-1]['mode'] = 'results'

                    # write prepared data to file
                    for res in lines:
                        Dc = res['D']
                        Ic = res['I']

                        xyz = (res['X'], res['Y'], res['Z'])

                        xyzgeo = core2geo(xyz, p['coreaz'], p['coredip'])
                        (Dg, Ig, M) = XYZ2DIL(xyzgeo)

                        xyzbed = geo2bed(xyzgeo, p['bedaz'], p['beddip'])
                        (Ds, Is, M) = XYZ2DIL(xyzbed)

                        outfile.write(('\t'.join((spec, "%.1f" % p['coreaz'], "%.1f" % p['coredip'],
                                                 "%.1f" % p['bedaz'], "%.1f" % p['beddip'], "%.1f" % p['vol'],
                                                 "%.1f" % p['weight'],
                                                 str(step['step']), str(step['type']), str(step['comment']),
                                                 str(res['time']), res['mode'],
                                                 "%.4E" % res['X'], "%.4E" % res['Y'], "%.4E" % res['Z'],
                                                 "%.4E" % res['M'], "%.4E" % res['sM'], "%.3f" % res['a95'],
                                                 "%.1f" % Dc, "%.1f" % Ic, "%.1f" % Dg, "%.1f" % Ig, "%.1f" % Ds,
                                                 "%.1f" % Is)) + '\x0D\x0A').encode())

        # close file
        outfile.close()

        return sc

    def ExportPropToFile(self, filename):
        """Export properties of all specimens to a file"""
        outfile = open(filename, 'wb')
        # write header
        outfile.write(('# Specimen properties exported by CryoMag %s\x0D\x0A' % CryoMagSettings.cryomagversion).encode())
        outfile.write(('\t'.join(('name', 'coreaz', 'coredip', 'bedaz', 'beddip', 'vol ccm', 'weight g')) + '\x0D\x0A').encode())

        sc = 0
        # write specimens
        for spec in self.GetSpecimenList():
            sc += 1
            p = self.GetProp(spec)

            outfile.write(('\t'.join((spec, "%.1f" % p['coreaz'], "%.1f" % p['coredip'], "%.1f" % p['bedaz'],
                                     "%.1f" % p['beddip'], "%.1f" % p['vol'], "%.1f" % p['weight'])) + '\x0D\x0A').encode())

        # close file
        outfile.close()

        return sc

    def ExportToPalmag(self, dirname):
        """Export all specimens into Palmag .dat files in a directory called dirname"""
        sc = 0
        # optionally assume inversed drill direction = z marked the wrong way
        for spec in self.GetSpecimenList():
            self.ExportSpecimenToPalmag(spec, dirname)
            sc += 1

        return sc

    def ExportSpecimenToPalmag(self, specimenname, dirname, invdrilldir=False):
        """Writes the results of a single specimen to a palmag file called specimenname.dat in
        a directory called dirname"""
        # optionally assume inversed drill direction = z marked the wrong way
        outfile = open(os.path.join(dirname, specimenname + '.dat'), 'wb')

        p = self.GetProp(specimenname)
        data = self.GetStepData(specimenname)

        # write first line
        # put type of first step in the header of the file
        steptype = ''
        try:
            steptype = data[0]['type']
        except:
            pass

        # strike = bedaz - 90
        coreaz = p['coreaz']
        coredip = p['coredip']
        # adjust field data, so that PMGSC displays the right values
        #if invdrilldir:
        #    coreaz = (coreaz + 180) % 360
        #    coredip = 180 - coredip

        # write corrected coreaz and coredip, so that conversion from XYZ to geo I,D makes sense also for inverted drill direction
        outfile.write((specimenname.ljust(35) +
                      ("%.1f" % coreaz).rjust(7) + ("%.1f" % coredip).rjust(7) +
                      ("%.1f" % p['bedaz']).rjust(7) + ("%.1f" % p['beddip']).rjust(7) + steptype.rjust(6) + '\x0D\x0A').encode())

        # write one line for each step
        for step in data:
            res = step['results']

            xyz = (res['X'], res['Y'], res['Z'])
            xyz = core2geo(xyz, p['coreaz'], p['coredip'])
            (geoD, geoI, M) = XYZ2DIL(xyz)

            xyz = geo2bed(xyz, p['bedaz'], p['beddip'])
            (bedD, bedI, M) = XYZ2DIL(xyz)

            # 2008-07-09 added conversion to magnetization, a95 in output

            # convert magnetic moment [Am^2] to magnetization [mA/m]
            magnetization = res['M'] / (p['vol'] * 1E-6) * 1E3

            outfile.write((("%d" % int(step['step'])).rjust(4)[:4] + ("%.3E" % magnetization).rjust(11) + (
                    "%.1f" % res['a95']).rjust(6) +
                          ("%.1f" % res['D']).rjust(7) + ("%.1f" % res['I']).rjust(7) +
                          ("%.1f" % geoD).rjust(7) + ("%.1f" % geoI).rjust(7) +
                          ("%.1f" % bedD).rjust(7) + ("%.1f" % bedI).rjust(7) + '\x0D\x0A').encode())

        # close the file when we are done
        outfile.close()

    def ExportToPalmagASC(self, filename):
        """Export all specimens into one Palmag .asc file called filename"""

        outfile = open(filename, 'wb')
        # write header
        # outfile.write('# Procedure data exported by CryoMag %s\x0D\x0A' % CryoMagSettings.cryomagversion

        # count lines
        linenr = 0
        # count specimens
        sc = 0
        # write specimen steps
        for spec in self.GetSpecimenList():
            if spec != 'holder':
                sc += 1
                p = self.GetProp(spec)
                for step in self.GetStepData(spec):
                    # write one line for each step

                    res = step['results']
                    # convert magnetic moment [Am^2] to magnetization [mA/m]
                    magnetization = res['M'] / (p['vol'] * 1E-6) * 1E3

                    # force to two digit exponent
                    magstr = "%.3E" % magnetization
                    if (magstr[-5] == 'E' and magstr[-3] == '0'):
                        magstr = magstr[:-3] + magstr[-2:]

                    outfile.write((
                        ("%3d" % linenr).rjust(3)[:3] + " " + spec.ljust(10)[:10] +
                        ("%2s" % step['type'][:2]) + ("%d" % int(step['step'])).rjust(4)[:4] + "KR" +
                        ("%.2f" % res['a95']).rjust(5) + magstr.rjust(10) +
                        ("%.1f" % res['I']).rjust(7) + ("%.1f" % res['D']).rjust(7) + '\x0D\x0A').encode())

                    linenr += 1
        # close file
        outfile.close()

        return sc

    def ExportToMagICText(self, filename, statetypes={}):
        """Export all specimens into Standard MagIC Text Files
        for format see http://earthref.org/cgi-bin/help.cgi?lib=magic&id=665
        statetypes example: { 'state_temp':['TH', 'TE'], 'state_dc': ['DF'], 'state_ac': ['AF']}"""

        outfile = open(filename, 'wb')

        # first export specimen properties

        # write header
        outfile.write(('tab\tER_Samples' + '\x0D\x0A').encode())
        outfile.write(('\t'.join(('er_sample_name', 'sample_azimuth', 'sample_dip', 'sample_bed_dip_direction',
                                 'sample_bed_dip')) + '\x0D\x0A').encode())

        # write sample data
        for spec in self.GetSpecimenList():
            p = self.GetProp(spec)

            outfile.write(('\t'.join((spec, "%.1f" % p['coreaz'], "%.1f" % p['coredip'], "%.1f" % p['bedaz'],
                                     "%.1f" % p['beddip'])) + '\x0D\x0A').encode())

        # write separator
        outfile.write('>>>>>>>>>>\x0D\x0A'.encode())

        # write header
        outfile.write(('tab\tER_Specimens' + '\x0D\x0A').encode())
        outfile.write(('\t'.join(('er_specimen_name', 'specimen_azimuth', 'specimen_dip', 'specimen_volume',
                                 'specimen_weight')) + '\x0D\x0A').encode())

        # write specimen data
        for spec in self.GetSpecimenList():
            p = self.GetProp(spec)

            # convert to m^3 and kg
            outfile.write(('\t'.join((spec, "%.1f" % p['coreaz'], "%.1f" % p['coredip'], "%.3E" % (p['vol'] * 1e-6),
                                     "%.3E" % (p['weight'] * 1e-3))) + '\x0D\x0A').encode())

        # write separator
        outfile.write(('>>>>>>>>>>\x0D\x0A').encode())

        # write header
        outfile.write(('tab\tMAGIC_Measurements' + '\x0D\x0A').encode())
        outfile.write(('\t'.join(('er_specimen_name', 'procedure_date', 'procedure_positions', 'state_temp',
                                 'state_ac_field', 'state_dc_field', 'procedure_inc', 'procedure_dec',
                                 'procedure_magn_moment',
                                 'procedure_magn_volume', 'procedure_magn_mass', 'procedure_sd')) + '\x0D\x0A').encode())

        sc = 0
        # write specimen steps
        for spec in self.GetSpecimenList():
            if spec != 'holder':
                sc += 1
                p = self.GetProp(spec)
                for step in self.GetStepData(spec):
                    # write all measurements, results for each step

                    # count number of procedure positions for this step
                    nof = 0
                    for s in step['measurements']:
                        if s['name'] != 'baseline':
                            nof += 1

                    # put step in column corresponding to step type
                    t_temp, t_ac, t_dc = '', '', ''

                    try:
                        if step['type'].upper() in statetypes['state_temp']:
                            t_temp = str(step['step'])
                    except:
                        t_temp = ''

                    try:
                        if step['type'].upper() in statetypes['state_ac_field']:
                            t_ac = str(step['step'])
                    except:
                        t_ac = ''

                    try:
                        if step['type'].upper() in statetypes['state_dc_field']:
                            t_dc = str(step['step'])
                    except:
                        t_dc = ''

                    outfile.write(('\t'.join((spec, str(step['results']['time']), str(nof), t_temp, t_ac, t_dc,
                                             "%.1f" % step['results']['I'], "%.1f" % step['results']['D'],
                                             "%.4E" % step['results']['M'],
                                             "%.4E" % (step['results']['M'] / (p['vol'] * 1E-6)),
                                             "%.4E" % (step['results']['M'] / (p['weight'] * 1E-6)),
                                             "%.4E" % step['results']['sM'])) + '\x0D\x0A').encode())

        # close file
        outfile.close()

        return sc

    def ExportToPaleoMag(self, filename):
        """Export all specimens into PaleoMag sample data files (*.0a) and create corrsepondig SAM-file in a directory called dirname"""
        # format description see http://cires.colorado.edu/people/jones.craig/PMag_Formats.html

        # create SAM File
        outfile = open(filename, 'wb')

        # write header
        outfile.write(('CIT' + '\x0D\x0A').encode())
        outfile.write(('Data exported by CryoMag at %s.' % GetTime() + '\x0D\x0A').encode())
        # lat, lon, magn. decl
        outfile.write((
            ("%.1f" % 0.0).rjust(5) + ' ' + ("%.1f" % 0.0).rjust(5) + ' ' + ("%.1f" % 0.0).rjust(5) + '\x0D\x0A').encode())

        # get path from file name
        dirname = os.path.dirname(filename)

        sc = 0
        for spec in self.GetSpecimenList():
            fname = self.ExportSpecimenToPaleoMag(spec, dirname)
            # write list of filenames
            outfile.write((fname + '\x0D\x0A').encode())

            sc += 1

        outfile.close()

        return sc

    def ExportSpecimenToPaleoMag(self, specimenname, dirname):
        """Writes the results of a single specimen to a PaleoMag sample data file called specimenname.0a in
        a directory called dirname"""
        # optionally assume inversed drill direction = z marked the wrong way
        fname = specimenname + '.0a'

        outfile = open(os.path.join(dirname, fname), 'wb')

        p = self.GetProp(specimenname)
        data = self.GetStepData(specimenname)

        # write two header lines

        # loclity id (4), sample_id(9), comment(255)
        outfile.write((''.ljust(4) + specimenname.ljust(9) + specimenname.ljust(200) + '\x0D\x0A').encode())

        # ignored (1), stratigraphic level(6), core strike(6), core dip(6), bedding strike(6), bedding dip(6), core vol (6)
        outfile.write((
            ' ' + ("%.1f" % 0.0).rjust(6) + ("%.1f" % p['coreaz']).rjust(6) + ("%.1f" % p['coredip']).rjust(6) +
            ("%.1f" % p['bedaz']).rjust(6) + ("%.1f" % p['beddip']).rjust(6) + ("%.1f" % p['vol']).rjust(
                6) + '\x0D\x0A').encode())

        # steptype.rjust( 6)
        # write one line for each step
        for step in data:
            res = step['results']

            xyz = (res['X'], res['Y'], res['Z'])
            xyz = core2geo(xyz, p['coreaz'], p['coredip'])
            (geoD, geoI, M) = XYZ2DIL(xyz)

            xyz = geo2bed(xyz, p['bedaz'], p['beddip'])
            (bedD, bedI, M) = XYZ2DIL(xyz)

            # convert magnetic moment [Am^2] to magnetization [emu/cm]
            magnetization = res['M'] / (p['vol'] * 1E-6) * 1E-3

            # type(2), step(4), geoD(6), geoI(6), bedD(6), bedI(6), intensity(emu/cm3)(9), a95(6), core plate decl(6), core plate inc(6), std dev x(8), std dev x(8), std dev x(8)
            outfile.write((step['type'][:2].ljust(2) + ("%d" % int(step['step'])).rjust(4)[:4] +
                          ("%.1f" % geoD).rjust(6) + ("%.1f" % geoI).rjust(6) +
                          ("%.1f" % bedD).rjust(6) + ("%.1f" % bedI).rjust(6) +
                          ("%.2E" % magnetization).rjust(9) + ("%.1f" % res['a95']).rjust(6) +
                          ("%.1f" % res['D']).rjust(6) + ("%.1f" % res['I']).rjust(6) +
                          ("%.4f" % 0).rjust(8) + ("%.4f" % 0).rjust(8) +
                          ("%.4f" % 0).rjust(8) + '\x0D\x0A').encode())
            # todo: add standard deviation of x, y, z

        # close the file when we are done
        outfile.close()

        # return used sample file name
        return fname

    def ExportToPaleoMac(self, dirname):
        """Export all specimens into Paleomac .pmd files in a directory called dirname"""
        # optionally assume inversed drill direction = z marked the wrong way
        sc = 0
        for spec in self.GetSpecimenList():
            self.ExportSpecimenToPaleoMac(spec, dirname)
            sc += 1

        return sc

    def ExportSpecimenToPaleoMac(self, specimenname, dirname, invdrilldir=False):
        """Writes the results of a single specimen to a paleomac file called specimenname.pmd in
        a directory called dirname"""
        # optionally assume inversed drill direction = z marked the wrong way
        outfile = open(os.path.join(dirname, specimenname + '.pmd'), 'wb')

        p = self.GetProp(specimenname)
        data = self.GetStepData(specimenname)

        # put type of first step in the header of the file
        steptype = ''
        try:
            steptype = data[0]['type']
        except:
            pass

        # write first line
        # outfile.write( 'CryoMag %s data file exported to PaleoMac\x0D\x0A' % CryoMagSettings.cryomagversion)
        # put latitude and longitude in the first line (as a comment ?)
        lat = p['lat']
        lon = p['lon']

        if lat == None or lon == None:
            fl = 'automag export'  # latitude and longitude are not defined
        else:
            fl = "%.5f %.5f" % (lat, lon)
        outfile.write((fl + '\x0D\x0A').encode())

        # write second line like this
        # 858       a=160.4   b= 20.0   s= 98.6   d= 57.0   v=11.0E-6m3  09-02-2003 16:16 
        # truncate specimenname to 9 characters

        # strike = bedaz - 90
        coreaz = p['coreaz']
        coredip = p['coredip']
        # adjust field data, so that PMGSC displays the right values
        if invdrilldir:
            coreaz = (coreaz + 180) % 360
            coredip = 180 - coredip

        outfile.write((specimenname[0:9].ljust(10) +
                      ("a=%5.1f" % coreaz).ljust(10) + ("b=%5.1f" % coredip).ljust(10) +
                      ("s=%5.1f" % Deg360(p['bedaz'] - 90.0)).ljust(10) +
                      ("d=%5.1f" % p['beddip']).ljust(10) + ("v=%4.1fE-6m3" % p['vol']).ljust(10) +
                      time.strftime("  %d-%m-%Y %H:%M") + '\x0D\x0A').encode())

        # write third line
        outfile.write(('STEP  Xc (Am2)  Yc (Am2)  Zc (Am2)  MAG(A/m)   Dg    Ig    Ds    Is   a95 \x0D\x0A').encode())

        # write one line for each step
        for step in data:
            res = step['results']

            xyz = (res['X'], res['Y'], res['Z'])
            xyz = core2geo(xyz, p['coreaz'], p['coredip'])
            (geoD, geoI, M) = XYZ2DIL(xyz)

            xyz = geo2bed(xyz, p['bedaz'], p['beddip'])
            (bedD, bedI, M) = XYZ2DIL(xyz)

            # convert magnetic moment [Am^2] to magnetization [A/m]
            magnetization = res['M'] / (p['vol'] * 1E-6)

            # step = first letter of type + first 3 digits of step
            outfile.write((("%1s" % step['type'][0:1]) + ("%03d" % int(step['step']))[:3] +
                          efmt2("%.2E" % res['X']).rjust(10) + efmt2("%.2E" % res['Y']).rjust(10) + efmt2(
                "%.2E" % res['Z']).rjust(10) +
                          efmt2("%.2E" % magnetization).rjust(10) + ("%.1f" % geoD).rjust(6) + ("%.1f" % geoI).rjust(
                6) +
                          ("%.1f" % bedD).rjust(6) + ("%.1f" % bedI).rjust(6) +
                          ("%.1f" % res['a95']).rjust(5) + ' ' + step['comment'] + '\x0D\x0A').encode())
            # TODO / BUG
            # 16.4.2010 exporting comment with Umlaut fails (e.g. pmd)

        # close the file when we are done
        outfile.close()

    def ExportToIAPD(self, dirname):
        """Export all specimens into IAPD .dat files in a directory called dirname"""
        # optionally assume inversed drill direction = z marked the wrong way
        sc = 0
        for spec in self.GetSpecimenList():
            self.ExportSpecimenToIAPD(spec, dirname)
            sc += 1

        return sc

    def ExportSpecimenToIAPD(self, specimenname, dirname):
        """Writes the results of a single specimen to a palmag file called specimenname.dat in
        a directory called dirname"""
        # optionally assume inversed drill direction = z marked the wrong way
        outfile = open(os.path.join(dirname, specimenname + '.dat'), 'wb')

        p = self.GetProp(specimenname)
        data = self.GetStepData(specimenname)

        # write first line
        # sample name - orientation dec, inc - bedding dec, inc - volume - ? - ? 

        outfile.write((specimenname[:8].rjust(8) +
                      ("%.1f" % p['coreaz']).rjust(6) + ("%.1f" % p['coredip']).rjust(6) +
                      ("%.1f" % p['bedaz']).rjust(6) + ("%.1f" % p['beddip']).rjust(6) +
                      ("%.2f" % p['vol']).rjust(8) + ("%d" % 1).rjust(3) +
                      ("%d" % 1).rjust(5) + '\x0D\x0A').encode())

        # write one line for each step
        for step in data:
            res = step['results']

            xyz = (res['X'], res['Y'], res['Z'])
            xyz = core2geo(xyz, p['coreaz'], p['coredip'])
            (geoD, geoI, M) = XYZ2DIL(xyz)

            xyz = geo2bed(xyz, p['bedaz'], p['beddip'])
            (bedD, bedI, M) = XYZ2DIL(xyz)

            # convert magnetic moment [Am^2] to magnetization [mA/m]
            magnetization = res['M'] / (p['vol'] * 1E-6) * 1E3

            # step - int(mA/m) - geodec - geoinc - a95 - coredec - coreinc - comment

            outfile.write((' ' + ("%d" % int(step['step'])).ljust(6) + ("%.4f" % magnetization).ljust(12) +
                          ("%.1f" % geoD).ljust(6) + ("%.1f" % geoI).ljust(6) + ("%.1f" % res['a95']).ljust(6) +
                          ("%.1f" % res['D']).ljust(6) + ("%.1f" % res['I']).ljust(6) + step['comment'] + '\x0D\x0A').encode())

        # close the file when we are done
        outfile.close()

    def DelSpecimenDataFile(self, specimenname):
        """Deletes the physical file for a specimen if it exists
        in the current working directory."""
        try:
            os.remove(specimenname + '.cmag.xml')
        except:
            pass

    def WriteSpecimenDataToFile(self, specimenname):
        """Writes self.proceduredata[ specimenname] as XML to a file called `specimenname + '.cmag.xml'`.
        holder data is always written to the users home directory"""

        root = ElementTree.Element("cryomagdata", version=CryoMagSettings.cryomagversion)

        root.append(ElementTree.Comment(
            'This file was written by CryoMag %s on %s.' % (CryoMagSettings.cryomagversion, GetTime())))

        ca = self.data[specimenname]['prop']['coreaz']
        cd = self.data[specimenname]['prop']['coredip']



        specimen = ElementTree.SubElement(root, "specimen",
                                          name=specimenname,
                                          weight="%.3E" % self.data[specimenname]['prop']['weight'],
                                          coreaz="%.1f" % ca,
                                          coredip="%.1f" % cd,
                                          beddip="%.1f" % self.data[specimenname]['prop']['beddip'],
                                          bedaz="%.1f" % self.data[specimenname]['prop']['bedaz'],
                                          vol="%.3E" % self.data[specimenname]['prop']['vol'])

        for stepdata in self.data[specimenname]['stepdata']:
            step = ElementTree.SubElement(specimen, "step",
                                          step=str(stepdata['step']),
                                          type=stepdata['type'],
                                          comment=stepdata['comment'],
                                          instr=CryoMagSettings.instrname)

            if not specimenname == 'holder':
                ElementTree.SubElement(step, "holder",
                                       X="%.4E" % 0,  # stepdata['holderresults']['X'],
                                       Y="%.4E" % 0,  # stepdata['holderresults']['Y'],
                                       Z="%.4E" % 0,  # stepdata['holderresults']['Z'],
                                       I="%.1f" % 0,  # stepdata['holderresults']['I'],
                                       D="%.1f" % 0,  # stepdata['holderresults']['D'],
                                       M="%.4E" % 0,  # stepdata['holderresults']['M'],
                                       a95="%.2f" % 0,  # stepdata['holderresults']['a95'],
                                       sM="%.4E" % 0,  # stepdata['holderresults']['sM'],
                                       time="")  # stepdata['holderresults']['time'])

            measurements = ElementTree.SubElement(step, "measurements")

            for magmoment in stepdata[ 'measurements']:
                    ElementTree.SubElement( measurements, "magmoment", 
                        type = magmoment['name'], 
                        X    = "%.4E" % magmoment['values']['X'], 
                        Y    = "%.4E" % magmoment['values']['Y'], 
                        Z    = "%.4E" % magmoment['values']['Z'], 
                        I    = "%.1f" % magmoment['values']['I'], 
                        D    = "%.1f" % magmoment['values']['D'], 
                        M    = "%.4E" % magmoment['values']['M'], 
                        sM   = "%.4E" % magmoment['values']['sM'], 
                        a95  = "%.2f" % magmoment['values']['a95'], 
                        time = magmoment['values']['time'],
                        no_readings = "%d" % magmoment['values']['no_readings'])

            ElementTree.SubElement(step, "results",
                                   X="%.4E" % stepdata['results']['X'],
                                   Y="%.4E" % stepdata['results']['Y'],
                                   Z="%.4E" % stepdata['results']['Z'],
                                   I="%.1f" % stepdata['results']['I'],
                                   D="%.1f" % stepdata['results']['D'],
                                   M="%.4E" % stepdata['results']['M'],
                                   sM="%.4E" % stepdata['results']['sM'],
                                   a95="%.2f" % stepdata['results']['a95'],
                                   time=str(stepdata['results']['time']))

        tree = ElementTree.ElementTree(root)

        # open file
        # in case of holder in the user's home directory
        # otherwise in the current directory
        if specimenname == 'holder':
            outfile = open(os.path.join(os.path.expanduser("~"), "holder.cmag.xml"), 'w')
        else:
            outfile = open(specimenname + '.cmag.xml', 'w')

        # write header
        outfile.write('<?xml version="1.0" encoding="UTF-8" ?>\n')
        outfile.write('<?xml-stylesheet type="text/xsl" href="cryomag.xsl" ?>\n')
        # outfile.write('<!DOCTYPE cryomagdata SYSTEM "cryomag.dtd">\n')
        # write xml tree
        tree.write(outfile, encoding='unicode')
        # close file
        outfile.close()

    def LoadSpecimenDataFromFile(self, filename):
        """Load data of a specimen (or multiple) from a xml file called `filename`.<br>
        returns no of loaded specimens"""
        tree = ElementTree.parse(filename)

        speccount = 0

        for specimen in tree.getroot().findall("specimen"):

            # read in specimen properties
            prop = {}

            specimenname = specimen.attrib['name']

            prop['coreaz'] = float(specimen.attrib['coreaz'])
            prop['coredip'] = float(specimen.attrib['coredip'])
            prop['beddip'] = float(specimen.attrib['beddip'])
            prop['bedaz'] = float(specimen.attrib['bedaz'])
            prop['vol'] = float(specimen.attrib['vol'])
            prop['weight'] = float(specimen.attrib['weight'])

            # delete old specimen data and properties
            self.DelSpecimen(specimenname)
            # add new specimen and properties
            self.AddSpecimen(specimenname, prop)

            speccount += 1

            for step in specimen.findall("step"):
                # append all steps from the file
                # wx.MessageBox( str( step.attrib))

                stepdata = {}

                # get data for one step
                stepdata['step'] = step.attrib['step']
                stepdata['type'] = step.attrib['type']
                stepdata['comment'] = step.attrib['comment']

                # read measurements of this step
                stepdata['measurements'] = []
                for magmoment in step.findall("measurements/magmoment"):
                    procedure_data = {}
                    procedure_data['name'] = magmoment.attrib['type']
                    procedure_data['values'] = {}
                    procedure_data['values']['X'] = float(magmoment.attrib['X'])
                    procedure_data['values']['Y'] = float(magmoment.attrib['Y'])
                    procedure_data['values']['Z'] = float(magmoment.attrib['Z'])
                    procedure_data['values']['I'] = float(magmoment.attrib['I'])
                    procedure_data['values']['D'] = float(magmoment.attrib['D'])
                    procedure_data['values']['M'] = float(magmoment.attrib['M'])
                    procedure_data['values']['sM'] = float(magmoment.attrib['sM'])
                    procedure_data['values']['a95'] = float(magmoment.attrib['a95'])
                    procedure_data['values']['time'] = magmoment.attrib['time']

                    # in files before version 1.4 attribute no_readings does not exist
                    # use default of three which was always used
                    try:
                        no_readings = int(magmoment.attrib['no_readings'])
                    except KeyError:
                        procedure_data['values']['no_readings'] = 3
                    else:
                        procedure_data['values']['no_readings'] = no_readings

                    stepdata['measurements'].append(procedure_data)

                results = step.find('results')

                stepdata['results'] = {}
                stepdata['results']['X'] = float(results.attrib['X'])
                stepdata['results']['Y'] = float(results.attrib['Y'])
                stepdata['results']['Z'] = float(results.attrib['Z'])
                stepdata['results']['I'] = float(results.attrib['I'])
                stepdata['results']['D'] = float(results.attrib['D'])
                stepdata['results']['M'] = float(results.attrib['M'])
                stepdata['results']['sM'] = float(results.attrib['sM'])
                stepdata['results']['a95'] = float(results.attrib['a95'])
                stepdata['results']['time'] = results.attrib['time']

                # if we do not have a holder procedure
                # look for holder data that was substracted from the procedure values
                if specimen.attrib['name'] != 'holder':
                    holderresults = step.find('holder')

                    stepdata['holderresults'] = {}
                    stepdata['holderresults']['X'] = float(holderresults.attrib['X'])
                    stepdata['holderresults']['Y'] = float(holderresults.attrib['Y'])
                    stepdata['holderresults']['Z'] = float(holderresults.attrib['Z'])
                    stepdata['holderresults']['I'] = float(holderresults.attrib['I'])
                    stepdata['holderresults']['D'] = float(holderresults.attrib['D'])
                    stepdata['holderresults']['M'] = float(holderresults.attrib['M'])
                    stepdata['holderresults']['sM'] = float(holderresults.attrib['sM'])
                    stepdata['holderresults']['a95'] = float(holderresults.attrib['a95'])
                    stepdata['holderresults']['time'] = holderresults.attrib['time']

                # add stepdata for current step
                self.AddStepData(specimenname, stepdata)

            # return number of read specimen
            return speccount
