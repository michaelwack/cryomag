# Boa:Dialog:CryoMagMeasurementResultsDialog
'''This dialog is displayed after each measurement to summarize the results.'''
# written by Michael Wack 2008

import wx


def create(parent):
    return CryoMagMeasurementResultsDialog(parent)


class CryoMagMeasurementResultsDialog(wx.Dialog):
    def _init_coll_ButtonSizer_Items(self, parent):
        parent.Add(self.SaveButton, 1, border=0, flag=0)
        parent.Add(self.CancelButton, 1, border=0, flag=0)

    def _init_coll_MainBoxSizer_Items(self, parent):
        parent.Add(self.SummaryTextCtrl, 1, border=0, flag=wx.GROW)
        parent.Add(self.ButtonSizer, 0, border=0, flag=wx.GROW)

    def _init_sizers(self):
        self.MainBoxSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self.ButtonSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_MainBoxSizer_Items(self.MainBoxSizer)
        self._init_coll_ButtonSizer_Items(self.ButtonSizer)

        self.SetSizer(self.MainBoxSizer)

    def _init_ctrls(self, prnt):
        wx.Dialog.__init__(self, id=wx.ID_ANY,
                           name=u'CryoMagMeasurementResultsDialog', parent=prnt,
                           pos=wx.Point(163, 107), size=wx.Size(993, 662),
                           style=wx.DEFAULT_DIALOG_STYLE, title=u'Measurement Results')
        self.SetClientSize(wx.Size(985, 635))

        self.SaveButton = wx.Button(id=wx.ID_OK, label=u'Save results',
                                    name=u'SaveButton', parent=self, pos=wx.Point(0, 612),
                                    size=wx.Size(492, 23), style=0)
        self.SaveButton.SetToolTip(u'Save Results')

        self.CancelButton = wx.Button(id=wx.ID_CANCEL, label=u'Cancel',
                                      name=u'CancelButton', parent=self, pos=wx.Point(492, 612),
                                      size=wx.Size(492, 23), style=0)
        self.CancelButton.SetToolTip(u'Cancel')

        self.SummaryTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                           name=u'SummaryTextCtrl', parent=self, pos=wx.Point(0, 0),
                                           size=wx.Size(985, 612), style=wx.TE_MULTILINE | wx.TE_BESTWRAP,
                                           value=u'')
        self.SummaryTextCtrl.SetEditable(False)
        self.SummaryTextCtrl.SetFont(wx.Font(10, wx.FONTFAMILY_TELETYPE, wx.NORMAL, wx.NORMAL, faceName="Monospace"))
        #.SetFont(wx.Font(10, 76, wx.NORMAL, wx.NORMAL, False, u'Courier 10 Pitch'))

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        # put focus on save button, not needed under win32
        self.SaveButton.SetFocus()

    def SetText(self, text, texthighlight):
        self.SummaryTextCtrl.SetValue(text)

        for section in texthighlight:
            self.SummaryTextCtrl.SetStyle(section[0], section[1], wx.TextAttr("red", "white"))

        # TODO: color columns
