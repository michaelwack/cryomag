# part of Cryomag
# written by Michael Wack 2008-2021

import wx
import wx.lib.intctrl
import wx.lib.stattext


def create(parent):
    return CryoMagStepEditDialog(parent)


class CryoMagStepEditDialog(wx.Dialog):
    def _init_coll_DialogboxSizer_Items(self, parent):
        parent.Add(self.panel1, 1, border=0, flag=wx.GROW)
        parent.Add(self.panel2, 0, border=0, flag=wx.GROW)

    def _init_coll_ButtonboxSizer_Items(self, parent):
        parent.Add(self.panel3, 1, border=0, flag=wx.GROW)
        parent.Add(self.OkButton, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.Add(self.panel4, 0, border=0, flag=wx.GROW)
        parent.Add(self.CancelButton, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.Add(self.panel5, 1, border=0, flag=wx.GROW)

    def _init_sizers(self):
        self.DialogboxSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self.ButtonboxSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_DialogboxSizer_Items(self.DialogboxSizer)
        self._init_coll_ButtonboxSizer_Items(self.ButtonboxSizer)

        self.SetSizer(self.DialogboxSizer)
        self.panel2.SetSizer(self.ButtonboxSizer)

    def _init_ctrls(self, prnt):
        wx.Dialog.__init__(self, id=wx.ID_ANY,
                           name=u'CryoMagStepEditDialog', parent=prnt, pos=wx.Point(517,
                                                                                    353), size=wx.Size(416, 156),
                           style=wx.DEFAULT_DIALOG_STYLE,
                           title=u'Step Edit')
        self.SetClientSize(wx.Size(408, 129))

        self.panel1 = wx.Panel(id=wx.ID_ANY,
                               name='panel1', parent=self, pos=wx.Point(0, 0), size=wx.Size(408,
                                                                                            89), style=wx.TAB_TRAVERSAL)

        self.panel2 = wx.Panel(id=wx.ID_ANY,
                               name='panel2', parent=self, pos=wx.Point(0, 89), size=wx.Size(408,
                                                                                             40),
                               style=wx.TAB_TRAVERSAL)

        self.StepintCtrl = wx.lib.intctrl.IntCtrl(allow_long=False,
                                                  allow_none=False, default_color=wx.BLACK,
                                                  id=wx.ID_ANY, limited=False, max=None,
                                                  min=None, name=u'StepintCtrl', oob_color=wx.RED,
                                                  parent=self.panel1, pos=wx.Point(16, 56), size=wx.Size(56, 21),
                                                  style=0, value=0)

        self.TypetextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                        name=u'TypetextCtrl', parent=self.panel1, pos=wx.Point(88, 56),
                                        size=wx.Size(56, 21), style=0, value=u'')

        self.CommentstaticText = wx.StaticText(id=wx.ID_ANY,
                                               label=u'comment', name=u'CommentstaticText', parent=self.panel1,
                                               pos=wx.Point(168, 32), size=wx.Size(120, 16), style=0)

        self.CommenttextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                           name=u'CommenttextCtrl', parent=self.panel1, pos=wx.Point(168,
                                                                                                     56),
                                           size=wx.Size(224, 21), style=0, value=u'')

        self.StepstaticText = wx.StaticText(id=wx.ID_ANY,
                                            label=u'step', name=u'StepstaticText', parent=self.panel1,
                                            pos=wx.Point(16, 32), size=wx.Size(40, 16), style=0)

        self.TypestaticText = wx.StaticText(id=wx.ID_ANY,
                                            label=u'type', name=u'TypestaticText', parent=self.panel1,
                                            pos=wx.Point(88, 32), size=wx.Size(40, 16), style=0)

        self.panel3 = wx.Panel(id=wx.ID_ANY,
                               name='panel3', parent=self.panel2, pos=wx.Point(0, 0),
                               size=wx.Size(109, 40), style=0)

        self.OkButton = wx.Button(id=wx.ID_OK, label=u'Ok', name=u'OkButton',
                                  parent=self.panel2, pos=wx.Point(109, 8), size=wx.Size(75, 23),
                                  style=0)
        #self.OkButton.SetToolTip(u'')

        self.panel4 = wx.Panel(id=wx.ID_ANY,
                               name='panel4', parent=self.panel2, pos=wx.Point(184, 0),
                               size=wx.Size(39, 40), style=0)

        self.CancelButton = wx.Button(id=wx.ID_CANCEL, label=u'Cancel',
                                      name=u'CancelButton', parent=self.panel2, pos=wx.Point(223, 8),
                                      size=wx.Size(75, 23), style=0)
        #self.CancelButton.SetToolTip(u'')

        self.panel5 = wx.Panel(id=wx.ID_ANY,
                               name='panel5', parent=self.panel2, pos=wx.Point(298, 0),
                               size=wx.Size(109, 40), style=0)

        self.staticText1 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'Please enter modified values for the selected step.',
                                         name='staticText1', parent=self.panel1, pos=wx.Point(16, 8),
                                         size=wx.Size(244, 13), style=0)
        #self.staticText1.SetToolTip(u'')

        self._init_sizers()

    def __init__(self, parent, step, type, comment):
        self._init_ctrls(parent)
        self.StepintCtrl.SetValue(int(step))
        self.TypetextCtrl.SetValue(type)
        self.CommenttextCtrl.SetValue(comment)

    def GetResults(self):
        return {'step': str(self.StepintCtrl.GetValue()), 'type': self.TypetextCtrl.GetValue(),
                'comment': self.CommenttextCtrl.GetValue()}
