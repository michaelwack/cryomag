"""Measurement Panel. Handles processing of measurement."""

import math
import sys

import wx
import wx.adv

import CryoComm
import CryoMagSettings
from CryoMagAux import *
from CryoMagMeasurementResultsDialog import CryoMagMeasurementResultsDialog


class CryoMagMeasurementPanel(wx.Panel):
    def _init_coll_boxSizer1_Items(self, parent):

        parent.Add(self.TopPanel, 0, border=0, flag=wx.GROW)
        parent.Add(self.MeasurementListCtrl, 1, border=0, flag=wx.GROW)

    def _init_coll_MeasurementListCtrl_Columns(self, parent):

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'No',
                            width=25)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Measurement', width=100)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                            heading=u'X [Am\xb2]', width=70)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Y [Am\xb2]', width=70)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Z [Am\xb2]', width=70)
        parent.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT,
                            heading=u'M [Am\xb2]', width=70)
        parent.InsertColumn(col=6, format=wx.LIST_FORMAT_LEFT, heading=u'sM',
                            width=70)
        parent.InsertColumn(col=7, format=wx.LIST_FORMAT_LEFT,
                            heading=u'a95 [\xb0]', width=50)
        parent.InsertColumn(col=8, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Dc [\xb0]', width=50)
        parent.InsertColumn(col=9, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Ic [\xb0]', width=50)

    def _init_sizers(self):

        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):

        wx.Panel.__init__(self, id=wx.ID_ANY,
                          name=u'CryoMagMeasurementPanel', parent=prnt, pos=wx.Point(167,
                                                                                     283), size=wx.Size(645, 440),
                          style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(637, 413))
        self.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD, False, u'Sans'))

        self.TopPanel = wx.Panel(id=wx.ID_ANY,
                                 name=u'TopPanel', parent=self, pos=wx.Point(0, 0),
                                 size=wx.Size(637, 109), style=0)
        self.TopPanel.Bind(wx.EVT_CHAR, self.OnTopPanelChar)

        self.MsgTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                       name=u'MsgTextCtrl', parent=self.TopPanel, pos=wx.Point(40, 24),
                                       size=wx.Size(352, 56), style=wx.TE_MULTILINE,
                                       value=u'measuring baseline ... hit enter when ready')
        self.MsgTextCtrl.SetEditable(False)

        self.staticText1 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'specimen', name='staticText1', parent=self.TopPanel,
                                         pos=wx.Point(7, 88), size=wx.Size(65, 18), style=0)

        self.SpecimenComboBox = wx.ComboBox(id=wx.ID_ANY,
                                            name=u'SpecimenComboBox', parent=self.TopPanel, pos=wx.Point(71, 88),
                                            size=wx.Size(129, 21),
                                            style=wx.TE_PROCESS_ENTER | wx.CB_DROPDOWN, value=u'AD234')
        self.SpecimenComboBox.Enable(False)
        self.SpecimenComboBox.Bind(wx.EVT_CHAR, self.OnSpecimenComboBoxChar)

        self.StepTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                        name=u'StepTextCtrl', parent=self.TopPanel, pos=wx.Point(241, 88),
                                        size=wx.Size(45, 21), style=wx.TE_PROCESS_ENTER, value=u'100')
        self.StepTextCtrl.Enable(False)
        self.StepTextCtrl.SetMinSize(wx.Size(45, 21))
        self.StepTextCtrl.Bind(wx.EVT_TEXT_ENTER, self.OnStepTextCtrlTextEnter,
                               id=wx.ID_ANY)
        self.StepTextCtrl.Bind(wx.EVT_TEXT, self.OnStepTextCtrlText,
                               id=wx.ID_ANY)

        self.staticText2 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'step', name='staticText2', parent=self.TopPanel,
                                         pos=wx.Point(204, 89), size=wx.Size(35, 18), style=0)

        self.MeasurementListCtrl = wx.ListCtrl(id=wx.ID_ANY,
                                               name=u'MeasurementListCtrl', parent=self, pos=wx.Point(0, 109),
                                               size=wx.Size(637, 304), style=wx.LC_REPORT)
        self._init_coll_MeasurementListCtrl_Columns(self.MeasurementListCtrl)
        self.MeasurementListCtrl.Bind(wx.EVT_CHAR,
                                      self.OnMeasurementListCtrlChar)

        self.TypeTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                        name=u'TypeTextCtrl', parent=self.TopPanel, pos=wx.Point(336, 88),
                                        size=wx.Size(72, 21), style=wx.TE_PROCESS_ENTER, value=u'AF')
        self.TypeTextCtrl.Enable(False)
        self.TypeTextCtrl.Bind(wx.EVT_TEXT_ENTER, self.OnTypeTextCtrlTextEnter,
                               id=wx.ID_ANY)

        self.typestatictext = wx.StaticText(id=wx.ID_ANY,
                                            label=u'type', name=u'typestatictext', parent=self.TopPanel,
                                            pos=wx.Point(293, 88), size=wx.Size(40, 18), style=0)

        self.coreazstaticText = wx.StaticText(id=wx.ID_ANY,
                                              label=u'coreaz', name=u'coreazstaticText', parent=self.TopPanel,
                                              pos=wx.Point(400, 16), size=wx.Size(42, 16), style=0)

        self.staticText4 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'coredip', name='staticText4', parent=self.TopPanel,
                                         pos=wx.Point(495, 16), size=wx.Size(49, 16), style=0)

        self.staticText5 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'beddip', name='staticText5', parent=self.TopPanel,
                                         pos=wx.Point(495, 40), size=wx.Size(48, 16), style=0)

        self.staticText6 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'bedaz', name='staticText6', parent=self.TopPanel,
                                         pos=wx.Point(400, 40), size=wx.Size(48, 16), style=0)

        self.staticText7 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'vol', name='staticText7', parent=self.TopPanel,
                                         pos=wx.Point(400, 64), size=wx.Size(40, 16), style=0)

        self.staticText8 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'weight', name='staticText8', parent=self.TopPanel,
                                         pos=wx.Point(495, 64), size=wx.Size(48, 16), style=0)

        self.beddiptextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                          name=u'beddiptextCtrl', parent=self.TopPanel, pos=wx.Point(544, 36),
                                          size=wx.Size(40, 21), style=0, value=u'0')
        self.beddiptextCtrl.Enable(False)

        self.voltextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                       name=u'voltextCtrl', parent=self.TopPanel, pos=wx.Point(448, 60),
                                       size=wx.Size(40, 21), style=0, value=u'0')
        self.voltextCtrl.Enable(False)

        self.corediptextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                           name=u'corediptextCtrl', parent=self.TopPanel, pos=wx.Point(544, 12),
                                           size=wx.Size(40, 21), style=0, value=u'0')
        self.corediptextCtrl.Enable(False)

        self.bedaztextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                         name=u'bedaztextCtrl', parent=self.TopPanel, pos=wx.Point(448, 36),
                                         size=wx.Size(40, 21), style=0, value=u'0')
        self.bedaztextCtrl.Enable(False)

        self.weighttextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                          name=u'weighttextCtrl', parent=self.TopPanel, pos=wx.Point(544,
                                                                                                     60),
                                          size=wx.Size(40, 21), style=0, value=u'0')
        self.weighttextCtrl.Enable(False)

        self.coreaztextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                          name=u'coreaztextCtrl', parent=self.TopPanel, pos=wx.Point(448,
                                                                                                     12),
                                          size=wx.Size(40, 21), style=0, value=u'0')
        self.coreaztextCtrl.Enable(False)

        self.commentstaticText = wx.StaticText(id=wx.ID_ANY,
                                               label=u'comment', name=u'commentstaticText', parent=self.TopPanel,
                                               pos=wx.Point(420, 88), size=wx.Size(65, 16), style=0)

        self.CommentTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                           name=u'CommentTextCtrl', parent=self.TopPanel, pos=wx.Point(488, 88),
                                           size=wx.Size(120, 21), style=wx.TE_PROCESS_ENTER, value=u'')
        self.CommentTextCtrl.Enable(False)
        self.CommentTextCtrl.Bind(wx.EVT_TEXT_ENTER, self.OnCommentTextCtrlTextEnter, id=wx.ID_ANY)

        self._init_sizers()

    def __init__(self, parent):
        """the constructor"""
        self._init_ctrls(parent)

        # init logfile

        # self.logfile = open('cryolog.txt', 'wa')

        # init default values for step and specimen text box
        self.laststep = '0'
        self.lasttype = 'AF'
        self.lastspecimen = 'XXX'
        # specifies the number of holder measurements
        self.holdermeasno = 0

        # ask if there is no valid holder data
        self.askforholder = True

        # open sound file in script directory
        self.ping = wx.adv.Sound(sys.path[0] + '/cork_pop.wav')

        # initialize communication with 2G Cryogenic Magnetometer
        # use first serial port by default
        # self.cryo = CryoComm.CryoComm( logfunction = self.logger)

        if CryoMagSettings.measmode == 'C3':  # cryo mode (C3) 
            self.cryo = CryoComm.CryoComm(port=CryoMagSettings.serialport)
        elif CryoMagSettings.measmode == 'C2':  # spinner mode (C2)
            self.spinner = CryoComm.SpinnerComm(port=CryoMagSettings.serialport)

        # indicates if the cryogenic has to be reset before next measurement
        # self.InitCryo = False

    def ShowMessage(self, msg):
        """Show a text in MsgTextCtrl"""
        self.MsgTextCtrl.SetValue(msg)

    def StartHolderMeasurement(self):
        """start the measurement of a holder"""

        self.MeasurementMode = 'holder'

        self.MakeMeasurementList(holder=True)

        # skip specimen and step
        self.SpecimenComboBox.Disable()
        self.SpecimenComboBox.SetValue('holder')
        self.StepTextCtrl.Disable()
        self.StepTextCtrl.SetValue(str(self.holdermeasno))

        self.TypeTextCtrl.Disable()
        self.TypeTextCtrl.SetValue('')

        self.CommentTextCtrl.Disable()
        self.CommentTextCtrl.SetValue('')

        self.StartMeasurement()

        # go directly to the measurement list
        self.MeasurementListCtrl.SetFocus()

    def StartSpecimenMeasurement(self):
        """start the measurement of a specimen"""
        if CryoMagSettings.measmode == 'C2':
            # in spinner mode do not ask for holder measurement
            self.askforholder = False

        if wx.GetApp().GetTopWindow().validholder == False and self.askforholder == True:
            dlg = wx.MessageDialog(self, 'Do you want to measure a holder first?', 'No valid holder data.',
                                   wx.YES_NO | wx.NO_DEFAULT | wx.ICON_EXCLAMATION)
            # if user clicked no -> do nothing
            if dlg.ShowModal() == wx.ID_YES:
                dlg.Destroy()
                self.StartHolderMeasurement()
                return
            else:
                dlg.Destroy()
                self.askforholder = False

        self.MeasurementMode = 'specimen'

        self.ShowMessage("Enter specimen name and press ENTER")

        # fill list with all specimens measured up to now except the holder
        self.SpecimenComboBox.Clear()
        sl = []
        for s in wx.GetApp().GetTopWindow().specimens.GetSpecimenList():
            if s != 'holder':
                sl.append(s)
        self.SpecimenComboBox.AppendItems(sl)

        # set step, type and specimen
        self.SpecimenComboBox.SetValue(self.lastspecimen)

        self.StepTextCtrl.SetValue(self.laststep)
        self.TypeTextCtrl.SetValue(self.lasttype)
        self.CommentTextCtrl.SetValue('')

        # activate and set focus to Specimen Text Box
        self.SpecimenComboBox.Enable()
        self.SpecimenComboBox.SetFocus()
        # workaround for SelectAll which does not select the last character under Linux
        self.SpecimenComboBox.SetTextSelection(0, len(self.SpecimenComboBox.GetValue()))

    def StartMeasurement(self):
        """start a measurement"""

        # check if step is valid otherwise assume 0
        if self.StepTextCtrl.GetValue().strip() == '':
            self.StepTextCtrl.SetValue('0')

        # check if specimen exists, if not throw out a warning

        if not wx.GetApp().GetTopWindow().specimens.SpecimenExists(self.SpecimenComboBox.GetValue()):
            # specimen not found in specimen list
            dlg = wx.MessageDialog(self, 'The specimen name is unknown. Add to specimen list?',
                                   'New specimen',
                                   wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION
                                   )
            if dlg.ShowModal() == wx.ID_YES:

                # add specimen to specimen data list and fill with default properties
                wx.GetApp().GetTopWindow().specimens.AddSpecimen(self.SpecimenComboBox.GetValue())

                # update list of specimens in specimens panel
                wx.GetApp().GetTopWindow().FunctionPanels['specimens'].UpdateSpecimens()

            else:
                self.SpecimenComboBox.SetFocus()
                # workaround for SelectAll which does not select the last character under Linux
                self.SpecimenComboBox.SetMark(0, len(self.SpecimenComboBox.GetValue()))
                self.ShowMessage("Enter specimen name and press ENTER")

            dlg.Destroy()

        # lock input fields
        self.SpecimenComboBox.Disable()
        self.StepTextCtrl.Disable()
        self.TypeTextCtrl.Disable()
        self.CommentTextCtrl.Disable()

        # clear list of measurements
        self.MeasurementListCtrl.DeleteAllItems()

        if CryoMagSettings.measmode == 'C3':  # cryo mode (C3)
            i = 0  # in cryo mode: first baseline = 0. measurement
        else:
            i = 1  # in spinner mode: Position one should be measurement one #### added 2013-05-24

        for m in self.measurements:
            self.MeasurementListCtrl.Append((str(i), m['name']))
            i = i + 1

        # number of next measurement
        self.measurement = 0
        self.max_measurement = 0

        # indicates if all needed measurements for one specimen are finished
        self.MeasurementsDone = False

        # Init magnetometer / reset counters
        # self.cryo.Init()
        # self.InitCryo = True

        # 22.05.2008
        # send reset command to cryo to avoid flux jump
        if CryoMagSettings.measmode == 'C3':  # cryo mode (C3)
            self.cryo.SendCommand('ACLP')

        # select first measurement
        self.SelectMeasurement(0)

        # show plots of previous data of specimen
        if wx.GetApp().GetTopWindow().specimens.SpecimenExists(self.SpecimenComboBox.GetValue()):
            wx.GetApp().GetTopWindow().ShowSpecimenData(self.SpecimenComboBox.GetValue(), False)

    def MakeMeasurementList(self, holder=False):
        """Prepare measurement list"""
        self.measurements = []

        if CryoMagSettings.measmode == 'C2':  # spinner mode (C2)
            if holder:
                for i in range(3):
                    self.measurements.append({'name': 'holder %d' % i})
            else:
                mps = wx.GetApp().GetTopWindow().FunctionPanels['settings'].GetSelectedMeasurementPositions()
                i = 0
                for mp in mps:
                    i = i + 1
                    self.measurements.append({'name': 'Pos %d (%s, %s)' % (i, mp[0], mp[1]), 'mp': mp})

        if CryoMagSettings.measmode == 'C3':  # cryo mode (C3)

            if holder:
                mps = wx.GetApp().GetTopWindow().FunctionPanels['settings'].GetHolderMeasurementPositions()
            else:
                mps = wx.GetApp().GetTopWindow().FunctionPanels['settings'].GetSelectedMeasurementPositions()

            # baseline at the beginning and end of all measurements
            if wx.GetApp().GetTopWindow().FunctionPanels['settings'].beforeandafterradioButton.GetValue() == True:
                self.measurements.append({'name': 'baseline'})
                i = 0
                for mp in mps:
                    i = i + 1
                    # e.g. 'Pos( 0, 180, 90)', rotation matrix

                    if holder:
                        self.measurements.append(
                            {'name': 'Pos %d (%d, %d, %d)' % (i, mp[0], mp[1], mp[2]), 'rotmat': CreateRotMat(mp)})
                    else:
                        # get holder orientation corresponding to measurement position
                        if holder:
                            # don't care about holder orientation when we measure the holder itself
                            hp = [0, 0, 0]
                        else:
                            hp = wx.GetApp().GetTopWindow().FunctionPanels[
                                'settings'].GetSelectedMeasurementPositionsHolder()[i - 1]
                        self.measurements.append(
                            {'name': 'Pos %d (%d, %d, %d)' % (i, mp[0], mp[1], mp[2]), 'rotmat': CreateRotMat(mp),
                             'holder_rotmat': CreateRotMat(hp)})

                self.measurements.append({'name': 'baseline'})

            # baseline before each individual measurement
            else:
                i = 0

                for mp in mps:
                    i = i + 1
                    self.measurements.append({'name': 'baseline'})
                    # get holder orientation corresponding to measurement position
                    if holder:
                        # don't care about holder orientation when we measure the holder itself
                        hp = [0, 0, 0]
                    else:
                        hp = \
                            wx.GetApp().GetTopWindow().FunctionPanels[
                                'settings'].GetSelectedMeasurementPositionsHolder()[
                                i - 1]
                    self.measurements.append(
                        {'name': 'Pos %d (%d, %d, %d)' % (i, mp[0], mp[1], mp[2]), 'rotmat': CreateRotMat(mp),
                         'holder_rotmat': CreateRotMat(hp)})

    def SelectMeasurement(self, measurement=-1):
        """ select next (-1) or specific measurement"""

        if measurement == -1:
            self.measurement = self.measurement + 1
        else:
            self.measurement = measurement

        if self.measurement >= len(self.measurements):
            # we reached the end of our list --> all done
            self.MeasurementsDone = True

            self.ShowMessage(
                "Measurements done.\nPress ENTER to store data and proceed to next specimen or enter number of measurement to repeat.")
        else:
            self.ShowMessage(
                "Ready to measure %s.\nPress ENTER when ready." % self.measurements[self.measurement]['name'])

        # select only one item
        for i in range(self.MeasurementListCtrl.GetItemCount()):
            self.MeasurementListCtrl.Select(i, i == self.measurement)

        # keep track of maximum measurement, to allow repeat only for already measured positions
        if self.max_measurement < self.measurement:
            self.max_measurement = self.measurement

    def Measure(self):
        """measure and go on to first or next measurement
        insert results in MeasurementListCtrl"""

        if self.measurement < len(self.measurements):
            if CryoMagSettings.measmode == 'C3':  # cryo mode (C3)
                # read magmom from cryo magnetometer
                # magmom = self.cryo.ReadMagMoment( show_progress = True, numval = CryoMagSettings.num_readings)
                # 10-03-09 number of reading to the squids can be specified through settings panel
                magmom = self.cryo.ReadMagMoment(show_progress=True, numval=wx.GetApp().GetTopWindow().FunctionPanels[
                    'settings'].noreadspinCtrl.GetValue())

                # put focus back to the measurement panel (not needed under win32)
                # self.SetFocus()
                self.MeasurementListCtrl.SetFocus()

                x, y, z = magmom['X'], magmom['Y'], magmom['Z']

                # if we are not measuring a baseline, we subtract the last one
                if 'rotmat' in self.measurements[self.measurement]:
                    # find baseline before this measurement
                    for bl1 in range(self.measurement)[::-1]:  # ::-1 inverses the list order
                        if not 'rotmat' in self.measurements[bl1]:
                            break

                    # subtract baseline
                    x -= self.measurements[bl1]['values']['X']
                    y -= self.measurements[bl1]['values']['Y']
                    z -= self.measurements[bl1]['values']['Z']

                    # if we measure a specimen we have to subtract the magnetic moment of the last holder measurement
                    if self.MeasurementMode == 'specimen':
                        # use holder data from last measurement or zero if not measured
                        holderresults = wx.GetApp().GetTopWindow().GetCurrentHolderResults()

                        # holder is fixed
                        # hx = holderresults['X']
                        # hy = holderresults['Y']
                        # hz = holderresults['Z']

                        # rotate holder data according to specified holder orientation (see CryoMagSettings)
                        (hx, hy, hz) = RotateVector((holderresults['X'], holderresults['Y'], holderresults['Z']),
                                                    self.measurements[self.measurement]['holder_rotmat'])

                        # wx.MessageBox( 'hx: %f, hy: %f, hz: %f')

                        # subtract holder
                        x -= hx
                        y -= hy
                        z -= hz

                    # rotate vector according to measurement position
                    (x, y, z) = RotateVector((x, y, z), self.measurements[self.measurement]['rotmat'])

                # show rotated components
                self.MeasurementListCtrl.SetItem(self.measurement, 2, str("% .2E" % x))
                self.MeasurementListCtrl.SetItem(self.measurement, 3, str("% .2E" % y))
                self.MeasurementListCtrl.SetItem(self.measurement, 4, str("% .2E" % z))

                (dec, inc, M) = XYZ2DIL((x, y, z))
                # show dec, inc and total moment of rotated data
                self.MeasurementListCtrl.SetItem(self.measurement, 5, str("% .2E" % M))
                self.MeasurementListCtrl.SetItem(self.measurement, 8, str("% .0f" % dec))
                self.MeasurementListCtrl.SetItem(self.measurement, 9, str("% .0f" % inc))

                # angular error
                a95 = magmom['a95']
                # stddev of magnetic moment
                sM = magmom['sM']

                self.MeasurementListCtrl.SetItem(self.measurement, 6, str("% .2E" % sM))
                self.MeasurementListCtrl.SetItem(self.measurement, 7, str("% .1f" % a95))

                self.ping.Play(wx.adv.SOUND_ASYNC)

                # keep the rotated components
                self.measurements[self.measurement]['values'] = {}
                self.measurements[self.measurement]['values']['X'] = x
                self.measurements[self.measurement]['values']['Y'] = y
                self.measurements[self.measurement]['values']['Z'] = z
                self.measurements[self.measurement]['values']['D'] = dec
                self.measurements[self.measurement]['values']['I'] = inc
                self.measurements[self.measurement]['values']['M'] = M
                self.measurements[self.measurement]['values']['a95'] = a95
                self.measurements[self.measurement]['values']['sM'] = sM
                self.measurements[self.measurement]['values']['time'] = GetTime()
                self.measurements[self.measurement]['values']['no_readings'] = magmom['no_readings']

                # if we did a baseline measurement, check the difference to the last one and
                # throw out a warning when the difference in moment exceeds the defined threshold

                if not 'rotmat' in self.measurements[self.measurement]:
                    # find baseline before this measurement
                    # go backwards through measurements, skip actual (last) measurement
                    for mm in self.measurements[:self.measurement][::-1]:
                        if not 'rotmat' in mm:
                            bl = mm

                    # calculate baseline difference

                    try:
                        bdiff = math.fabs(bl['values']['M'] - self.measurements[self.measurement]['values']['M'])
                    except NameError:
                        # in case that there was no baseline before and bl is not defined therefore
                        # we simply return and do nothing

                        # 18.3.2010 added "True", otherwise measurement with Cryo was stuck at first baseline
                        return True

                    # if we are still running, check bldiff and throw the warning if appropriate

                    # get threshold
                    a = wx.GetApp().GetTopWindow().FunctionPanels['settings'].thresSpinCtrl1.GetValue()
                    b = wx.GetApp().GetTopWindow().FunctionPanels['settings'].thresSpinCtrl2.GetValue()

                    if bdiff * 1.00 > (a * 10 ** b):
                        # TODO: message box screws the focus and pressing enter to store the data fails in winX
                        # Message dialog with proper parent works fine
                        wx.MessageDialog(self, u'Difference to last baseline is %E Am\xb2.' % bdiff, 'Baseline Warning',
                                         wx.ICON_INFORMATION)


            elif CryoMagSettings.measmode == 'C2':  # spinner mode (C2)        
                magmom = self.spinner.ReadMagMoment2C(show_progress=True)

                # put focus back to the measurement panel (not needed under win32)
                # self.SetFocus()
                self.MeasurementListCtrl.SetFocus()

                # if measurement was aborted --> return
                if magmom == False:
                    return

                mp = self.measurements[self.measurement]['mp']
                # determine measured components according to measurement position
                # mp something like ['+X', '+Y'] as configured in CryoMagSettings.py

                a, b = magmom['A'], magmom['B']
                # TODO: subtract holder?
                # sign change by electronic?
                # a -= holder
                # b -= holder

                # TODO: check mp for validity

                magmom['X'] = float('nan')
                magmom['Y'] = float('nan')
                magmom['Z'] = float('nan')

                if mp[0][0] == '-':
                    a = a * -1
                if mp[1][0] == '-':
                    b = b * -1

                magmom[mp[0][1]] = a
                magmom[mp[1][1]] = b

                # show components in list
                self.MeasurementListCtrl.SetStringItem(self.measurement, 2, "% .2E" % magmom['X'])
                self.MeasurementListCtrl.SetStringItem(self.measurement, 3, "% .2E" % magmom['Y'])
                self.MeasurementListCtrl.SetStringItem(self.measurement, 4, "% .2E" % magmom['Z'])

                # make "ping"
                self.ping.Play(wx.SOUND_ASYNC)

                # keep the components
                self.measurements[self.measurement]['values'] = {}
                self.measurements[self.measurement]['values']['X'] = magmom['X']
                self.measurements[self.measurement]['values']['Y'] = magmom['Y']
                self.measurements[self.measurement]['values']['Z'] = magmom['Z']
                self.measurements[self.measurement]['values']['D'] = float('nan')
                self.measurements[self.measurement]['values']['I'] = float('nan')
                self.measurements[self.measurement]['values']['M'] = float('nan')
                self.measurements[self.measurement]['values']['a95'] = float('nan')
                self.measurements[self.measurement]['values']['sM'] = float('nan')
                self.measurements[self.measurement]['values']['time'] = GetTime()
                self.measurements[self.measurement]['values']['no_readings'] = 1

        # return True on success
        return True

    def OnMeasurementListCtrlChar(self, event):
        """Handle key presses in MeasurementListCtrl"""
        if self.MeasurementMode == 'specimen':
            # preserve specimen for the next time
            self.lastspecimen = self.SpecimenComboBox.GetValue()

            # preserve step for the next time
            self.laststep = self.StepTextCtrl.GetValue()

            # preserve type for the next time
            self.lasttype = self.TypeTextCtrl.GetValue()

        kc = event.GetKeyCode()
        if kc == wx.WXK_RETURN:
            # get values
            if self.Measure():
                if self.MeasurementsDone == False:
                    # go to next measurement
                    self.SelectMeasurement()
                else:
                    if self.measurement == len(self.measurements):

                        # measurement finished ->

                        specimenname = self.SpecimenComboBox.GetValue()

                        # get properties of the specimen
                        specprop = wx.GetApp().GetTopWindow().specimens.GetProp(specimenname)

                        dlg = CryoMagMeasurementResultsDialog(self)

                        # calculate results out of the measured values
                        if CryoMagSettings.measmode == 'C3':  # cryo mode (C3)        
                            results = self.ProcessMeasurements3C()
                        elif CryoMagSettings.measmode == 'C2':  # spinner mode (C2)        
                            results = self.ProcessMeasurements2C()

                        texthighlight = []

                        if self.MeasurementMode == 'holder':
                            report = 'Holder Measurement. The measured magnetic moment will be subtracted from all subsequent measurements!\n'

                        else:
                            # TODO: append weight, volume
                            report = "%s\n" % specimenname

                        texthighlight.append((0, len(report)))

                        # write header for report window
                        report += "%5s%5s%10s%10s%10s%10s%13s%9s%8s%8s" % (
                            u"step", u"type", u"X [Am\xb2]", u"Y [Am\xb2]", u"Z [Am\xb2]", u"M [Am\xb2]",
                            u"sM [Am\xb2]",
                            u"a95 [\xb0]", u"Dc [\xb0]", u"Ic [\xb0]")

                        # if we want to display specimen data, append direction in geographic and bedding corrected coordinates
                        if self.MeasurementMode == 'specimen':
                            report += "%8s%8s%8s%8s" % (u"Dg [\xb0]", u"Ig [\xb0]", u"Ds [\xb0]", u"Is [\xb0]")

                        report += "\n\nactual result:\n"
                        pos1 = len(report)

                        # append line with actual results
                        report += self.MakeReportLine(self.StepTextCtrl.GetValue(), self.TypeTextCtrl.GetValue(),
                                                      results, specprop, self.MeasurementMode == 'holder')

                        # highlight first line with actual result
                        # deactivated 18-06-2008
                        # texthighlight.append( (pos1, len( report)))                   

                        # append data from previous steps if appropriate
                        if len(wx.GetApp().GetTopWindow().specimens.GetStepData(specimenname)) > 0:
                            report += "\n\nprevious results:\n"
                            for stepdata in wx.GetApp().GetTopWindow().specimens.GetStepData(specimenname):
                                res = stepdata['results']
                                report += self.MakeReportLine(stepdata['step'], stepdata['type'], res, specprop,
                                                              self.MeasurementMode == 'holder')

                            pos1 = len(report)

                            # append actual measurement as the last step
                            report += self.MakeReportLine(self.StepTextCtrl.GetValue(), self.TypeTextCtrl.GetValue(),
                                                          results, specprop, self.MeasurementMode == 'holder')

                            texthighlight.append((pos1, len(report)))

                            # display subtracted holder data
                        if self.MeasurementMode == 'specimen':
                            holderresults = wx.GetApp().GetTopWindow().GetCurrentHolderResults()

                            report += "\n\nsubtracted holder:\n"
                            report += self.MakeReportLine('', '', holderresults, 0, True)

                        dlg.SetText(report, texthighlight)

                        if dlg.ShowModal() == wx.ID_OK:
                            if self.MeasurementMode == 'specimen':
                                # store: specimen name, step, data and the results of the last holder measurement

                                wx.GetApp().GetTopWindow().StoreData(self.SpecimenComboBox.GetValue(),
                                                                     {'step': self.StepTextCtrl.GetValue(),
                                                                      'type': self.TypeTextCtrl.GetValue(),
                                                                      'comment': self.CommentTextCtrl.GetValue(),
                                                                      'measurements': self.measurements,
                                                                      'results': results,
                                                                      'holderresults': holderresults})
                            elif self.MeasurementMode == 'holder':
                                # store holder data
                                wx.GetApp().GetTopWindow().StoreData(self.SpecimenComboBox.GetValue(),
                                                                     {'step': self.StepTextCtrl.GetValue(), 'type': '',
                                                                      'comment': '', 'measurements': self.measurements,
                                                                      'results': results, 'time': GetTime()})

                                # now we have valid holder data
                                wx.GetApp().GetTopWindow().validholder = True

                                # increase number of holder measurement -> used as step
                                self.holdermeasno += 1

                            # start measurement of next specimen
                            self.SetFocus()
                            self.StartSpecimenMeasurement()

                            # clear list of measurements
                            self.MeasurementListCtrl.DeleteAllItems()

                        dlg.Destroy()

                    else:
                        # go to the end - deselect all
                        self.SelectMeasurement(len(self.measurements))

        # allow to repeat a specific measurement by hotkey
        elif (kc >= ord('0')) & (kc <= ord('9') - 9 + self.max_measurement):
            if CryoMagSettings.measmode == 'C3':  # cryo mode (C3)
                self.SelectMeasurement(kc - ord('1') + 1)
            else:  # spinner mode #  respect offset of 1 --> first measurement = 1
                if kc - ord('1') >= 0:  # do not allow negative values, i.e. somebody pressed 0
                    self.SelectMeasurement(kc - ord('1'))

        elif kc == wx.WXK_TAB:
            # eat all tabs
            pass
        else:
            # forward all unhandled keys to the application, especially the function keys
            event.Skip()

    def MakeReportLine(self, step, type, results, properties, holder=False):
        """Make a report line out of the results.
        If holder is False directions will be converted 
        to geo and bedding corrected coordinate systems"""

        # make line with results
        line = u"%5s%5s %+3.02E %+3.02E %+3.02E %+3.02E    \xb1%s %8s%8s%8s" % (step, type, results['X'], results['Y'],
                                                                                results['Z'], results['M'],
                                                                                ("%1.02E" % results['sM']).ljust(8),
                                                                                ("%.1f" % results['a95']).rjust(6),
                                                                                ("%.1f" % results['D']).rjust(6),
                                                                                ("%.1f" % results['I']).rjust(6))

        # for a specimen we append the transformed coordinates
        if holder == False:
            xyz = (results['X'], results['Y'], results['Z'])
            xyzgeo = core2geo(xyz, properties['coreaz'], properties['coredip'])
            (Dg, Ig, M) = XYZ2DIL(xyzgeo)

            xyzbed = geo2bed(xyzgeo, properties['bedaz'], properties['beddip'])
            (Ds, Is, M) = XYZ2DIL(xyzbed)

            line += "%8s%8s%8s%8s" % (
                ("%.1f" % Dg).rjust(6), ("%.1f" % Ig).rjust(6), ("%.1f" % Ds).rjust(6), ("%.1f" % Is).rjust(6))

        # append a line break
        line += "\n"

        return line

    def OnSpecimenComboBoxChar(self, event):
        """Handle Enter in SpecimenComboBox"""
        # enter? else exit
        if event.GetKeyCode() != wx.WXK_RETURN:
            event.Skip()

        else:
            # check if specimen exists, if not throw out a warning
            if self.SpecimenComboBox.GetValue().strip() == '':
                wx.MessageBox('Please enter specimen name')
                return

            # if not self.SpecimenTextCtrl.GetValue() in wx.GetApp().GetTopWindow().specimens.GetSpecimenList():
            if not wx.GetApp().GetTopWindow().specimens.SpecimenExists(self.SpecimenComboBox.GetValue()):
                # specimen not found in specimen list
                dlg = wx.MessageDialog(self, 'The specimen name is unknown. Add to specimen list?',
                                       'New specimen',
                                       wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION
                                       )
                if dlg.ShowModal() == wx.ID_YES:

                    # add specimen to specimen data list and fill with default properties
                    wx.GetApp().GetTopWindow().specimens.AddSpecimen(self.SpecimenComboBox.GetValue())

                    # update list of specimens in specimens panel
                    wx.GetApp().GetTopWindow().FunctionPanels['specimens'].UpdateSpecimens()

                    # go on to step field
                    self.StepTextCtrl.Enable()
                    self.StepTextCtrl.SetFocus()
                    self.StepTextCtrl.SelectAll()
                    self.ShowMessage("Enter step and press ENTER")
                else:
                    # workaround for SelectAll which does not select the last character under Linux
                    self.SpecimenComboBox.SetTextSelection(0, len(self.SpecimenComboBox.GetValue()))

                dlg.Destroy()
            # specimen found -> go on to step field
            else:
                self.StepTextCtrl.Enable()
                self.StepTextCtrl.SetFocus()
                self.StepTextCtrl.SelectAll()
                self.ShowMessage("Enter step and press ENTER")

            # update display of specimen properties 
            self.UpdatePropDisp()

    def OnStepTextCtrlTextEnter(self, event):
        """Got Enter in StepTextCtrl"""

        self.TypeTextCtrl.Enable()
        self.TypeTextCtrl.SetFocus()
        self.TypeTextCtrl.SelectAll()
        self.ShowMessage("Enter type and press ENTER")
        event.Skip()

    def OnTypeTextCtrlTextEnter(self, event):
        """Got Enter in TypeTextCtrl"""
        self.CommentTextCtrl.Enable()
        self.CommentTextCtrl.SetFocus()
        self.CommentTextCtrl.SelectAll()
        self.ShowMessage("Enter comment and press ENTER")
        event.Skip()

    def OnCommentTextCtrlTextEnter(self, event):
        """Got Enter in CommentTextCtrl"""
        self.MakeMeasurementList()

        self.StartMeasurement()

        self.MeasurementListCtrl.SetFocus()
        event.Skip()

    def UpdatePropDisp(self):
        """Update the properties of the specimen shown in the upper right corner
        of the measurement panel"""
        try:
            prop = wx.GetApp().GetTopWindow().specimens.GetProp(self.SpecimenComboBox.GetValue())
        except KeyError:
            return

        self.coreaztextCtrl.SetValue("%.0f" % prop['coreaz'])
        self.corediptextCtrl.SetValue("%.0f" % prop['coredip'])
        self.bedaztextCtrl.SetValue("%.0f" % prop['bedaz'])
        self.beddiptextCtrl.SetValue("%.0f" % prop['beddip'])
        self.voltextCtrl.SetValue("%.1G" % prop['vol'])
        self.weighttextCtrl.SetValue("%.1G" % prop['weight'])

    def ProcessMeasurements2C(self):
        """calculate results out of self.measurements[]['values'] and store into ['results'] for 2 component measurements"""
        # initialize results
        results = {}

        results['X'] = 0.0
        results['Y'] = 0.0
        results['Z'] = 0.0

        # count number of added components
        count = {}

        count['X'] = 0.0
        count['Y'] = 0.0
        count['Z'] = 0.0

        # go through all measurements
        for i in range(len(self.measurements)):
            vi = self.measurements[i]['values']  # data to process
            # sum up individual components if not NAN
            for id in ['X', 'Y', 'Z']:
                if not math.isnan(vi[id]):
                    results[id] = results[id] + vi[id]
                    count[id] = count[id] + 1

        # take average for all components
        for id in ['X', 'Y', 'Z']:
            results[id] = results[id] / count[id]

        # calc correpond inc, dec, total moment
        (results['D'], results['I'], results['M']) = XYZ2DIL((results['X'], results['Y'], results['Z']))

        # TODO: add error estimate
        results['a95'] = float('nan')
        results['sM'] = float('nan')

        results['time'] = GetTime()

        return results

    def ProcessMeasurements3C(self):
        """calculate results out of self.measurements[]['values'] and store into ['results'] for 3 component measurements"""

        # proessed measurement values
        proc_xyz = []

        # display baselines and difference between first and second baseline if appropriate
        if wx.GetApp().GetTopWindow().FunctionPanels['settings'].beforeandafterradioButton.GetValue() == True:
            # take first and last measurement as baselines
            vbl1 = self.measurements[0]['values']  # data of baseline 1
            vbl2 = self.measurements[-1]['values']  # data of baseline 1

            (dx, dy, dz) = np.array((vbl1['X'], vbl1['Y'], vbl1['Z'])) - np.array((vbl2['X'], vbl2['Y'], vbl2['Z']))

        # go through all measurements (baselines & several positions)
        for i in range(len(self.measurements)):
            # if we have an orientation matrix we are not measuring a baseline
            if 'rotmat' in self.measurements[i]:
                # find baseline before this measurement
                # for bl1 in range( i)[::-1]: # ::-1 inverses the list order
                #    if not 'rotmat' in self.measurements[ bl1].:
                #        break

                """#find baseline after this measurement
                for bl2 in [ii + i for ii in range( len( self.measurements) - i)]:
                    if not 'rotmat' in self.measurements[ bl2]:
                        break
                    
                #subtract mean of both baselines
                
                vi = self.measurements[ i][ 'values'] # data to process
                vbl1 = self.measurements[ bl1][ 'values'] # data of baseline 1
                vbl2 = self.measurements[ bl2][ 'values'] # data of baseline 2
                
                xyz = np.array((vi['X'], vi['Y'], vi['Z'])) - (np.array( (vbl1['X'], vbl1['Y'], vbl1['Z']))+np.array( (vbl2['X'], vbl2['Y'], vbl2['Z'])))/2.0
                """

                vi = self.measurements[i]['values']  # data to process
                # vbl1 = self.measurements[ bl1][ 'values'] # data of baseline 1

                # xyz = np.array((vi['X'], vi['Y'], vi['Z'])) - np.array((vbl1['X'], vbl1['Y'], vbl1['Z']))

                xyz = (vi['X'], vi['Y'], vi['Z'])

                DIL = XYZ2DIL(xyz)

                # append rotated data according to measurement position
                proc_xyz.append(xyz)

        X = []
        Y = []
        Z = []
        M = []
        DI = []

        for xyz in proc_xyz:
            (d, i, m) = XYZ2DIL(xyz)
            DI.append((d, i))
            M.append(m)
            X.append(xyz[0])
            Y.append(xyz[1])
            Z.append(xyz[2])

        X, sX = mean_stddev(X)
        Y, sY = mean_stddev(Y)
        Z, sZ = mean_stddev(Z)
        M, sM = mean_stddev(M)

        a95 = AXX(DI)

        # calc correpond inc, dec, total moment
        DIL = XYZ2DIL((X, Y, Z))

        results = {}
        results['X'] = X
        results['Y'] = Y
        results['Z'] = Z
        results['M'] = DIL[2]
        results['D'] = DIL[0]
        results['I'] = DIL[1]
        results['a95'] = a95
        results['sM'] = sM
        results['time'] = GetTime()

        return results

    def OnStepTextCtrlText(self, event):
        """Reacts on a changed value in StepTextCtrl"""
        if self.StepTextCtrl.GetValue() != "":
            try:
                int(self.StepTextCtrl.GetValue())
            except ValueError:
                wx.MessageBox('Step must be numeric.')
                self.StepTextCtrl.SetValue(str(self.laststep))
                self.StepTextCtrl.SelectAll()

        event.Skip()

    def OnTopPanelChar(self, event):
        # eat all tabs
        if event.GetKeyCode() != wx.WXK_TAB:
            event.Skip()

    def logger(self, evt, msg1, msg2=''):
        # log cryo raw data to file

        self.logfile.write("\r\n" + GetTime() + "#" + str(evt) + "#" + str(msg1) + "#" + str(msg2))
