"""This file holds basic CryoMag program settings"""
# written by Michael Wack 2008, 2009, 2010, 2011, 2021

# simulate measurements?
# change this to false if you want to do real measurements
simulate = True 

# serial port to use (number or device)
serialport = 0


# measure in 2 or 3 component mode
# 2G SQUID magnetometer deliver all 3 components at once
# Spinner magnetometers provide only 2 components at once
measmode = 'C3' # 3 component aka cryogenic magnetometer
#measmode = 'C2' # 2 component aka spinner magnetometer

# settings for cryogenic magnetometer

# configure selectable measurement positions for specimens
# [desc, array of sample directions, array of holder directions]

C3positionmodes = []
C3positionmodes.append( ['1 position mode', [[0, 0, 0]], [[0, 0, 0]]])
C3positionmodes.append( ['2 position mode', [[0, 0, 0], [0, 180, 90]], [[0, 0, 0], [0, 0, 0]]])
C3positionmodes.append( ['4 position mode', [[0, 0, 0], [90, 0, 0], [0, 180, 180], [0, 180, 90]], [[0, 0, 0], [270, 0, 0], [180, 0, 0], [90, 0, 0]]])
C3positionmodes.append( ['2 pos. m. (no flip)', [[0, 0, 0], [90, 0, 0]], [[0, 0, 0], [270, 0, 0]]])
C3positionmodes.append( ['4 pos. m. (no flip)', [[0, 0, 0], [90, 0, 0], [180, 0, 0], [270, 0, 0]], [[0, 0, 0], [270, 0, 0], [180, 0, 0], [90, 0, 0]]])

# C3 magnetometer calibration factors (3 components)
# factors to convert from flux quanta to Am^2
# CryoNL1
# C3calib = { 'x': 3.52E-8, 'y': 3.49E-8, 'z': 2.51E-8}
# CryoNL2
C3calib = { 'x': -5.794E-8, 'y': -5.756E-8, 'z': -3.931E-8}


# settings for spinner magnetometer
# configure selectable measurement positions for specimen
# [ desc, array of sample components as +-XYZ]
C2positionmodes = []
C2positionmodes.append( ['4 position mode', [['+X', '+Z'], ['+Y', '+Z'], ['+X', '+Z'], ['+Y', '+Z']]])

# C2 magnetometer calibration factor (1 component)
# factor to convert to Am^2
C2calib = 8.7

# configure instrument name
instrname = 'Cryo'

# configure program version
cryomagversion = '3.0a'
