# CryoMag auxiliary functions
"""Auxiliary functions for CryoMag. Coordinate transformations and statistics."""
# written by Michael Wack 2008, 2021

import time
from math import degrees, radians, cos, sin, tan, atan2, asin, acos, sqrt

import numpy as np


def XYZ2DIL(XYZ):
    """Converts a tuple of x,y,z components to a tuple of declination, inclination and length."""
    DIL = []
    L = np.linalg.norm(XYZ)
    # L=sqrt(XYZ[0]**2+XYZ[1]**2+XYZ[2]**2) # calculate resulting vector length
    try:
        D = degrees(atan2(XYZ[1], XYZ[0]))  # calculate declination taking care of correct quadrants (atan2)
    except ValueError:
        D = 0
    if D < 0:
        D = D + 360.  # put declination between 0 and 360.
    if D > 360.:
        D = D - 360.
    DIL.append(D)  # append declination to Dir list 
    try:
        I = degrees(asin(XYZ[2] / L))  # calculate inclination (converting to degrees)
    except ValueError:
        I = 0
    DIL.append(I)  # append inclination to Dir list
    DIL.append(L)  # append vector length to Dir list
    return DIL


def DIL2XYZ(DIL):
    """Converts a tuple of D,I,L components to a tuple of x,y,z."""
    (D, I, L) = DIL
    H = L * cos(radians(I))
    X = H * cos(radians(D))
    Y = H * sin(radians(D))
    Z = H * tan(radians(I))
    return (X, Y, Z)


def CreateRotMat(angles):
    """Generate roation matrix out of 3 Euler angles (a, b, c)."""

    A = ZRot(angles[0])
    B = XRot(angles[1])
    C = ZRot(angles[2])

    return np.dot(A, np.dot(B, C))


def XRot(xrot):
    """Generate rotation matrix for a rotation of xrot degrees around the x-axis"""
    a = radians(xrot)

    return np.array(((1, 0, 0), (0, cos(a), -sin(a)), (0, sin(a), cos(a))))


def YRot(yrot):
    """Generate rotation matrix for a rotation of yrot degrees around the yx-axis"""
    a = radians(yrot)

    return np.array(((cos(a), 0, sin(a)), (0, 1, 0), (-sin(a), 0, cos(a))))


def ZRot(zrot):
    """Generate rotation matrix for a rotation of zrot degrees around the z-axis"""
    a = radians(zrot)

    return np.array(((cos(a), -sin(a), 0), (sin(a), cos(a), 0), (0, 0, 1)))


def RotateVector(v, rotmat):
    """Rotate a vector v = (x,y,z) about 3 Euler angles (x-convention) defined by matrix rotmat."""
    # return resulting vector
    return np.dot(v, rotmat)


def AXX(listofdirections, p=.05):
    """Calculate the angular dispersion in which the given direction lies with the given confidence p.<br>
    listofdirections is a list of tuples declinations, inclinations.<br>
    This routine follows Butler, chapter 6, equ 6.21"""

    N = len(listofdirections)
    if N <= 1:
        return 0

    l, m, n = 0, 0, 0
    for d in listofdirections:
        dec = radians(d[0])
        inc = radians(d[1])
        l += cos(inc) * cos(dec)
        m += cos(inc) * sin(dec)
        n += sin(inc)
    R = sqrt(l ** 2 + m ** 2 + n ** 2)
    # return R
    try: 
       return degrees(acos(1.0 - (float(N) - R) / R * ((1.0 / p) ** (1.0 / (float(N) - 1.0)) - 1.0)))
    except ValueError:
       return -1

def AXX_xyz(listofxyz, use_diff=False, p=.05):
    """returns angle of uncertainty for given probability level p"""
    N = len(listofxyz)
    l, m, n = 0, 0, 0
    if use_diff == False:
        for d in listofxyz:
            l += listofxyz[0]
            m += listofxyz[1]
            n += listofxyz[2]
    else:
        last_xyz = listofxyz[-1]
        for d in listofxyz:
            l += listofxyz[0] - last_xyz[0]
            m += listofxyz[1] - last_xyz[1]
            n += listofxyz[2] - last_xyz[2]

    R = sqrt(l ** 2 + m ** 2 + n ** 2)
    # return R
    return degrees(acos(1.0 - (float(N) - R) / R * ((1.0 / p) ** (1.0 / (float(N) - 1.0)) - 1.0)))


# untested
def mean_stddev(listofvalues):
    """returns the mean and relative standard deviation of a list of values"""
    sum = 0
    N = len(listofvalues)

    if N == 1:
        return listofvalues[0], 0

    for v in listofvalues:
        sum += v
    mean = sum / float(N)

    sum = 0

    for v in listofvalues:
        sum += (v - mean) ** 2

    return mean, sqrt(1 / (float(N) - 1) * sum)


# coordinate transformations

def core2geo(core_xyz, coreaz, coredip):
    """convert core coordinates to geographic coordinates"""
    # rotate around y-axis to compensate core dip
    xyz = RotateVector(core_xyz[0:3], YRot(-coredip))
    # rotate around z-axis
    geo_xyz = RotateVector(xyz, ZRot(-coreaz))

    # do we have additional elements? Append the 4th (e.g. label)
    if len(core_xyz) > 3:
        geo_xyz = (geo_xyz[0], geo_xyz[1], geo_xyz[2], core_xyz[3])

    return geo_xyz


def geo2bed(geo_xyz, bedaz, beddip):
    """convert geographic coordinates to bedding corrected coordinates"""
    # TEST: XYZ2DIL( core2bed( (.3,.825,1.05), 40, 20)) -> D=62, I=32 (Butler S.72)
    # rotate around z-axis until strike is along y axis of fixed system
    xyz = RotateVector(geo_xyz[0:3], ZRot(bedaz))
    # now rotate around y-axis to compensate bed dip
    xyz = RotateVector(xyz, YRot(-beddip))
    # rotate back around z-axis
    bed_xyz = RotateVector(xyz, ZRot(-bedaz))

    # do we have additional elements? Append the 4th (e.g. label)
    if len(geo_xyz) > 3:
        bed_xyz = (bed_xyz[0], bed_xyz[1], bed_xyz[2], geo_xyz[3])

    return bed_xyz


def GetTime():
    return time.strftime("%y-%m-%d %H:%M:%S")


def Deg360(deg):
    """add or subtract 360 degrees until deg is in range 0 to 360"""
    while deg > 360.0:
        deg -= 360

    while deg < 0.0:
        deg += 360

    return deg


def efmt2(estr):
    """reduce number of digits in exponent to 2 if needed"""
    if (estr[-5] == 'E' or estr[-5] == 'e') and estr[-3] == '0':
        return estr[:-3] + estr[-2:]
    else:
        return estr
