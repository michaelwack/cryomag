# Boa:FramePanel:CryoMagDataPanel
"""Implementation for the Data Panel to display specimens in a tree structure"""

from sys import platform

import wx
# import wx.gizmos
import wx.dataview

from CryoMagAux import *
from CryoMagStepEditDialog import CryoMagStepEditDialog


class CryoMagDataPanel(wx.Panel):
    def _init_coll_boxSizer1_Items(self, parent):
        

        parent.Add(self.MeasurementDataTreeListCtrl, 1, border=0,
                   flag=wx.GROW)
        parent.Add(self.EditstaticText, 0, border=0, flag=wx.GROW)

    def _init_sizers(self):
        
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        wx.Panel.__init__(self, id=wx.ID_ANY,
                          name=u'CryoMagDataPanel', parent=prnt, pos=wx.Point(512, 268),
                          size=wx.Size(454, 477), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(446, 450))
        self.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.LIGHT, False, u'Sans'))
        self.MeasurementDataTreeListCtrl = wx.dataview.TreeListCtrl(id=wx.ID_ANY,
                                                                    name=u'MeasurementDataTreeListCtrl', parent=self,
                                                                    pos=wx.Point(0,
                                                                                 0), size=wx.Size(446, 437),
                                                                    style=wx.TR_HAS_BUTTONS | wx.TR_FULL_ROW_HIGHLIGHT | wx.TR_HIDE_ROOT)
        self.MeasurementDataTreeListCtrl.SetToolTip(u'measurement data')
        self.MeasurementDataTreeListCtrl.Bind(wx.dataview.EVT_TREELIST_ITEM_EXPANDING,
                                              self.OnMeasurementDataTreeListCtrlTreeItemExpanding,
                                              id=wx.ID_ANY)
        self.MeasurementDataTreeListCtrl.Bind(wx.dataview.EVT_TREELIST_SELECTION_CHANGED,
                                              self.OnMeasurementDataTreeListCtrlTreeSelChanged,
                                              id=wx.ID_ANY)
        self.MeasurementDataTreeListCtrl.Bind(wx.EVT_KEY_DOWN,  # TODO: check this event #NOT WORKING in winX
                                              self.OnMeasurementDataTreeListCtrlTreeKeyDown,
                                              id=wx.ID_ANY)
        self.MeasurementDataTreeListCtrl.Bind(wx.dataview.EVT_TREELIST_ITEM_ACTIVATED,
                                              self.OnMeasurementDataTreeListCtrlTreeItemActivated,
                                              id=wx.ID_ANY)

        self.EditstaticText = wx.StaticText(id=wx.ID_ANY,
                                            label=u'Edit data with double click.', name=u'EditstaticText',
                                            parent=self, pos=wx.Point(0, 437), size=wx.Size(446, 13),
                                            style=0)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)

        self.MeasurementDataTreeListCtrl.AppendColumn(u'measurement')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'X [Am\xb2]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Y [Am\xb2]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Z [Am\xb2]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'M [Am\xb2]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'sM')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'a95 [\xb0]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Dc [\xb0]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Ic [\xb0]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Dg [\xb0]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Ig [\xb0]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Ds [\xb0]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'Is [\xb0]')
        self.MeasurementDataTreeListCtrl.AppendColumn(u'time')

        self.MeasurementDataTreeListCtrl.SetColumnWidth(0, 140)
        for i in range(5):
            self.MeasurementDataTreeListCtrl.SetColumnWidth(i + 1, 75)
        for i in range(7):
            self.MeasurementDataTreeListCtrl.SetColumnWidth(i + 6, 50)
        self.MeasurementDataTreeListCtrl.SetColumnWidth(13, 100)

        #self.MeasurementDataTreeListCtrl.SetAutoLayout(True)
        #self.MeasurementDataTreeListCtrl.Layout()
        # self.MeasurementDataTreeListCtrl.AddRoot( 'Measurement data')

        # specifies the name of the specimen that was selected in the MeasurementDataTreeListCtrl
        self.last_specimen_selected = 0

        self.needupdate = False

    # overwrite Show from Parent
    def Show(self, show=True):
        if show == True and self.needupdate == True:
            # update data if needed
            self.UpdateSpecimens()
            # wx.MessageBox( 'specimens updated')
        return wx.Panel.Show(self, show)

    def UpdateSpecimens(self):
        # load measurement data into TreeListCtrl
        self.MeasurementDataTreeListCtrl.DeleteAllItems()

        # fill TreeListCtrl
        # root = self.MeasurementDataTreeListCtrl.AddRoot( 'Measurement data')

        # create progress dialog
        dlg = wx.ProgressDialog("Preparing data....",
                                "",
                                maximum=len(wx.GetApp().GetTopWindow().specimens.GetSpecimenList()),
                                parent=wx.GetApp().GetTopWindow(),
                                style=wx.PD_APP_MODAL
                                      | wx.PD_ELAPSED_TIME
                                      | wx.PD_ESTIMATED_TIME
                                      | wx.PD_REMAINING_TIME
                                      | wx.PD_AUTO_HIDE
                                )
        # set the focus to the dialog to prevent key events in other windows
        # dlg.SetFocus()

        pcnt = 0

        for specimen in wx.GetApp().GetTopWindow().specimens.GetSpecimenList():
            # update progress dialog
            dlg.Update(pcnt, str(specimen))
            pcnt += 1

            specimenitem = self.MeasurementDataTreeListCtrl.AppendItem(self.MeasurementDataTreeListCtrl.GetRootItem(),
                                                                       str(specimen))
            self.MeasurementDataTreeListCtrl.SetItemData(specimenitem, {'specimen': str(specimen)})

            stepno = 0

            properties = wx.GetApp().GetTopWindow().specimens.GetProp(specimen)

            for step in wx.GetApp().GetTopWindow().specimens.GetStepData(specimen):
                # add steps
                stepitem = self.MeasurementDataTreeListCtrl.AppendItem(specimenitem, "%s (%s) [%s]" % (
                    str(step['step']), step['type'], step['comment']))
                self.MeasurementDataTreeListCtrl.SetItemData(stepitem, {'specimen': str(specimen), 'step': step['step'],
                                                                        'stepno': stepno})

                # put results in table
                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 1, "% .2E" % step['results']['X'])
                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 2, "% .2E" % step['results']['Y'])
                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 3, "% .2E" % step['results']['Z'])
                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 4, "% .2E" % step['results']['M'])
                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 5, "%.2E" % step['results']['sM'])
                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 6, "%.1f" % step['results']['a95'])

                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 7, "%.1f" % step['results']['D'])
                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 8, "%.1f" % step['results']['I'])

                if specimen != 'holder':
                    # do coordinate transformations
                    xyz = (step['results']['X'], step['results']['Y'], step['results']['Z'])
                    xyzgeo = core2geo(xyz, properties['coreaz'], properties['coredip'])
                    (Dg, Ig, M) = XYZ2DIL(xyzgeo)

                    xyzbed = geo2bed(xyzgeo, properties['bedaz'], properties['beddip'])
                    (Ds, Is, M) = XYZ2DIL(xyzbed)

                    self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 9, "%.1f" % Dg)
                    self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 10, "%.1f" % Ig)

                    self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 11, "%.1f" % Ds)
                    self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 12, "%.1f" % Is)

                self.MeasurementDataTreeListCtrl.SetItemText(stepitem, 13, "%s" % step['results']['time'])

                # specimen with holder results?
                if 'holderresults' in step:
                    holderitem = self.MeasurementDataTreeListCtrl.AppendItem(stepitem, 'holder')
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 1, "% .2E" % step['holderresults']['X'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 2, "% .2E" % step['holderresults']['Y'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 3, "% .2E" % step['holderresults']['Z'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 4, "% .2E" % step['holderresults']['M'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 5, "%.2E" % step['holderresults']['sM'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 6, "%.1f" % step['holderresults']['a95'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 7, "%.1f" % step['holderresults']['D'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 8, "%.1f" % step['holderresults']['I'])
                    self.MeasurementDataTreeListCtrl.SetItemText(holderitem, 13, "%s" % step['holderresults']['time'])
                    # changed 17-07-2008
                    # self.MeasurementDataTreeListCtrl.SetItemData( holderitem, {'specimen': str( specimen)})
                    self.MeasurementDataTreeListCtrl.SetItemData(holderitem,
                                                                 {'specimen': str(specimen), 'step': step['step'],
                                                                  'stepno': stepno})

                for p in step['measurements']:
                    # add positions
                    positem = self.MeasurementDataTreeListCtrl.AppendItem(stepitem, str(p['name']))
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 1, "% .2E" % p['values']['X'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 2, "% .2E" % p['values']['Y'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 3, "% .2E" % p['values']['Z'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 4, "% .2E" % p['values']['M'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 5, "%.2E" % p['values']['sM'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 6, "%.1f" % p['values']['a95'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 7, "%.1f" % p['values']['D'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 8, "%.1f" % p['values']['I'])
                    self.MeasurementDataTreeListCtrl.SetItemText(positem, 13, "%s" % p['values']['time'])
                    self.MeasurementDataTreeListCtrl.SetItemData(positem,
                                                                 {'specimen': str(specimen), 'step': step['step'],
                                                                  'stepno': stepno, 'pos': p['name']})

                stepno += 1

        self.MeasurementDataTreeListCtrl.Expand(self.MeasurementDataTreeListCtrl.GetRootItem())

        # we are up to data
        self.needupdate = False

        # destroy progress dialog
        dlg.Destroy()

    def OnMeasurementDataTreeListCtrlTreeItemExpanding(self, event):
        # patch under Linux
        if platform.find('linux') > -1:
            self.MeasurementDataTreeListCtrl.Refresh()
        event.Skip()

    def OnMeasurementDataTreeListCtrlTreeSelChanged(self, event):
        """Another Tree Item was selected.
        Find out of which specimen and show its data"""
        #print('OnMeasurementDataTreeListCtrlTreeSelChanged')
        item = event.GetItem()
        # wx.MessageBox( str( self.MeasurementDataTreeListCtrl.GetItemData( item).GetData()))
        #print(self.MeasurementDataTreeListCtrl.GetItemData(item))

        # show specimen data if changed
        itdat = self.MeasurementDataTreeListCtrl.GetItemData(item)
        #specimen = None
        if itdat is not None:
            specimen = itdat['specimen']

            if self.last_specimen_selected != specimen:
                wx.GetApp().GetTopWindow().ShowSpecimenData(specimen, False)
                self.last_specimen_selected = specimen

            # select corresponding data point in plots
            try:
                dnr = itdat['stepno']

                # 17-6-2008 added correction for reduced no of points in plots
                maxnoofpoints = int(wx.GetApp().GetTopWindow().NoPointsShowSpinCtrl.GetValue())
                noofpoints = len(wx.GetApp().GetTopWindow().specimens.GetStepData(specimen))
                if noofpoints > maxnoofpoints:
                    dnr -= noofpoints - maxnoofpoints
                    if dnr < 0:
                        wx.MessageBox('Data point not in plot. Increase number of displayed points.')
                        return

                wx.GetApp().GetTopWindow().DecayPlotPanel.SelectDataPoint(0, dnr)
                wx.GetApp().GetTopWindow().ZijderveldPlotPanel.SelectDataPoint(0, dnr)
                wx.GetApp().GetTopWindow().StereoPlotPanel.SelectDataPoint(0, dnr)
            except KeyError as e:
                # key stepno does not exist if specimen is selected
                #print("Exception OnMeasurementDataTreeListCtrlTreeSelChanged", e)
                pass

        event.Skip()

    def OnMeasurementDataTreeListCtrlTreeKeyDown(self, event):
        print('keydown')
        if event.GetKeyCode() == wx.WXK_DELETE:
            item = self.MeasurementDataTreeListCtrl.GetSelection()
            wx.GetApp().GetTopWindow().DeleteStep(
                self.MeasurementDataTreeListCtrl.GetItemData(item)['specimen'],
                self.MeasurementDataTreeListCtrl.GetItemData(item)['stepno'])
        else:
            event.Skip()

    def OnMeasurementDataTreeListCtrlTreeItemActivated(self, event):
        """Allows rename of specimens or change of step data on double click"""
        item = event.GetItem()
        specimen = self.MeasurementDataTreeListCtrl.GetItemData(item)['specimen']
        try:
            dnr = self.MeasurementDataTreeListCtrl.GetItemData(item)['stepno']
        except KeyError:
            # no step value found --> rename specimen
            dlg = wx.TextEntryDialog(
                self, 'Enter new name for specimen %s.' % specimen,
                'Cryomag', specimen)

            if dlg.ShowModal() == wx.ID_OK:
                newspecimen = dlg.GetValue()
                msgdlg = wx.MessageDialog(self, "Do you really want to change the name of specimen %s to %s?" % (
                    specimen, newspecimen),
                                          'Rename Specimen',
                                          wx.YES_NO | wx.ICON_INFORMATION
                                          )
                if msgdlg.ShowModal() == wx.ID_YES:
                    # now we can really rename it
                    res = wx.GetApp().GetTopWindow().specimens.RenameSpecimen(specimen, newspecimen)
                    if res == 0:
                        # write the renamed specimen to a data file
                        wx.GetApp().GetTopWindow().specimens.WriteSpecimenDataToFile(newspecimen)
                        # delete the old orphaned data file
                        wx.GetApp().GetTopWindow().specimens.DelSpecimenDataFile(specimen)

                        # update specimen tree with changed values
                        self.UpdateSpecimens()
                        # update specimen panel
                        wx.GetApp().GetTopWindow().FunctionPanels['specimens'].UpdateSpecimens()

                    elif res == 2:
                        wx.MessageBox('Error: %s already exists!' % newspecimen)

                        # finally destroy the message dialog
                msgdlg.Destroy()

            # finally destroy the text entry dialog
            dlg.Destroy()

        else:
            # step no found --> change step data
            sd = wx.GetApp().GetTopWindow().specimens.GetStepData(specimen)[dnr]

            dlg = CryoMagStepEditDialog(self, sd['step'], sd['type'], sd['comment'])

            if dlg.ShowModal() == wx.ID_OK:
                # change step data
                # wx.MessageBox( "Do you really want to change step data?")
                res = dlg.GetResults()

                # store updated values in stepdata
                for k in res.keys():
                    sd[k] = res[k]

                # rewrite data file with updated step data
                wx.GetApp().GetTopWindow().specimens.WriteSpecimenDataToFile(specimen)

                # update specimen tree with changed values
                self.UpdateSpecimens()

            dlg.Destroy()

        # do not skip event to prevent expansion of item?
        event.Skip()
