# part of CryoMag
# written by Michael Wack 2008-2021
"""Timed Measurement Panel. Handles processing of timed measurement."""

import math
import sys

import wx
import wx.adv
import wx.lib.intctrl
import wx.lib.masked.numctrl
import wx.lib.stattext

from CryoMagAux import *

# This is a fix for the IndexError when the original masked.NumCtrl is edited
class CustomNumCtrl(wx.lib.masked.NumCtrl):
    def _OnKeyDown(self, event):
        self._OnChar(event)

class CryoMagTimedMeasurementPanel(wx.Panel):
    def _init_coll_boxSizer1_Items(self, parent):
        parent.Add(self.TopPanel, 0, border=0, flag=wx.GROW)
        parent.Add(self.TimedMeasurementListCtrl, 1, border=0,
                   flag=wx.GROW)

    def _init_coll_TimedMeasurementListCtrl_Columns(self, parent):
        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT, heading=u'i',
                            width=25)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
                            heading=u'X [Am\xb2]', width=95)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Y [Am\xb2]', width=95)
        parent.InsertColumn(col=3, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Z [Am\xb2]', width=95)
        parent.InsertColumn(col=4, format=wx.LIST_FORMAT_LEFT,
                            heading=u'M [Am\xb2]', width=95)
        parent.InsertColumn(col=5, format=wx.LIST_FORMAT_LEFT, heading=u'sM',
                            width=70)
        parent.InsertColumn(col=6, format=wx.LIST_FORMAT_LEFT,
                            heading=u'a95 [\xb0]', width=50)
        parent.InsertColumn(col=7, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Dc [\xb0]', width=50)
        parent.InsertColumn(col=8, format=wx.LIST_FORMAT_LEFT,
                            heading=u'Ic [\xb0]', width=50)

    def _init_utils(self):
        self.meas_timer = wx.Timer(id=wx.ID_ANY,
                                   owner=self)
        self.Bind(wx.EVT_TIMER, self.OnMeas_timerTimer,
                  id=wx.ID_ANY)

    def _init_sizers(self):
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        wx.Panel.__init__(self, id=wx.ID_ANY,
                          name=u'CryoMagTimedMeasurementPanel', parent=prnt,
                          pos=wx.Point(297, 334), size=wx.Size(637, 413),
                          style=wx.TAB_TRAVERSAL)
        self._init_utils()
        self.SetClientSize(wx.Size(637, 413))
        self.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.LIGHT, False, u'Sans'))

        self.TopPanel = wx.Panel(id=wx.ID_ANY,
                                 name=u'TopPanel', parent=self, pos=wx.Point(0, 0),
                                 size=wx.Size(637, 104), style=0)
        self.TopPanel.Bind(wx.EVT_CHAR, self.OnTopPanelChar)

        self.staticText1 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'specimen', name='staticText1', parent=self.TopPanel,
                                         pos=wx.Point(7, 7), size=wx.Size(65, 18), style=0)

        self.SpecimenComboBox = wx.ComboBox(id=wx.ID_ANY,
                                            name=u'SpecimenComboBox', parent=self.TopPanel, pos=wx.Point(71, 7),
                                            size=wx.Size(129, 21),
                                            style=wx.TE_PROCESS_ENTER | wx.CB_DROPDOWN, value=u'AD234')
        self.SpecimenComboBox.Enable(False)
        self.SpecimenComboBox.Bind(wx.EVT_CHAR, self.OnSpecimenComboBoxChar)

        self.TimedMeasurementListCtrl = wx.ListCtrl(id=wx.ID_ANY,
                                                    name=u'TimedMeasurementListCtrl', parent=self, pos=wx.Point(0, 104),
                                                    size=wx.Size(637, 323), style=wx.LC_REPORT)
        self._init_coll_TimedMeasurementListCtrl_Columns(self.TimedMeasurementListCtrl)

        self.TypeTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                        name=u'TypeTextCtrl', parent=self.TopPanel, pos=wx.Point(71, 35),
                                        size=wx.Size(129, 21), style=wx.TE_PROCESS_ENTER, value=u'TIME')

        self.coreazstaticText = wx.StaticText(id=wx.ID_ANY,
                                              label=u'coreaz', name=u'coreazstaticText', parent=self.TopPanel,
                                              pos=wx.Point(400, 16), size=wx.Size(42, 16), style=0)

        self.staticText4 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'coredip', name='staticText4', parent=self.TopPanel,
                                         pos=wx.Point(495, 16), size=wx.Size(49, 16), style=0)

        self.staticText5 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'beddip', name='staticText5', parent=self.TopPanel,
                                         pos=wx.Point(495, 40), size=wx.Size(48, 16), style=0)

        self.staticText6 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'bedaz', name='staticText6', parent=self.TopPanel,
                                         pos=wx.Point(400, 40), size=wx.Size(48, 16), style=0)

        self.staticText7 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'vol', name='staticText7', parent=self.TopPanel,
                                         pos=wx.Point(400, 64), size=wx.Size(40, 16), style=0)

        self.staticText8 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'weight', name='staticText8', parent=self.TopPanel,
                                         pos=wx.Point(495, 64), size=wx.Size(48, 16), style=0)

        self.beddiptextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                          name=u'beddiptextCtrl', parent=self.TopPanel, pos=wx.Point(544,
                                                                                                     36),
                                          size=wx.Size(40, 21), style=0, value=u'0')
        self.beddiptextCtrl.Enable(False)

        self.voltextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                       name=u'voltextCtrl', parent=self.TopPanel, pos=wx.Point(448, 60),
                                       size=wx.Size(40, 21), style=0, value=u'0')
        self.voltextCtrl.Enable(False)

        self.corediptextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                           name=u'corediptextCtrl', parent=self.TopPanel, pos=wx.Point(544, 12),
                                           size=wx.Size(40, 21), style=0, value=u'0')
        self.corediptextCtrl.Enable(False)

        self.bedaztextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                         name=u'bedaztextCtrl', parent=self.TopPanel, pos=wx.Point(448, 36),
                                         size=wx.Size(40, 21), style=0, value=u'0')
        self.bedaztextCtrl.Enable(False)

        self.weighttextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                          name=u'weighttextCtrl', parent=self.TopPanel, pos=wx.Point(544, 60),
                                          size=wx.Size(40, 21), style=0, value=u'0')
        self.weighttextCtrl.Enable(False)

        self.coreaztextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                          name=u'coreaztextCtrl', parent=self.TopPanel, pos=wx.Point(448, 12),
                                          size=wx.Size(40, 21), style=0, value=u'0')
        self.coreaztextCtrl.Enable(False)

        self.staticText2 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'type', name='staticText2', parent=self.TopPanel,
                                         pos=wx.Point(28, 36), size=wx.Size(40, 18), style=0)

        self.staticBox1 = wx.StaticBox(id=wx.ID_ANY,
                                       label=u'interval', name='staticBox1', parent=self.TopPanel,
                                       pos=wx.Point(216, 2), size=wx.Size(144, 88), style=0)

        self.staticText3 = wx.StaticText(id=wx.ID_ANY,
                                         label=u't = n * i ^ x seconds', name='staticText3',
                                         parent=self.TopPanel, pos=wx.Point(227, 18), size=wx.Size(93, 13),
                                         style=0)

        self.n_numCtrl = CustomNumCtrl(id=wx.ID_ANY,
                                                       name=u'n_numCtrl', parent=self.TopPanel, pos=wx.Point(248, 36),
                                                       size=wx.Size(40, 22), style=0)
        #self.n_numCtrl = wx.lib.masked.numctrl.NumCtrl(id=wx.ID_ANY,
        #                                              name=u'n_numCtrl', parent=self.TopPanel, pos=wx.Point(248, 36),
        #                                              size=wx.Size(30, 22), style=0, value=10)
        self.n_numCtrl.SetIntegerWidth(4)
        self.n_numCtrl.SetMin(1)
        self.n_numCtrl.ChangeValue(10)
        self.n_numCtrl.SetFont(wx.Font(8, family=wx.DEFAULT, style=wx.NORMAL, weight=wx.FONTWEIGHT_NORMAL))
        self.n_numCtrl.SetInitialSize(wx.Size(50, 22))

        self.x_numCtrl = CustomNumCtrl(id=wx.ID_ANY,
                                                       name=u'x_numCtrl', parent=self.TopPanel, pos=wx.Point(315, 36),
                                                       size=wx.Size(40, 13), style=0)
        #self.x_numCtrl = wx.lib.masked.numctrl.NumCtrl(id=wx.ID_ANY,
        #                                               name=u'x_numCtrl', parent=self.TopPanel, pos=wx.Point(311, 36),
        #                                               size=wx.Size(40, 22), style=0, value=1)
        self.x_numCtrl.SetFractionWidth(1)
        self.x_numCtrl.SetIntegerWidth(1)
        self.x_numCtrl.SetMin(1)
        self.x_numCtrl.ChangeValue(1.0)
        self.x_numCtrl.SetFont(wx.Font(8, family=wx.DEFAULT, style=wx.NORMAL, weight=wx.FONTWEIGHT_NORMAL))
        self.x_numCtrl.SetInitialSize(wx.Size(40, 22))

        self.genStaticText1 = wx.lib.stattext.GenStaticText(ID=wx.ID_ANY,
                                                            label=u'n=', name='genStaticText1', parent=self.TopPanel,
                                                            pos=wx.Point(224, 40), size=wx.Size(12, 13), style=0)

        self.genStaticText2 = wx.lib.stattext.GenStaticText(ID=wx.ID_ANY,
                                                            label=u'x=', name='genStaticText2', parent=self.TopPanel,
                                                            pos=wx.Point(300, 40), size=wx.Size(11, 13), style=0)

        self.genStaticText3 = wx.lib.stattext.GenStaticText(ID=wx.ID_ANY,
                                                            label=u'i=', name='genStaticText3', parent=self.TopPanel,
                                                            pos=wx.Point(227, 66), size=wx.Size(8, 13), style=0)

        self.istart_intCtrl = wx.lib.intctrl.IntCtrl(allow_long=False,
                                                     allow_none=False, default_color=wx.BLACK,
                                                     id=wx.ID_ANY, limited=True,
                                                     max=None, min=0, name=u'istart_intCtrl', oob_color=wx.RED,
                                                     parent=self.TopPanel, pos=wx.Point(249, 62), size=wx.Size(24, 21),
                                                     style=0, value=1)

        self.iend_intCtrl = wx.lib.intctrl.IntCtrl(allow_long=False,
                                                   allow_none=False, default_color=wx.BLACK,
                                                   id=wx.ID_ANY, limited=True,
                                                   max=None, min=1, name=u'iend_intCtrl', oob_color=wx.RED,
                                                   parent=self.TopPanel, pos=wx.Point(297, 62), size=wx.Size(48, 21),
                                                   style=0, value=10)

        self.staticText9 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'...', name='staticText9', parent=self.TopPanel,
                                         pos=wx.Point(280, 69), size=wx.Size(9, 13), style=0)

        self.StartButton = wx.Button(id=wx.ID_ANY,
                                     label=u'Start', name=u'StartButton', parent=self.TopPanel,
                                     pos=wx.Point(71, 63), size=wx.Size(128, 23), style=0)
        self.StartButton.SetForegroundColour(wx.Colour(255, 0, 0))
        self.StartButton.Bind(wx.EVT_BUTTON, self.OnStartButtonButton,
                              id=wx.ID_ANY)

        self._init_sizers()

    def __init__(self, parent):
        """the constructor"""
        self._init_ctrls(parent)

        # init logfile

        # self.logfile = open('cryolog.txt', 'wa')

        # init default values for step and specimen text box
        self.lasttype = 'TIME'
        self.lastspecimen = 'XXX'

        # ask if there is no valid holder data
        self.askforholder = True

        # open sound file in script directory
        self.ping = wx.adv.Sound(sys.path[0] + '/cork_pop.wav')

    # initialize communication with 2G Cryogenic Magnetometer
    # use first serial port by default
    # self.cryo = CryoComm.CryoComm( logfunction = self.logger)
    # self.cryo = CryoComm.CryoComm()
    # 30.1.09 use cryo instance from measurement panel

    def InitSpecimenTimedMeasurement(self):
        """initialize the TimedMeasurementPanel and ask for missing holder data"""
        # check if we have data for holder
        if wx.GetApp().GetTopWindow().validholder == False and self.askforholder == True:
            dlg = wx.MessageDialog(self, 'Do you want to measure a holder first?', 'No valid holder data.',
                                   wx.YES_NO | wx.NO_DEFAULT | wx.ICON_EXCLAMATION)
            # if user clicked no -> do nothing
            if dlg.ShowModal() == wx.ID_YES:
                dlg.Destroy()
                wx.GetApp().GetTopWindow().OnHolderMeasurement()
                return
            else:
                dlg.Destroy()
                self.askforholder = False

        # fill list with all specimens measured up to now except the holder
        self.SpecimenComboBox.Clear()
        sl = []
        for s in wx.GetApp().GetTopWindow().specimens.GetSpecimenList():
            if s != 'holder':
                sl.append(s)
        self.SpecimenComboBox.AppendItems(sl)

        # set number of measurement to 0
        self.measurement = 0

        # set type and specimen, ignore axception if there is no last...
        try:
            self.SpecimenComboBox.SetValue(self.lastspecimen)
            self.TypeTextCtrl.SetValue(self.lasttype)
        finally:
            pass

        # activate and set focus to Specimen Text Box
        self.SpecimenComboBox.Enable()
        self.SpecimenComboBox.SetFocus()
        # workaround for SelectAll which does not select the last character under Linux
        self.SpecimenComboBox.SetTextSelection(0, len(self.SpecimenComboBox.GetValue()))

    def StartTimedMeasurement(self):
        """start a measurement"""

        # check input values
        if self.istart_intCtrl.GetValue() >= self.iend_intCtrl.GetValue():
            wx.MessageBox('start value for i must be smaller than the end value.')
            return

        # force n within limits
        self.n_numCtrl.SetLimited(True)
        # force x within limits
        self.x_numCtrl.SetLimited(True)

        # clear list of measurements
        self.TimedMeasurementListCtrl.DeleteAllItems()

        # check if specimen exists, if not throw out a warning
        if not wx.GetApp().GetTopWindow().specimens.SpecimenExists(self.SpecimenComboBox.GetValue()):
            # specimen not found in specimen list
            dlg = wx.MessageDialog(self, 'The specimen name is unknown. Add to specimen list?',
                                   'New specimen',
                                   wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION
                                   )
            if dlg.ShowModal() == wx.ID_YES:

                # add specimen to specimen data list and fill with default properties
                wx.GetApp().GetTopWindow().specimens.AddSpecimen(self.SpecimenComboBox.GetValue())

                # update list of specimens in specimens panel
                wx.GetApp().GetTopWindow().FunctionPanels['specimens'].UpdateSpecimens()

            else:
                self.SpecimenComboBox.SetFocus()
                # workaround for SelectAll which does not select the last character under Linux
                self.SpecimenComboBox.SetMark(0, len(self.SpecimenComboBox.GetValue()))

                return

            dlg.Destroy()

        # update display of specimen properties
        self.UpdatePropDisp()

        # measure baseline
        dlg = wx.MessageDialog(self, "Ready to measure baseline?", "Baseline Measurement",
                               wx.YES_NO | wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()

        if result == wx.ID_NO:
            return

        # send reset command to cryo to avoid flux jump
        wx.GetApp().GetTopWindow().FunctionPanels['measurement'].cryo.SendCommand('ACLP')

        # wait a little bit - 23.03.2010
        # increased to .5 8.3.2013 -- x-component of baseline measurement in NL was often screwed up (e-8 range)
        time.sleep(.5)

        self.baseline = wx.GetApp().GetTopWindow().FunctionPanels['measurement'].cryo.ReadMagMoment(show_progress=True,
                                                                                                    numval=
                                                                                                    wx.GetApp().GetTopWindow().FunctionPanels[
                                                                                                        'settings'].noreadspinCtrl.GetValue())

        self.AppendMeasurementListCtrlLine('bl', self.baseline)

        dlg = wx.MessageDialog(self, "Baseline measured. Please insert specimen.\n Start timed measurement?",
                               "Timed Measurement",
                               wx.YES_NO | wx.ICON_QUESTION)

        result = dlg.ShowModal()
        dlg.Destroy()

        if result == wx.ID_NO:
            return

        # set starting value of i
        self.i = self.istart_intCtrl.GetValue()
        self.starttime = time.time()  # seconds since the epoch

        # show progress dialog
        self.prgdlg = wx.ProgressDialog("Timed measurement running ...",
                                        "next measurement time", parent=self,
                                        maximum=int(self.n_numCtrl.GetValue() * math.pow(self.iend_intCtrl.GetValue(),
                                                                                     self.x_numCtrl.GetValue()) + 2),
                                        style=wx.PD_CAN_ABORT
                                              | wx.PD_APP_MODAL
                                              | wx.PD_ELAPSED_TIME
                                              | wx.PD_ESTIMATED_TIME
                                              | wx.PD_REMAINING_TIME
                                        )

        # start timer every second
        self.meas_timer.Start(1000)

        # show plots of previous data of specimen
        if wx.GetApp().GetTopWindow().specimens.SpecimenExists(self.SpecimenComboBox.GetValue()):
            wx.GetApp().GetTopWindow().ShowSpecimenData(self.SpecimenComboBox.GetValue(), False)

    def MeasureTimed(self, seconds):
        """called each time for a timed measurement, seconds specifies the
        time since start of measurements and is used as the step value"""

        # read magnetic moment from magnetometer

        magmom = wx.GetApp().GetTopWindow().FunctionPanels['measurement'].cryo.ReadMagMoment(show_progress=True, numval=
        wx.GetApp().GetTopWindow().FunctionPanels['settings'].noreadspinCtrl.GetValue())

        # put focus back to the measurement panel (not needed under win32)
        self.SetFocus()

        x, y, z = magmom['X'], magmom['Y'], magmom['Z']

        # subtract baseline
        x -= self.baseline['X']
        y -= self.baseline['Y']
        z -= self.baseline['Z']

        # get holder results
        holderresults = wx.GetApp().GetTopWindow().GetCurrentHolderResults()

        # subtract holder
        x -= holderresults['X']
        y -= holderresults['Y']
        z -= holderresults['Z']

        # use magmom - holder - baseline as result
        results = magmom

        results['X'] = x
        results['Y'] = y
        results['Z'] = z

        # calc correpond inc, dec, total moment
        dec, inc, M = XYZ2DIL((x, y, z))

        results['M'] = M
        results['D'] = dec
        results['I'] = inc

        # do not care about rotating measurement data, always assume coordinate system of instrument

        self.AppendMeasurementListCtrlLine(str(self.i - 1), results)

        # store data
        # measurements consist of baseline and the actual measurement

        measurements = [{}, {}]
        measurements[0]['name'] = 'baseline'
        measurements[0]['values'] = self.baseline

        dec, inc, M = XYZ2DIL((self.baseline['X'], self.baseline['Y'], self.baseline['Z']))

        measurements[0]['values']['D'] = dec
        measurements[0]['values']['I'] = inc
        measurements[0]['values']['M'] = M

        measurements[1]['name'] = 'Pos (0, 0, 0)'
        measurements[1]['values'] = results

        wx.GetApp().GetTopWindow().StoreData(self.SpecimenComboBox.GetValue(),
                                             {'step': "%d" % seconds, 'type': self.TypeTextCtrl.GetValue(),
                                              'comment': 'timed measurement', 'measurements': measurements,
                                              'results': results, 'holderresults': holderresults})


    def MeasureTimedStop(self):
        self.meas_timer.Stop()
        self.prgdlg.Destroy()

        # measure final baseline
        dlg = wx.MessageDialog(self, "Ready to measure final baseline? Attention this will not be written to any file!", "Baseline Measurement",
                               wx.YES_NO | wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()

        if result == wx.ID_NO:
            return

        # send reset command to cryo to avoid flux jump
        wx.GetApp().GetTopWindow().FunctionPanels['measurement'].cryo.SendCommand('ACLP')

        # wait a little bit - 23.03.2010
        # increased to .5 8.3.2013 -- x-component of baseline measurement in NL was often screwed up (e-8 range)
        time.sleep(.5)

        self.baseline = wx.GetApp().GetTopWindow().FunctionPanels['measurement'].cryo.ReadMagMoment(show_progress=True,
                                                                                                    numval=
                                                                                                    wx.GetApp().GetTopWindow().FunctionPanels[
                                                                                                        'settings'].noreadspinCtrl.GetValue())

        self.AppendMeasurementListCtrlLine('bl', self.baseline)


    def AppendMeasurementListCtrlLine(self, txt, magmom):
        x, y, z = magmom['X'], magmom['Y'], magmom['Z']
        # calc dec, inc, M from components
        (dec, inc, M) = XYZ2DIL((x, y, z))
        # angular error
        a95 = magmom['a95']
        # stddev of magnetic moment
        sM = magmom['sM']

        self.TimedMeasurementListCtrl.Append((txt, str("% .4E" % x), str("% .4E" % y), str("% .4E" % z),
                                              str("% .4E" % M), str("% .2E" % sM), str("% .1f" % a95),
                                              str("% .0f" % dec), str("% .0f" % inc)))

        self.ping.Play(wx.adv.SOUND_ASYNC)


    def OnSpecimenComboBoxChar(self, event):
        """Handle Enter in SpecimenComboBox"""
        # enter? else exit


        if event.GetKeyCode() != wx.WXK_RETURN:
            event.Skip()

        else:
            # check if specimen exists, if not throw out a warning

            # if not self.SpecimenTextCtrl.GetValue() in wx.GetApp().GetTopWindow().specimens.GetSpecimenList():
            if not wx.GetApp().GetTopWindow().specimens.SpecimenExists(self.SpecimenComboBox.GetValue()):
                # specimen not found in specimen list
                dlg = wx.MessageDialog(self, 'The specimen name is unknown. Add to specimen list?',
                                       'New specimen',
                                       wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION
                                       )
                if dlg.ShowModal() == wx.ID_YES:

                    # add specimen to specimen data list and fill with default properties
                    wx.GetApp().GetTopWindow().specimens.AddSpecimen(self.SpecimenComboBox.GetValue())

                    # update list of specimens in specimens panel
                    wx.GetApp().GetTopWindow().FunctionPanels['specimens'].UpdateSpecimens()
                else:
                    # workaround for SelectAll which does not select the last character under Linux
                    self.SpecimenComboBox.SetMark(0, len(self.SpecimenComboBox.GetValue()))

                dlg.Destroy()

            # update display of specimen properties
            self.UpdatePropDisp()


    def UpdatePropDisp(self):
        """Update the properties of the specimen shown in the upper right corner
        of the measurement panel"""
        try:
            prop = wx.GetApp().GetTopWindow().specimens.GetProp(self.SpecimenComboBox.GetValue())
        except KeyError:
            return

        self.coreaztextCtrl.SetValue("%.0f" % prop['coreaz'])
        self.corediptextCtrl.SetValue("%.0f" % prop['coredip'])
        self.bedaztextCtrl.SetValue("%.0f" % prop['bedaz'])
        self.beddiptextCtrl.SetValue("%.0f" % prop['beddip'])
        self.voltextCtrl.SetValue("%.1G" % prop['vol'])
        self.weighttextCtrl.SetValue("%.1G" % prop['weight'])


    def OnTopPanelChar(self, event):
        # eat all tabs
        if event.GetKeyCode() != wx.WXK_TAB:
            event.Skip()


    def logger(self, evt, msg1, msg2=''):
        # log cryo raw data to file

        self.logfile.write("\r\n" + GetTime() + "#" + str(evt) + "#" + str(msg1) + "#" + str(msg2))


    def OnMeas_timerTimer(self, event):
        # check time passed
        td = time.time() - self.starttime  # seconds since start of measurement

        # claculate timestamp for next measurement
        # t = n * i ^ x

        n = self.n_numCtrl.GetValue()
        x = self.x_numCtrl.GetValue()

        # skip time steps that can't be fulfilled
        while n * math.pow(self.i, x) < td - 1:
            self.i += 1

        # calculate timestamp for next measurement
        ts = n * math.pow(self.i, x)

        # stop measuring when we exceed maximum value for i
        if self.i > self.iend_intCtrl.GetValue():
            self.MeasureTimedStop()
            return

        # update progress dialog
        # under win32 return value is (cont, skip)
        # under ubuntu only cont
        prgupd = self.prgdlg.Update(int(td), 'Next measurement: %d s\nTime to next measurement %d s' % (ts, ts - td))

        # only one boolean value returned
        if type(prgupd) == type(True):
            cont = prgupd

        # tuple returned -> use first variable
        elif type(prgupd) == type((1, 2)):
            cont = prgupd[0]

        # if unexpected return type -> cancel dialog
        else:
            cont = False

        # stop if progress dialog was canceled
        if cont != True:
            self.MeasureTimedStop()
            return

        # if time difference is larger than the timestamp -> do a measurement
        if td >= ts:
            self.MeasureTimed(seconds=td)


    def OnStartButtonButton(self, event):
        self.StartTimedMeasurement()
