#!/usr/bin/env python

# CryoMag Helper
# written by Michael Wack

import wx

import CryoMagHelperFrame

modules ={u'CryoMagHelperFrame': [1,
                         'Main frame of Application',
                         u'CryoMagHelperFrame.py']}

class BoaApp(wx.App):
    def OnInit(self):
        self.main = CryoMagHelperFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True

def main():
    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
