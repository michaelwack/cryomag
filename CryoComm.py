# communication with 2G200 and Spinner JR4
"""CryoComm implements the low level communication with measurement electronic"""
# written by Michael Wack 2008, 2009, 2010


import CryoMagSettings

if not CryoMagSettings.simulate:
    import serial
import wx
from random import randrange
# from math import *
from CryoMagAux import *

import time


class CryoMagSerialInstrument:
    """abstract baseclass for instrument control over rs232 serial link"""

    def __init__(self, port=0, logfunction=0):
        '''constructor: open serial port and initialize instrument'''

        self.logfunction = logfunction
        self.simulate = False
        try:
            if CryoMagSettings.simulate:
                self.SimulateOn(
                    'CryoMag is running in measurement simulation mode as configured in CryoMagSettings.py.')
        except Exception:
            self.SimulateOn('CryoMagSettings.simulate not defined. Add self.simulate=True to CryoMagSettings.py.')

        self.Init(port)

    def __del__(self):
        """destructor: close serial port"""
        self.ClosePort()

    def SetLogFunction(self, logfunction):
        self.logfunction = logfunction

    def OpenPort(self, **parameters):
        """open serial port with number port"""
        try:
            self.ser = serial.Serial(**parameters)
        except Exception:
            # if we cannot open the port go to simulation mode
            self.SimulateOn(
                'Cannot open serial port! Check configuration. Restart program to reestablish hardware communication.')

    def ClosePort(self):
        """Close serial port"""
        if not self.simulate:
            self.ser.close()

    def SendCommand(self, cmd):
        if not self.simulate:
            if cmd[-1] != "\r":
                cmd += "\r"
            self.ser.write(cmd.encode())

            if self.logfunction != 0:
                self.logfunction('send', cmd, 0)

            # wait for a tenth of a second
            time.sleep(.2)

    def ReadAnswer(self):
        answer = ''
        while True:
            a = self.ser.read().decode('utf-8')
            if a == "\r" or a == '':
                # stop if newline or empty character
                break
            else:
                # add received character to the answer
                answer += a

        if self.logfunction != 0:
            self.logfunction('read', answer, self.ser.inWaiting())

        return answer

    def SimulateOn(self, warning=''):
        self.simulate = True
        if warning != '':
            wx.MessageBox(warning, 'Measurement Simulation Mode Activated!')


class CryoComm(CryoMagSerialInstrument):
    """CryoComm communication class for 2G200 electronics"""

    def Init(self, port):
        """Initialize serial port and electronics"""
        # open serial port
        if not self.simulate:
            self.OpenPort(port=port, baudrate=1200, timeout=1)

        # send init commands to electronic
        initcmds = ('ACLP', 'ARC', 'ARC', 'ACF1', 'ACR1', 'ACGD', 'ACSE', 'ACTE', 'ACLC', 'ACPU')
        for cmd in initcmds:
            self.SendCommand(cmd)

        # factors to convert from flux quanta to Am^2
        # set defaults from CryoMagSettings.py
        self.SetCalibFactors(CryoMagSettings.C3calib['x'], CryoMagSettings.C3calib['y'], CryoMagSettings.C3calib['z'])

    def SetCalibFactors(self, XCalib, YCalib, ZCalib):
        self.Calib = {}

        self.Calib['X'] = XCalib
        self.Calib['Y'] = YCalib
        self.Calib['Z'] = ZCalib

        if self.logfunction != 0:
            self.logfunction('set C3 calibration factors',
                             "X: %f, Y: %f, Z: %f" % (self.Calib['X'], self.Calib['Y'], self.Calib['Z']))

    def ReadMagMoment(self, show_progress=True, numval=1):
        """ read values of all 3 squids, repeat numval times """

        if numval < 1:
            return

        if show_progress:
            dlg = wx.ProgressDialog("Communication with Magnetometer....",
                                    "Locking signals ...",
                                    maximum=6 * numval,
                                    parent=wx.GetApp().GetTopWindow(),
                                    style=wx.PD_APP_MODAL
                                          | wx.PD_ELAPSED_TIME
                                          | wx.PD_ESTIMATED_TIME
                                          | wx.PD_REMAINING_TIME
                                          | wx.PD_AUTO_HIDE
                                    )
            # set the focus to the dialog to prevent key events in other windows
            # dlg.SetFocus()

        magmoments = []

        # count for progress dialog
        count = 0

        # repeat the measurement numval times
        for n in range(numval):

            # lock Counter and Data on all axes
            for vtype in ('C', 'D'):
                for axis in ('X', 'Y', 'Z'):
                    self.LockSignal(axis, vtype)

                    # read all values

            val = {}
            for vtype in ('C', 'D'):
                val[vtype] = {}
                for axis in ('X', 'Y', 'Z'):
                    if show_progress:
                        # update progress dialog
                        dlg.Update(count, 'reading axis: %1s, signal: %1s, cycle: %d  ...' % (axis, vtype, n))

                    try:
                    	val[vtype][axis] = self.ReadValue(axis, vtype)
                    except RuntimeError:
                    	wx.MessageBox("Communication with magnetometer failed. Please restart program and try again.")
                    		
                    count += 1

            # calculate magnetic moment
            magmom = {}
            for axis in ('X', 'Y', 'Z'):
                magmom[axis] = (val['C'][axis] + val['D'][axis]) * self.Calib[axis]

            # append measured magmom to list of magnetic moments
            magmoments.append(magmom)

            # We are done, destroy the dialog
        if show_progress:
            dlg.Destroy()

        # calculate mean magmoment
        dirs = []
        M = []
        X, Y, Z = [], [], []

        mean_magmom = {}
        mean_magmom['X'] = 0
        mean_magmom['Y'] = 0
        mean_magmom['Z'] = 0

        for mm in magmoments:
            (D, I, L) = XYZ2DIL((mm['X'], mm['Y'], mm['Z']))
            # build list of inclinations, declinations
            dirs.append((D, I))
            # build list of magnetic moments
            M.append(L)
            X.append(mm['X'])
            Y.append(mm['Y'])
            Z.append(mm['Z'])

        # calculate errors and means
        mean_magmom['a95'] = AXX(dirs)
        mean, mean_magmom['sM'] = mean_stddev(M)
        mean_magmom['X'], mean_magmom['sX'] = mean_stddev(X)
        mean_magmom['Y'], mean_magmom['sY'] = mean_stddev(Y)
        mean_magmom['Z'], mean_magmom['sZ'] = mean_stddev(Z)

        # save number of readings used to calculate the mean
        mean_magmom['no_readings'] = len(magmoments)

        # add time stamp
        mean_magmom['time'] = GetTime()

        if self.logfunction != 0:
            self.logfunction('mean magentic moment',
                             "X: %.5E, Y: %.5E, Z: %.5E" % (mean_magmom['X'], mean_magmom['Y'], mean_magmom['Z']))

        # return the magnetic moment
        return mean_magmom

    def LockSignal(self, axis, type):
        """Lock (C)ounter or analog (D)ata depending on type (C,D)"""
        if axis not in ('X', 'Y', 'Z'):
            return -1
        if type not in ('C', 'D'):
            return -1

        cmd = axis + 'L' + type
        self.SendCommand(cmd)

    def ReadValue(self, axis, vtype, rep_send=5, rep_read=3):
        """Read a single measurement value of a certain axis from magnetometer\n
        axis = X,Y,Z ; type = C,D<br>
        You have to call LockSignal( axis, type) before!"""
        if axis not in ('X', 'Y', 'Z'):
            raise ValueError(f"{axis} is not a valid axis specifier. Use 'X', 'Y' or 'Z'.")
        if vtype not in ('C', 'D'):
            raise ValueError(f"{vtype} is not a valid value type specifier. Use 'C' or 'D'.")

        if self.simulate:
            # if we are in simulation mode - return a random number
            # delay .15 second        
            wx.MilliSleep(150)

            if vtype == 'C':
                val = randrange(100) / 100.0
            if vtype == 'D':
                val = randrange(1000) / 10.0

            return val
        else:
            # if we are not in simulation mode, get values from magnetometer
            val = 0

            cmd = axis + 'S' + vtype

        # try rep_send times to send command
        for a in range(rep_send):
            if a > 0:
                # trouble -> wait some extra time
                time.sleep(.3)
            self.SendCommand(cmd)

            # after command was sent try rep_read times to get a valid answer
            for i in range(rep_read):
                if i > 0:
                    # trouble -> wait for an extra .3 seconds (18.3.2010)
                    time.sleep(.3)
                ans = self.ReadAnswer()
                # check whether response is valid
                # sometimes only +0 or -0 instead of the whole value gets transmitted
                if len(ans) <= 2:
                    # break inner for loop and resend command
                    break

                try:
                    val = float(ans)
                except ValueError:
                    # no valid float value received -> try to reread value again from electronics
                    print(f"encountered invalid answer >{ans}< from magnetometer")
                    continue
                else:
                    # if we got a valid float value return it
                    return val

            # we didn't get a valid response up to now -> switch to simulation mode
            # self.SimulateOn(
            #    'Could not read value from magnetometer. Check connections and electronic! Restart program to reestablish hardware communication.')

        raise RuntimeError("getting reading from magnetometer failed")


class SpinnerComm(CryoMagSerialInstrument):
    """SpinnerComm communication class for JR4 spinner electronics"""

    def Init(self, port):
        '''Initialize serial port and electronics'''
        if not self.simulate:
            self.OpenPort(port=port, baudrate=300, parity=serial.PARITY_EVEN, stopbits=serial.STOPBITS_ONE,
                          bytesize=serial.SEVENBITS, rtscts=1, timeout=1)

        # factors to convert from flux quanta to Am^2
        # set defaults from CryoMagSettings.py
        self.SetCalibFactor(CryoMagSettings.C2calib)

    def SetCalibFactor(self, Calib):
        self.Calib = Calib

        if self.logfunction != 0:
            self.logfunction('set C2 calibration factor', "%f" % self.Calib)

    def ReadAnswer(self):
        """ read answer of JR4 over serial line"""
        answer = ''
        # while True:

        # a = self.ser.readlines( eol='\n') # doesn't work with newer versions of pyserial
        a = self.ser.readlines()

        #    if a == "\r" or a == '':
        #        #stop if newline or empty character
        #        break
        #    else:
        #        #add received character to the answer
        #        answer += a

        if self.logfunction != 0:
            self.logfunction('read', answer, self.ser.inWaiting())

        return a

    def ReadMagMoment2C(self, show_progress=True):
        """ read 2 components of mag. moment."""

        magmom = {}

        if self.simulate:
            magmom['A'] = 1.2
            magmom['B'] = 2.1

        else:
            self.SendCommand('!')

            # wait 60 seconds for data
            maxwaits = 60

            if show_progress:
                dlg = wx.ProgressDialog("Waiting for data....",
                                        "Activate measurement",
                                        maximum=maxwaits,
                                        parent=wx.GetApp().GetTopWindow(),
                                        style=wx.PD_APP_MODAL
                                              | wx.PD_CAN_ABORT
                                        )
                # set the focus to the dialog to prevent key events in other windows
                dlg.SetFocus()

            ans = []
            waits = 0
            while len(ans) != 3:
                waits = waits + 1
                ans = self.ReadAnswer()
                if show_progress:  # update progress dialog
                    stat = dlg.Update(waits, "Run Spinner!")

                    if not stat or waits >= maxwaits - 1:
                        # cancel pressed
                        dlg.Destroy()
                        return False

            # We are done, destroy the dialog
            if show_progress:
                dlg.Destroy()

            A = float(ans[0].strip())  # first component
            B = float(ans[1].strip())  # second component
            E = float(ans[2].strip())  # exponent == range selector

            # mutiply components with exponent and calibration factor
            magmom['A'] = CryoMagSettings.C2calib * A * 10 ** E
            magmom['B'] = CryoMagSettings.C2calib * B * 10 ** E

            # TODO: switch to simulation mode if answer not valid
            # extract two components from ans (A,B)
            # ans --> magmom[A,B]

        # add time stamp
        magmom['time'] = GetTime()

        if self.logfunction != 0:
            self.logfunction('magentic moment', "A: %.5E, B: %.5E" % (magmom['A'], mean_magmom['B']))

        # return the magnetic moment
        return magmom
