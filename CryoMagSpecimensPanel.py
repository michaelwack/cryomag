# Boa:FramePanel:CryoMagSpecimensPanel
"""Implementation of the specimen panel. Allows to edit properties of specimens
and updates corresponding data files."""

import wx
import wx.grid


class CryoMagSpecimensPanel(wx.Panel):
    def _init_coll_boxSizer1_Items(self, parent):

        parent.Add(self.SpecimensGrid, 1, border=0, flag=wx.GROW)

    def _init_sizers(self):

        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):

        wx.Panel.__init__(self, id=wx.ID_ANY,
                          name=u'CryoMagSpecimensPanel', parent=prnt, pos=wx.Point(363,
                                                                                   300), size=wx.Size(562, 410),
                          style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(554, 383))
        self.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL, False, u'Sans'))

        self.SpecimensGrid = wx.grid.Grid(id=wx.ID_ANY,
                                          name=u'SpecimensGrid', parent=self, pos=wx.Point(0, 0),
                                          size=wx.Size(554, 383), style=0)
        self.SpecimensGrid.SetColLabelSize(16)
        self.SpecimensGrid.SetToolTip(u'Specimens')
        self.SpecimensGrid.SetDefaultRowSize(16)
        self.SpecimensGrid.Bind(wx.EVT_KEY_DOWN, self.OnSpecimensGridKeyDown)
        self.SpecimensGrid.Bind(wx.grid.EVT_GRID_CELL_CHANGED,
                                self.OnSpecimensGridGridCellChange)
        self.SpecimensGrid.Bind(wx.grid.EVT_GRID_SELECT_CELL,
                                self.OnSpecimensGridGridSelectCell)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)

        self.SpecimensGrid.CreateGrid(0, 7)
        self.SpecimensGrid.SetColLabelValue(0, u'specimen')
        self.SpecimensGrid.SetColLabelValue(1, u'coreaz [\xb0]')
        self.SpecimensGrid.SetColLabelValue(2, u'coredip [\xb0]')
        self.SpecimensGrid.SetColLabelValue(3, u'bedaz [\xb0]')
        self.SpecimensGrid.SetColLabelValue(4, u'beddip [\xb0]')
        self.SpecimensGrid.SetColLabelValue(5, u'volume [cm\xb3]')
        self.SpecimensGrid.SetColLabelValue(6, u'weight [g]')

        # init last row to -1
        self.last_r = -1

    def UpdateSpecimens(self):
        """ fills grid with data from self.specimens """

        # delete all rows from grid
        if self.SpecimensGrid.GetNumberRows() > 0:
            self.SpecimensGrid.DeleteRows(numRows=self.SpecimensGrid.GetNumberRows())

        r = 0
        self.SpecimensGrid.BeginBatch()
        for specimenname in wx.GetApp().GetTopWindow().specimens.GetSpecimenList():
            if specimenname != 'holder':
                # append a new row to the grid
                self.SpecimensGrid.AppendRows()

                prop = wx.GetApp().GetTopWindow().specimens.GetProp(specimenname)
                self.SpecimensGrid.SetCellValue(r, 0, specimenname)
                self.SpecimensGrid.SetCellValue(r, 1, "%.3G" % (prop['coreaz']))
                self.SpecimensGrid.SetCellValue(r, 2, "%.3G" % (prop['coredip']))
                self.SpecimensGrid.SetCellValue(r, 3, "%.3G" % (prop['bedaz']))
                self.SpecimensGrid.SetCellValue(r, 4, "%.3G" % (prop['beddip']))
                self.SpecimensGrid.SetCellValue(r, 5, "%.3G" % (prop['vol']))
                self.SpecimensGrid.SetCellValue(r, 6, "%.3G" % (prop['weight']))
                r += 1

        self.SpecimensGrid.EndBatch()

    def OnSpecimensGridKeyDown(self, evt):
        """Key handling for SpecimensGrid"""

        # patch for linux (ubuntu 7.10, gnome)
        # function keys do not work properly since they are catched by the cell editor
        # therefore we deactivate it in case of a function key and reactivate the editing for all other keys
        if (evt.GetKeyCode() >= wx.WXK_F5) and (evt.GetKeyCode() <= wx.WXK_F9):
            self.SpecimensGrid.EnableEditing(False)
            evt.Skip()
            return
        else:
            self.SpecimensGrid.EnableEditing(True)

        # change key behavior, enter moves to the right
        if evt.GetKeyCode() != wx.WXK_RETURN:
            evt.Skip()
            return
        if evt.ControlDown():  # the edit control needs this key
            evt.Skip()
            return
        self.SpecimensGrid.DisableCellEditControl()
        success = self.SpecimensGrid.MoveCursorRight(evt.ShiftDown())
        if not success:
            newRow = self.SpecimensGrid.GetGridCursorRow() + 1
            if newRow < self.SpecimensGrid.GetTable().GetNumberRows():
                self.SpecimensGrid.SetGridCursor(newRow, 0)
                self.SpecimensGrid.MakeCellVisible(newRow, 0)
            else:
                # this would be a good place to add a new row if your app 
                # needs to do that 
                pass

    def OnSpecimensGridGridCellChange(self, event):
        """When a cell in the grid is changed self.specimendata has to be adjusted.
        Further the specimenfile has to be rewritten"""

        # get the position of the cell that was changed
        r = event.GetRow()
        c = event.GetCol()

        specimenname = self.SpecimensGrid.GetCellValue(r, 0)

        # column order
        cols = ['specimenname', 'coreaz', 'coredip', 'bedaz', 'beddip', 'vol', 'weight']

        if c == 0:  # specimenname
            wx.MessageBox('Please edit the specimen name in the data panel.', 'Change of name not allowed')

            # BUGFIX 1.10.2008
            event.Veto()
        else:
            try:
                # set specimen property
                wx.GetApp().GetTopWindow().specimens.GetProp(specimenname)[cols[c]] = float(
                    self.SpecimensGrid.GetCellValue(r, c))
                # store changed specimen to file
                wx.GetApp().GetTopWindow().specimens.WriteSpecimenDataToFile(specimenname)
                # update plots if needed
                # performance tuning 20.01.09
                # update plots only when field data was changed and coordinate system is not core
                if wx.GetApp().GetTopWindow().CoreRadioButton.GetValue() == False and cols[c] in (
                        'coreaz', 'coredip', 'bedaz', 'beddip'):
                    wx.GetApp().GetTopWindow().ShowSpecimenData(specimenname, False)

                # update data panel
                # wx.GetApp().GetTopWindow().FunctionPanels[ 'data'].UpdateSpecimens()
                # performance tuning 20.01.09
                # mark datapanel for update when it's shown the next time
                wx.GetApp().GetTopWindow().FunctionPanels['data'].needupdate = True

            except ValueError:
                # veto event
                wx.MessageBox('%s must be numeric.' % cols[c])
                # BUGFIX 21.10.2008
                event.Veto()

    def OnSpecimensGridGridSelectCell(self, event):
        # get the position of the cell that was selected
        r = event.GetRow()
        c = event.GetCol()

        # get specimen
        specimenname = self.SpecimensGrid.GetCellValue(r, 0)

        # update plots
        # performance tuning 20.01.09
        # only update plot when row changed
        if self.last_r != r:
            self.last_r = r
            wx.GetApp().GetTopWindow().ShowSpecimenData(specimenname, False)

        event.Skip()
