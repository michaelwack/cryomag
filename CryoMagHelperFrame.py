# CryoMagHelperFrame
# written by Michael Wack
# wack@geophysik.uni-muenchen.de

# adapted to Python 3 in 2022

import time
from datetime import datetime

import wx
import wx.lib.intctrl

import CryoMagSettings
from CryoComm import CryoComm
from pathlib import Path

mm_keys = ('X', 'Y', 'Z', 'sX', 'sY', 'sZ', 'sM', 'a95')  # dict keys of measurements results

def create(parent):
    return CryoMagHelperFrame(parent)

class CryoMagHelperFrame(wx.Frame):

    def _init_ctrls(self, prnt):

        wx.Frame.__init__(self, id=wx.ID_ANY,
                          name=u'CryoMagHelperFrame', parent=prnt, pos=wx.Point(198, 223),
                          size=wx.Size(716, 610), style=wx.DEFAULT_FRAME_STYLE,
                          title=u'CryoMag Helper')
        self.MeasurementTimer = wx.Timer(id=wx.ID_ANY,
                                         owner=self)
        self.Bind(wx.EVT_TIMER, self.OnMeasurementTimerTimer,
                  id=wx.ID_ANY)
        self.SetClientSize(wx.Size(708, 576))

        self.TopPanel = wx.Panel(id=wx.ID_ANY,
                                 name=u'TopPanel', parent=self, pos=wx.Point(0, 0),
                                 size=wx.Size(708, 81), style=wx.TAB_TRAVERSAL)

        self.FunctionNotebook = wx.Notebook(id=wx.ID_ANY,
                                            name=u'FunctionNotebook', parent=self, pos=wx.Point(0, 81),
                                            size=wx.Size(708, 495), style=0)

        self.panel1 = wx.Panel(id=wx.ID_ANY, name='panel1',
                               parent=self.FunctionNotebook, pos=wx.Point(0, 0),
                               size=wx.Size(700, 469), style=wx.TAB_TRAVERSAL)

        self.panel2 = wx.Panel(id=wx.ID_ANY, name='panel2',
                               parent=self.FunctionNotebook, pos=wx.Point(0, 0),
                               size=wx.Size(700, 469), style=wx.TAB_TRAVERSAL)

        self.CommandTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                           name=u'CommandTextCtrl', parent=self.TopPanel, pos=wx.Point(16,
                                                                                                       56),
                                           size=wx.Size(328, 21), style=0, value=u'')

        self.CommandButton = wx.Button(id=wx.ID_ANY,
                                       label=u'Send Command', name=u'CommandButton',
                                       parent=self.TopPanel, pos=wx.Point(368, 56), size=wx.Size(144,
                                                                                                 25), style=0)
        self.CommandButton.Bind(wx.EVT_BUTTON, self.OnCommandButtonButton,
                                id=wx.ID_ANY)

        self.AverageCountIntCtrl = wx.lib.intctrl.IntCtrl(allow_long=False,
                                                          allow_none=False, default_color=wx.BLACK,
                                                          id=wx.ID_ANY, limited=True,
                                                          max=None, min=1, name=u'AverageCountIntCtrl',
                                                          oob_color=wx.RED,
                                                          parent=self.TopPanel, pos=wx.Point(16, 24),
                                                          size=wx.Size(48, 21),
                                                          style=0, value=1)

        self.RepeatCountIntCtrl = wx.lib.intctrl.IntCtrl(allow_long=False,
                                                         allow_none=False, default_color=wx.BLACK,
                                                         id=wx.ID_ANY, limited=True,
                                                         max=None, min=1, name=u'RepeatCountIntCtrl', oob_color=wx.RED,
                                                         parent=self.TopPanel, pos=wx.Point(128, 24),
                                                         size=wx.Size(56, 21),
                                                         style=0, value=1)

        self.ClockIntCtrl = wx.lib.intctrl.IntCtrl(allow_long=False,
                                                   allow_none=False, default_color=wx.BLACK,
                                                   id=wx.ID_ANY, limited=True, max=None,
                                                   min=0, name=u'ClockIntCtrl', oob_color=wx.RED,
                                                   parent=self.TopPanel, pos=wx.Point(232, 24), size=wx.Size(64, 21),
                                                   style=0, value=0)

        self.staticText1 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'average count', name='staticText1', parent=self.TopPanel,
                                         pos=wx.Point(16, 8), size=wx.Size(96, 25), style=0)

        self.staticText2 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'repeat count', name='staticText2', parent=self.TopPanel,
                                         pos=wx.Point(128, 8), size=wx.Size(88, 25), style=0)

        self.staticText3 = wx.StaticText(id=wx.ID_ANY,
                                         label=u'clock [s]', name='staticText3', parent=self.TopPanel,
                                         pos=wx.Point(232, 8), size=wx.Size(72, 25), style=0)

        self.StartMeasurementButton = wx.Button(id=wx.ID_ANY,
                                                label=u'Start Measurement', name=u'StartMeasurementButton',
                                                parent=self.TopPanel, pos=wx.Point(368, 24), size=wx.Size(144,
                                                                                                          25), style=0)
        self.StartMeasurementButton.Bind(wx.EVT_BUTTON,
                                         self.OnStartMeasurementButtonButton,
                                         id=wx.ID_ANY)

        self.MeasurementResultsTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                                      name=u'MeasurementResultsTextCtrl', parent=self.panel1,
                                                      pos=wx.Point(0, 0), size=wx.Size(700, 469), style=wx.TE_MULTILINE,
                                                      value=u'')

        self.MeasurementResultsTextCtrl.SetDefaultStyle(wx.TextAttr("black", font=wx.Font(10, family=wx.DEFAULT, style=wx.NORMAL, weight=wx.NORMAL, underline=False, faceName="Monospace")))

        self.MeasurementResultsTextCtrl.SetEditable(False)

        self.LowLevelCommTextCtrl = wx.TextCtrl(id=wx.ID_ANY,
                                                name=u'LowLevelCommTextCtrl', parent=self.panel2, pos=wx.Point(0, 0),
                                                size=wx.Size(700, 469), style=wx.TE_MULTILINE, value=u'')
        self.LowLevelCommTextCtrl.SetEditable(False)

        self.FunctionNotebook.AddPage(imageId=-1, page=self.panel1, select=True,
                                      text=u'Measurement Results')
        self.FunctionNotebook.AddPage(imageId=-1, page=self.panel2, select=False,
                                      text=u'Low Level Comm Protocol')

        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)
        self.boxSizer2 = wx.BoxSizer(orient=wx.VERTICAL)
        self.boxSizer3 = wx.BoxSizer(orient=wx.VERTICAL)
        self.boxSizer1.Add(self.TopPanel, proportion=0, border=0, flag=wx.GROW)
        self.boxSizer1.Add(self.FunctionNotebook, proportion=1, border=0, flag=wx.GROW)
        self.boxSizer2.Add(self.MeasurementResultsTextCtrl, proportion=1, border=0,
                           flag=wx.GROW)
        self.boxSizer3.Add(self.LowLevelCommTextCtrl, 1, border=0, flag=wx.GROW)
        self.SetSizer(self.boxSizer1)
        self.panel1.SetSizer(self.boxSizer2)
        self.panel2.SetSizer(self.boxSizer3)

    def __init__(self, parent):
        self._init_ctrls(parent)

        self.cryo = CryoComm(port=CryoMagSettings.serialport, logfunction=self.logger)

        self.lasttime = 0

        self.outfile = None  # file handle to output data to

        self.sum_mm = {}   # initialize dict to sum all measurement components for final average
        for key in mm_keys:
            self.sum_mm[key] = 0

    def logger(self, evt, msg1, msg2=''):
        if evt == 'send':
            log = "%s: %s" % (str(evt), str(msg1))
        elif evt == 'read':
            log = "%s: %s (bytes left: %s)\n" % (str(evt), str(msg1), str(msg2))
        else:
            log = 'unknown logging call'

        log = time.asctime() + '  ' + log

        self.LowLevelCommTextCtrl.AppendText(log)

    def OnCommandButtonButton(self, event):
        """ send a command over serial port to magnetometer and wait for the answer """
        self.cryo.SendCommand(self.CommandTextCtrl.GetValue())

        self.cryo.ReadAnswer()

    def OnMeasurementTimerTimer(self, event):
        """ checks if we have to do a measurement """

        now = time.time()
        if now - self.lasttime > self.clock:
            self.lasttime = now
            mm = self.cryo.ReadMagMoment(show_progress=True, numval=self.averagecount)

            for key in mm_keys:
                self.sum_mm[key] += mm[key]

            # append results to Measurement Results
            self.OutputData(
                "%03d  |  %03d  |   %s  |  %.3E | %.3E | %.3E | %.3E | %.3E | %.3E | %.3E | %.2f\n" % (
                    self.mno, self.averagecount, time.asctime(), mm['X'], mm['Y'], mm['Z'], mm['sX'], mm['sY'], mm['sZ'], mm['sM'], mm['a95']))

            self.remainingMeasurements -= 1
            if self.remainingMeasurements <= 0:
                # we are done
                self.StopMeasurement()

            # increase number of measurement
            self.mno += 1

        # wx.MessageBox( 'Timer')
        # time.sleep( 10)
        event.Skip()

    def OnStartMeasurementButtonButton(self, event):
        """ start MeasurementTimer """
        if self.MeasurementTimer.IsRunning():
            self.StopMeasurement()
        else:
            self.StartMeasurement()

        event.Skip()

    def StopMeasurement(self):
        self.MeasurementTimer.Stop()
        self.StartMeasurementButton.SetLabel('Start Measurement')
        self.StartMeasurementButton.Refresh()

        for key in mm_keys:  # calculate averages
            self.sum_mm[key] /= self.mno

        # append results to Measurement Results
        self.OutputData(
            "%03d  |  %03d  |   %s  |  %.3E | %.3E | %.3E | %.3E | %.3E | %.3E | %.3E | %.2f\n" % (
                0, self.mno, 'AVERAGE                 ', self.sum_mm['X'], self.sum_mm['Y'], self.sum_mm['Z'], self.sum_mm['sX'], self.sum_mm['sY'], self.sum_mm['sZ'],
                self.sum_mm['sM'], self.sum_mm['a95']))

        if self.outfile is not None:
            self.outfile.close()  # close output file

        # do some statistics ....

    def StartMeasurement(self):
        # start timer, clock .5s
        self.MeasurementTimer.Start(milliseconds=500)

        self.remainingMeasurements = self.RepeatCountIntCtrl.GetValue()
        self.clock = self.ClockIntCtrl.GetValue()
        self.averagecount = self.AverageCountIntCtrl.GetValue()
        self.StartMeasurementButton.SetLabel('Stop Measurement')
        self.StartMeasurementButton.Refresh()

        outfilename = Path.home().joinpath(datetime.now().strftime("%Y%m%d-%H%M%S") + '_cryomag_helper.txt')
        print(f"writing data to {outfilename}")
        try:
            self.outfile = open(outfilename, 'w')
        except:
            self.outfile = None

        self.mno = 1

        self.OutputData(
            "No      AvgNo    time                         X            Y            Z            sX          sY          sZ          sM          a95\n")


    def OutputData(self, line):
        # output one data line to the gui and to the current output file
        self.MeasurementResultsTextCtrl.AppendText(line)
        if self.outfile is not None:
            self.outfile.write(line)
